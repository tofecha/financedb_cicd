import { create } from 'apisauce';

const bspApi = create({
  baseURL: 'https://www.bsp.gov.ph'
});

export default bspApi;
