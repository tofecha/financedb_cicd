export default {
  NODE_ENV: process.env.NODE_ENV,
  HOST: process.env.HOST,
  PORT: process.env.PORT,
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_NAME: process.env.DB_NAME,
  DB_USER: process.env.DB_USER,
  DB_PASS: process.env.DB_PASS,
  SKIP_MAINTENANCE: process.env.SKIP_MAINTENANCE === 'true',
  CLIENT_BUILD_LOCATION: process.env.CLIENT_BUILD_LOCATION,
  AUTH: process.env.AUTH === 'true'
};