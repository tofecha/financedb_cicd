import { create } from 'apisauce';

const accountsApi = create({
  baseURL: 'https://accounts.awsnmsappstaging.nmscreative.com/'
});

export default accountsApi;
