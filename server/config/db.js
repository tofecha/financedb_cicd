import mongoose from 'mongoose';
import env from './env.js';

const MongoDBConnect = () => new Promise((resolve, reject) => {
  mongoose.connect(`mongodb://${env.DB_USER}:${env.DB_PASS}@${env.DB_HOST}:${env.DB_PORT}/${env.DB_NAME}?authSource=admin`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // useCreateIndex: true
  });

  const db = mongoose.connection;
  db.on('error', () => {
    console.error.bind(console, 'connection error: ');
    reject();
  });
  db.once('open', () => {
    resolve();
  });
});

export {
  mongoose,
  MongoDBConnect
};