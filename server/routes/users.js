import lodash from 'lodash';
import { Router } from 'express';
import accountsApi from '../config/accountsApi.js';
import env from '../config/env.js';
import { getUser, updateUser } from '../controllers/userController.js';
const { includes } = lodash;
const url = '/api/authorize';

const userRouter = Router();

userRouter.all('*', async (req, res, next) => {
  if (includes(req.url, 'favicon.ico')) {
    res.sendStatus(204);
  } else {
    if (env.AUTH) {
      try {
        const authenticateUser = await accountsApi.get(`${url}/user`, {}, {
          headers: {
            Authorization: req.headers.authorization || ''
          }
        });
        if (authenticateUser.ok && authenticateUser.status === 200) {
          const { birthdate, client_id, freelance, fulltime, gender, request_id, ...importantDetails } = authenticateUser.data;
          const userData = {
            ...importantDetails,
            platform_account: await getUser(importantDetails)
          };
          if (req.url === '/') {
            res.status(authenticateUser.status).json(userData);
          } else {
            // get user settings
            req.user = userData;
            req.permissions = [];
            next();
          }
        } else {
          res.status(authenticateUser.status).send(authenticateUser.data);
        }
      } catch (err) {
        res.status(500).send(err);
      }
    } else {
      next();
    }
  }
});

userRouter.get('/all', async (req, res) => {
  const query = await accountsApi.get(`${url}/user/data/list/`, {
    option: {
      search: 'Eula'
    },
  }, {
    headers: {
      Authorization: req.headers.authorization
    }
  });
  res.status(query.status).send(query.data);
});

userRouter.get('/pending', async (req, res) => {
  const query = await accountsApi.get(`${url}/client-user/pending-users`, {}, {
    headers: {
      Authorization: req.headers.authorization
    }
  });
  res.status(query.status).send(query.data);
});

userRouter.get('/unauthorized', async (req, res) => {
  const query = await accountsApi.get(`${url}/client-user/unauthorized-users`, {}, {
    headers: {
      Authorization: req.headers.authorization
    }
  });
  res.status(query.status).send(query.data);
});

userRouter.get('/allowed', async (req, res) => {
  const query = await accountsApi.get(`${url}/client-user/allowed-users`, {}, {
    headers: {
      Authorization: req.headers.authorization
    }
  });
  res.status(query.status).send(query.data);
});

// user_account_id
userRouter.post('/allow/', async (req, res) => {
  const query = await accountsApi.post(`${url}/client-user/allow-user`, {
    user_account_id: [req.body.user_account_id]
  }, {
    headers: {
      Authorization: req.headers.authorization
    }
  });
  console.log(query.status, query.data);
  if (query.status && query.data && query.data.message === 'success') {
    res.status(query.status).send(query.data);
  } else {
    res.status(500).send({
      error: query.data.message || 'Something unexpected happened. Please try again.'
    });
  }
});

// user_account_id
userRouter.post('/deny', async (req, res) => {
  const query = await accountsApi.post(`${url}/client-user/deny-user`, {
    user_account_id: [req.body.user_account_id]
  }, {
    headers: {
      Authorization: req.headers.authorization
    }
  });
  if (query.status && query.data && query.data.message === 'success') {
    res.status(query.status).send(query.data);
  } else {
    res.status(500).send({
      error: query.data.message || 'Something unexpected happened. Please try again.'
    });
  }
});

//
userRouter.post('/update', async (req, res) => {
  try {
    const updatedUser = await updateUser(req);
    res.status(200).send(updatedUser);
  } catch (err) {
    res.status(500).send({
      error: err
    });
  }
});

export default userRouter;