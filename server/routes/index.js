import lodash from 'lodash';
import { Router } from 'express';
const { includes } = lodash;

const indexRouter = Router();

indexRouter.all('*', async (req, res, next) => {
  if (includes(req.url, 'favicon.ico')) {
    res.sendStatus(204);
  } else {
    next();
  }
});

indexRouter.get('/ping', async (req, res) => {
  res.send('pong');
});

export default indexRouter;