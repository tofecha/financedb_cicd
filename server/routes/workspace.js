import lodash from 'lodash';
import { Router } from 'express';
import accountsApi from '../config/accountsApi.js';
import env from '../config/env.js';
import { deleteWorkspace, getAllWorkspace, getWorkspace, upsertWorkspace } from '../controllers/workspaceController.js';
import { getUser } from '../controllers/userController.js';
const { includes } = lodash;
const url = '/api/authorize';

const workspaceRouter = Router();

workspaceRouter.all('*', async (req, res, next) => {
  if (includes(req.url, 'favicon.ico')) {
    res.sendStatus(204);
  } else {
    if (env.AUTH) {
      try {
        const authenticateUser = await accountsApi.get(`${url}/user`, {}, {
          headers: {
            Authorization: req.headers.authorization || ''
          }
        });
        if (authenticateUser.ok && authenticateUser.status === 200) {
          const { birthdate, client_id, freelance, fulltime, gender, request_id, ...importantDetails } = authenticateUser.data;
          const userData = {
            ...importantDetails,
            platform_account: await getUser(importantDetails)
          };
          if (req.url === '/') {
            res.status(authenticateUser.status).json(userData);
          } else {
            // get workspace settings/???
            req.user = userData;
            next();
          }
        } else {
          res.status(authenticateUser.status).send(authenticateUser.data);
        }
      } catch (err) {
        res.status(500).send(err);
      }
    } else {  
      next();
    }
  }
});

workspaceRouter.get('/all', async (req, res) => {
  res.send(await getAllWorkspace());
});

workspaceRouter.get('/', async (req, res) => {
  if (req.query?.id) {
    const workspace = await getWorkspace(req.query.id);
    if (workspace) {
      res.status(200).send(workspace);
    } else {
      res.status(404).send(`Workspace "${req.query.id}" does not exist.`);
    }
  } else {
    res.status(200).send('The parameter `id` is missing.');
  }
});

workspaceRouter.post('/save', async (req, res) => {
  try {
    const result = await upsertWorkspace(req);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(false);
  }
});

workspaceRouter.post('/delete', async (req, res) => {
  try {
    const result = await deleteWorkspace(req);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(false);
  }
});

export default workspaceRouter;