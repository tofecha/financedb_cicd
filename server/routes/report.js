import lodash from 'lodash';
import { Router } from 'express';
import { getDataFrom, getData, getQuarterData, upsertData, getMonthsWithData } from '../controllers/financeReportDataController.js';
const { includes } = lodash;
import accountsApi from '../config/accountsApi.js';
import env from '../config/env.js';
const url = '/api/authorize';

const reportRouter = Router();

reportRouter.all('*', async (req, res, next) => {
  if (includes(req.url, 'favicon.ico')) {
    res.sendStatus(204);
  } else {
    if (env.AUTH) {
      try {
        const authenticateUser = await accountsApi.get(`${url}/user`, {}, {
          headers: {
            Authorization: req.headers.authorization || ''
          }
        });
        if (authenticateUser.ok && authenticateUser.status === 200) {
          next();
        } else {
          res.status(authenticateUser.status).send(authenticateUser.data);
        }
      } catch (err) {
        res.status(500).send(err);
      }
    } else {
      next();
    }
  }
});

reportRouter.get('/months', async (req, res) => {
  res.send(await getMonthsWithData());
});

reportRouter.get('/get/quarter', async (req, res) => {
  res.send(await getQuarterData(req.query.quarter, req.query.year));
});

reportRouter.get('/get', async (req, res) => {
  if (req.query.limit) {
    res.send(await getDataFrom(req.query.date, req.query.limit));
  } else {
    res.send(await getData(req.query.date));
  }
});

reportRouter.post('/save', async (req, res) => {
  try {
    const result = await upsertData(req.body);
    res.status(200).send(result);
  } catch (err) {
    res.status(500).send(false);
  }
});

export default reportRouter;