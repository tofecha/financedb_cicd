import { isValid, format } from 'date-fns';
import lodash from 'lodash';
import { saveToDatabase } from '../controllers/databaseController.js';
import { Router } from 'express';
import { generate, getHistoricalRates } from '../controllers/dataGeneratorController.js';
import { dataPiper, downloader } from '../controllers/downloadController.js';
import { scrapeProcess } from '../controllers/processController.js';
import accountsApi from '../config/accountsApi.js';
import env from '../config/env.js';
const { includes } = lodash;
const url = '/api/authorize';

const newDateFormatApr62021 = new Date('2021 Apr 6');
const forexRouter = Router();

forexRouter.all('*', async (req, res, next) => {
  if (includes(req.url, 'favicon.ico')) {
    res.sendStatus(204);
  } else {
    if (env.AUTH) {
      try {
        const authenticateUser = await accountsApi.get(`${url}/user`, {}, {
          headers: {
            Authorization: req.headers.authorization || ''
          }
        });
        if (authenticateUser.ok && authenticateUser.status === 200) {
          next();
        } else {
          res.status(authenticateUser.status).send(authenticateUser.data);
        }
      } catch (err) {
        res.status(500).send(err);
      }
    } else {
      next();
    }
  }
});

forexRouter.get('/latest', async (req, res) => {
  const { base = 'USD', symbols = 'PHP' } = req.query;
  res.send(await generate(new Date(), base, symbols, false));
});

forexRouter.get('/history', async (req, res) => {
  const { base = 'USD', symbols = 'PHP', start_at, end_at } = req.query;
  const startAt = Date.parse(start_at);
  const endAt = Date.parse(end_at);

  if (start_at && end_at && isValid(startAt) && isValid(endAt)) {
    const test = await generate(endAt, base, symbols, false);
    if (test.error) {
      res.status(400).send(test);
    } else {
      res.status(200).send({
        rates: await getHistoricalRates(base, symbols, start_at, end_at),
        start_at: start_at,
        base: base,
        end_at: end_at
      });
    }
  } else {
    if (!start_at || !end_at) {
      res.status(400).send({
        error: `missing ${start_at ? '' : 'start_at'} ${end_at ? '' : 'end_at'} parameter`,
      });
    } else {
      res.status(400).send(!isValid(startAt) ? {
        error: 'start_at parameter format',
        exception: `time data '${start_at}' does not match format '%Y-%m-%d'`
      } : {
        error: 'end_at parameter format',
        exception: `time data '${end_at}' does not match format '%Y-%m-%d'`
      });
    }
  }
});

forexRouter.post('/save', saveToDatabase);
forexRouter.get('/pdf', async (req, res) => {
  try {
    const { date } = req.query;
    const convertedDate = new Date(date);
    let urlDateFormat = undefined;
    if (convertedDate >= newDateFormatApr62021) {
      urlDateFormat = `/statistics/rerb/${format(convertedDate, 'ddLLLyyyy')}.pdf`;
    } else {
      urlDateFormat = `/statistics/rerb/${format(convertedDate, 'dd%20LLL%20yyyy')}.pdf`;
    }
    const formattedDate = format(convertedDate, 'dd-LLL-yyyy');
    
    const downloadedData = await downloader(urlDateFormat);
    const pipedData = await dataPiper(downloadedData, `${formattedDate}.pdf`);
    
    if (pipedData.existsInBSP) {
      res.sendFile(pipedData.file_path);
    } else {
      res.sendStatus(404);
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

forexRouter.get('/:data_1/:data_2?', async (req, res) => {
  if (includes(req.params.data_1, 'pdf') && includes(req.params.data_2, 'scrape')) {
    const { date = undefined, version = undefined} = req.query;
    const parsedDate = Date.parse(date);
    const supportedVersion = includes(['v1', 'v2'], version);
    const isValidDate = isValid(parsedDate);
  
    if (isValidDate && supportedVersion) {
      res.send(await scrapeProcess(parsedDate, true, false));
    } else {
      let error = '';
      if (!date || !version) {
        error = `missing ${!date ? 'date' : '' }, ${!version ? 'version' : ''} parameter${!date && !version ? 's' : ''}`;
      } else if (!isValidDate) {
        error = `Date ${date} is not a valid format or is a format that cannot be parsed`;
      } else {
        error = `version ${version} is not supported`;
      }
      res.send({
        error: error 
      });
    }
  } else if (includes(req.params.data_1, 'scrape')) {
    const parsedDate = Date.parse(req.params.data_2);
    if (isValid(parsedDate)) {
      res.send(await scrapeProcess(parsedDate, true, true, true));
    } else {
      if (req.params.data_2) {
        res.send({
          error: `time data '${req.params.data_2}' does not match format '%Y-%m-%d'`
        });
      } else {
        res.send({
          error: 'missing date parameter',
        });
      }
    }  
  } else {
    const { base = 'USD', symbols = 'PHP', full = 'false' } = req.query;
    const parsedDate = Date.parse(req.params.data_1);
    const fullData = full === 'true';
    if (isValid(parsedDate)) {
      res.send(await generate(parsedDate, base, symbols, fullData));
    } else {
      if (req.params.data_1) {
        res.send({
          error: `time data '${req.params.data_1}' does not match format '%Y-%m-%d'`
        });
      } else {
        res.send({
          error: 'missing date parameter',
        });
      }
    }
  }
});

export default forexRouter;