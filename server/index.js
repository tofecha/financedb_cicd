import express from 'express';
import cors from 'cors';
import { CronJob } from 'cron';
import { format, addDays, subDays, isWeekend, isSameDay, intervalToDuration } from 'date-fns';
import path from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import nocache from 'nocache';

import env from './config/env.js';

import { dateToday, scrapeProcess, setupDate } from './controllers/processController.js';
import { getData } from './controllers/databaseController.js';
import { MongoDBConnect } from './config/db.js';

import indexRouter from './routes/index.js';
import reportRouter from './routes/report.js';
import forexRouter from './routes/forex.js';
import userRouter from './routes/users.js';
import workspaceRouter from './routes/workspace.js';

import { clearPdfFolder } from './controllers/fsController.js';
import { initPermissions } from './models/permissionsModel.js';
import { initRoles } from './models/rolesModel.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

process.on('unhandledRejection', (reason, p) => {
  console.error('Unhandled Rejection at:', p, 'reason:', reason);
  process.exit(1);
});

const HOST = env.HOST;
const PORT = env.PORT;
const startDate = new Date('2018-01-01');
const clientBuildPath = env.CLIENT_BUILD_LOCATION;

const app = express();
app.use(nocache());
app.set('port', PORT);
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/api/workspace', workspaceRouter);
app.use('/api/report', reportRouter);
app.use('/api/forex', forexRouter);
app.use('/api/user', userRouter);
app.use('/api', indexRouter);

if (env.NODE_ENV === 'production') {
  console.log('-------------------- PRODUCTION MODE --------------------');
  app.use(express.static(path.join(__dirname, clientBuildPath)));
  // catch-all (/*) approach instead of isomorphic mode
  app.get('/stats', (req, res) => {
    res.sendFile(path.join(__dirname, clientBuildPath, 'stats.html'));
  });

  app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, clientBuildPath, 'index.html'));
  });
}

const appState = {
  maintenance: false,
  scrapingToday: false,
  connectedToMongo: false
};

app.listen(PORT, async () => {
  setupDate(new Date());

  console.log(`App listening at ${HOST}:${PORT}`);
  try {
    await MongoDBConnect();
    appState.connectedToMongo = true;
    // initialize and make sure Admin and Visitor roles are created.
    await initPermissions();
    await initRoles();
  } catch (err) {
    console.error('MongoDBConnect Error : ', err);
    appState.connectedToMongo = false;
  }
  
  const x = new Date();
  const offset = -x.getTimezoneOffset();
  const timeStart = offset === 0 ? 0 : 8;
  const scraperTodayEightNineMinutely = new CronJob(`0 */3 ${timeStart}-${timeStart + 1} * * *`, async () => {
    await scrapeToday();
  });

  const scraperTodayNineSixFiveMinutely = new CronJob(`0 */30 ${timeStart + 1}-${timeStart + 10} * * *`, async () => {
    await scrapeToday();
  });

  const dailyMaintenance = new CronJob(`0 0 ${timeStart === 8 ? 0 : 16} * * *`, () => {
    maintenance();
  });

  scraperTodayEightNineMinutely.start();
  scraperTodayNineSixFiveMinutely.start();
  dailyMaintenance.start();

  if (!env.SKIP_MAINTENANCE) {
    await maintenance();
  }
  await scrapeToday();
});

const maintenance = async () => {  
  if (!appState.maintenance) {
    const maintenanceStartTime = new Date();
    appState.maintenance = true;
    console.info(`[${format(maintenanceStartTime, 'hh:mm a : LLL dd yyyy')}] Maintenance started`);
    clearPdfFolder();

    setupDate(new Date());

    let date = startDate;
    
    while (!isSameDay(date, dateToday)) {
      await scrapeProcess(date, false, true);
      date = addDays(date, 1);
    }

    const maintenanceFinishedTime = new Date();
    const duration = intervalToDuration({
      start: maintenanceStartTime,
      end: maintenanceFinishedTime
    });
    // console.info('Backing up database');
    // await mongodbRestore.database({
    //   uri: `mongodb://${env.DB_HOST}:${env.DB_PORT}`,
    //   database: env.DB_NAME
    // });

    console.info(`[${format(maintenanceFinishedTime, 'hh:mm a : LLL dd yyyy')}] Maintenance finished\n(Maintenance duration: ${JSON.stringify(duration, null, 2)})\n`);

    appState.maintenance = false;
  }
};

// const takeAdump = () => new Promise((resolve, reject) => {

// });

const scrapeToday = async () => {
  if (appState.maintenance || appState.scrapingToday) {
    return;
  }

  try {
    appState.scrapingToday = true;
    const datesToScrape = [];

    let date = dateToday || new Date();
    while (datesToScrape.length < 2) {
      if (!isWeekend(date)) {
        datesToScrape.push(date);
      }
      date = subDays(date, 1);
    }

    for (const date of datesToScrape) {
      const formattedDate = format(date, 'dd-LLL-yyyy');
      const currentData = await getData(formattedDate) || { date: formattedDate, existsInBSP: true, scraped: false };
      if (!currentData.data) {
        await scrapeProcess(date, true, true);
      }
    }
    appState.scrapingToday = false;
  } catch (err) {
    appState.scrapingToday = false;
  }
};