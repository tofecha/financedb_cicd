import { mongoose } from '../config/db.js';
import pkg from 'mongoose';
import { RoleSchema } from './rolesModel.js';

const { Schema } = pkg;

const UserSchema = new mongoose.Schema({
  userId: {
    type: Number,
    index: true,
    unique: true
  },
  role: RoleSchema,
  currentWorkspace: {
    type: Schema.Types.ObjectId,
    required: false,
    ref: 'WorkspaceConfig'
  }
});

const UserModel = mongoose.model('User', UserSchema);

export {
  UserSchema
};
export default UserModel;