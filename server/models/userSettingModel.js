import { mongoose } from '../config/db.js';

const UserSettingSchema = new mongoose.Schema({
  // darkMode: {
  //   type: Boolean,
  //   default: false
  // },
  // timeZone: {
  //   type: String,
  //   default: 'Asia/Manila'
  // }
});

const UserSettingModel = mongoose.model('UserSetting', UserSettingSchema);

export {
  UserSettingSchema
};
export default UserSettingModel;
