import { mongoose } from '../config/db.js';

const FinanceReportModelSchema = new mongoose.Schema({
  date: { type: String, index: true, unique: true },
  sales_collection: Number,
  target_revenue: Number,
  actual_revenue: Number,
  regular: Number,
  adhoc: Number,
  pending_launch: Number,
  clients: [{
    client_name: { type: String },
    payment: Number
  }],
  operating_costs: [{
    operation_name: { type: String },
    cost: Number
  }],
  fixed_assets: [{
    asset_name: { type: String },
    cost: Number
  }],
});

const FinanceReportModel = mongoose.model('FinanceReport', FinanceReportModelSchema);

export {
  FinanceReportModelSchema
};
export default FinanceReportModel;