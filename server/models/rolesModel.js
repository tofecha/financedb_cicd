import { mongoose } from '../config/db.js';
import { PermissionSchema } from './permissionsModel.js';

const RoleSchema = new mongoose.Schema({
  role: {
    type: String,
    index: true,
    unique: true,
    required: true,
  },
  permissions: [PermissionSchema]
});

const RoleModel = mongoose.model('Role', RoleSchema);

const ROLES = {
  ADMIN: 'admin',
  FINANCE: 'finance',
  EXECUTIVE: 'executive',
  DEVELOPER: 'developer',
  VISITOR: 'visitor',
};

Object.freeze(ROLES);

const initRoles = () => new Promise((resolve, reject) => {
  RoleModel.bulkWrite([{
    role: ROLES.ADMIN,
    permissions: []
  }, {
    role: ROLES.FINANCE,
    permissions: []
  }, {
    role: ROLES.EXECUTIVE,
    permissions: []
  }, {
    role: ROLES.DEVELOPER,
    permissions: []
  }, {
    role: ROLES.VISITOR,
    permissions: []
  }].map((data) => ({
    updateOne: {
      filter: { role: data.role },
      update: data,
      upsert: true
    }
  })), (err) => {
    if (err) reject();
    else resolve();
  });
});

export {
  ROLES,
  initRoles,
  RoleSchema
};
export default RoleModel;