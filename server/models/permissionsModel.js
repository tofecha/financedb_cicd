import { mongoose } from '../config/db.js';

const PermissionSchema = new mongoose.Schema({
  permission: {
    type: String,
    index: true,
    unique: true,
    required: true,
  }
});

const PermissionModel = mongoose.model('Permission', PermissionSchema);

const PERMISSIONS = {
  CAN_VIEW_FOREX_PAGE: 'can_view_forex_page',
  CAN_VIEW_DASHBOARD_REPORTS_PAGE: 'can_view_dashboard_reports_page',
  CAN_VIEW_WORKSPACE_PAGE: 'can_view_workspace_page',
  CAN_VIEW_DEVELOPER_PAGE: 'can_view_developer_page',
  CAN_VIEW_ADMIN_SETTINGS_PAGE: 'can_view_admin_settings_page',
  CAN_MODIFY_DASHBOARD_REPORTS: 'can_modify_dashboard_reports',
  CAN_MODIFY_ROLES_PERMISSIONS: 'can_modify_roles_permissions',
  CAN_DOWNLOAD_REPORTS: 'can_download_reports',
};

Object.freeze(PERMISSIONS);

const initPermissions = () => new Promise((resolve, reject) => {
  PermissionModel.bulkWrite([{
    permission: PERMISSIONS.CAN_VIEW_FOREX_PAGE
  }, {
    permission: PERMISSIONS.CAN_VIEW_DASHBOARD_REPORTS_PAGE
  }, {
    permission: PERMISSIONS.CAN_VIEW_WORKSPACE_PAGE
  }, {
    permission: PERMISSIONS.CAN_VIEW_DEVELOPER_PAGE
  }, {
    permission: PERMISSIONS.CAN_VIEW_ADMIN_SETTINGS_PAGE
  }, {
    permission: PERMISSIONS.CAN_MODIFY_DASHBOARD_REPORTS
  }, {
    permission: PERMISSIONS.CAN_MODIFY_ROLES_PERMISSIONS
  }, {
    permission: PERMISSIONS.CAN_DOWNLOAD_REPORTS
  }].map((data) => ({
    updateOne: {
      filter: { permission: data.permission },
      update: data,
      upsert: true
    }
  })), (err) => {
    if (err) {
      console.error(err);
      reject();
    } else {
      resolve();
    }
  });
});

export {
  PERMISSIONS,
  initPermissions,
  PermissionSchema
};
export default PermissionModel;