import { mongoose } from '../config/db.js';

const ForexSchema = new mongoose.Schema({
  date: { type: String, index: true, unique: true },
  existsInBSP: Boolean,
  scraped: Boolean,
  data: [{
    symbol: String,
    EUR: Number,
    USD: Number,
    PHP: Number
  }]
});

const ForexModel = mongoose.model('Forex', ForexSchema);

export {
  ForexSchema
};
export default ForexModel;