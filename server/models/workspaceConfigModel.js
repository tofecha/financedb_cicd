import { mongoose } from '../config/db.js';
import { UserSchema } from './userModel.js';

const WorkspaceConfigSchema = new mongoose.Schema({
  workspaceConfigId: { type: String, index: true, unique: true },
  name: String,
  layout: Object,
  items: [Object],
  userId: { type: Number }
});

const WorkspaceConfigModel = mongoose.model('WorkspaceConfig', WorkspaceConfigSchema);

export {
  WorkspaceConfigSchema
};
export default WorkspaceConfigModel;