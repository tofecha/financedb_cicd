import { PdfReader } from 'pdfreader';

const pdfReader = new PdfReader();

const scrapper = (filePath) => new Promise((resolve, reject) => {
  if (filePath) { // also check is file does exist
    let currentPage = 0;
    const scraped = [];
    pdfReader.parseFileItems(filePath, (err, item) => {
      if (err) {
        reject({
          error: '[scraper] : parseFileItems error',
          data: err
        });
      } else if (!item) {
        resolve(scraped);
      } else if (item.page) {
        currentPage = item.page;
      } else {
        if (currentPage === 1) {
          scraped.push(item.text);
        }
      }
    });  
  } else {
    reject({
      error: '[scraper] : filePath invalid',
      data: filePath
    });
  }
});

export {
  scrapper
};