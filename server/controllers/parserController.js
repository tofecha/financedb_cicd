import lodash from 'lodash';
const { clone, find, findIndex, isArray, isEmpty } = lodash;

const listOfCurrencies = [
  'USD', 'JPY', 'GBP', 'HKD',
  'CHF', 'CAD', 'SGD', 'AUD', 'BHD',
  'KWD', 'SAR', 'BND', 'IDR', 'THB',
  'AED', 'EUR', 'KRW', 'CNY', 'ARS',
  'BRL', 'DKK', 'INR', 'MYR', 'MXN',
  'NZD', 'NOK', 'PKR', 'ZAR', 'SEK',
  'SYP', 'TWD', 'VEB'
];

const parser = (data) => new Promise((resolve, reject) => {
  if (isArray(data) && !isEmpty(data)) {
    const scrapedData = clone(data);
    const parsedData = [];

    try {
      listOfCurrencies.forEach((currency) => {
        const index = findIndex(scrapedData, (data) => (data === currency));
        if (index !== -1) {
          if (scrapedData[index] === 'KWD') {
            parsedData.push({
              symbol: 'KWD',
              EUR: 0,
              USD: 0,
              PHP: 0
            });
          } else {
            parsedData.push({
              symbol: scrapedData[index],
              EUR: scrapedData[index+1] === 'N/A' ? 0 : parseFloat(scrapedData[index+1]),
              USD: scrapedData[index+2] === 'N/A' ? 0 : parseFloat(scrapedData[index+2]),
              PHP: scrapedData[index+3] === 'N/A' ? 0 : parseFloat(scrapedData[index+3])
            });
          }
        }
      });

      
      // generate for PHP
      if (parsedData) {
        parsedData.push({
          symbol: 'PHP',
          EUR: parseFloat((1 / find(parsedData, ['symbol', 'EUR']).PHP).toFixed(6)),
          USD: parseFloat((1 / find(parsedData, ['symbol', 'USD']).PHP).toFixed(6)),
          PHP: 1
        });
      }

      resolve(parsedData);
    } catch (err) {
      reject({
        error: '[parser] : caught error',
        data: err
      });
    }
  } else {
    reject({
      error: '[parser] : data is empty or not an array.',
      data: `data typeof : ${typeof data} `
    });
  }
});

export {
  parser
};