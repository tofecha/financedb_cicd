import lodash from 'lodash';
import RoleModel, { ROLES } from '../models/rolesModel.js';
import UserModel from '../models/userModel.js';

const { map } = lodash;

export const initializeUser = async (userAccount) => {
  const role = await RoleModel.findOne({ role: userAccount.owner ? ROLES.ADMIN : ROLES.VISITOR }).exec();
  const fdbUserAccount = await UserModel.create({
    userId: userAccount.user_account_id,
    role: role,
    currentWorkspace: null
  }).exec();
  return fdbUserAccount;
};

export const getUser = async (userAccount) => {
  const fdbUserAccount = await UserModel.findOne({ userId: userAccount.user_account_id }).exec();
  if (fdbUserAccount) return fdbUserAccount;
  else await initializeUser(userAccount);
};

export const getAllUser = async () => {
  return await UserModel.find({}).exec();
};

export const updateUser = (req) => new Promise((resolve, reject) => {
  const { user, body } = req;
  UserModel.updateOne(
    { userId: user.userId },
    { ...user, ...body },
    { upsert: true },
    (err) => {
      if (err) reject(err);
      else {
        getUser(user.userId).then((doc) => (
          resolve(doc)
        )).catch((err) => (
          reject(err)
        ));
      }
    });
});
