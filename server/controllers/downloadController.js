import bspApi from '../config/bspApi.js';
import fs from 'fs-extra';
import path from 'path';
import lodash from 'lodash';
const { includes } = lodash;

const dataPiper = (data, filename) => new Promise((resolve, reject) => {
  const filePath = path.resolve(fs.realpathSync('.'), 'pdf', filename);
  const file = fs.createWriteStream(filePath);  
  file.on('finish', () => {
    if (includes(fs.readFileSync(filePath, { encoding: 'utf8'}), 'Your requested page is not available.')) {
      fs.unlinkSync(filePath);
      resolve({
        existsInBSP: false
      });
    } else {
      resolve({
        file_path: filePath,
        existsInBSP: true
      });
    }
  });
  file.on('error', (err) => reject(err));

  data.pipe(file);
});

const downloader = (url) => new Promise((resolve, reject) => {
  bspApi.get(url, {}, { responseType: 'stream' }).then((response) => {
    if (response.ok) {
      resolve(response.data);
    } else {
      reject({
        error: 'download_error',
        data: response
      });
    }
  }).catch((err) => {
    reject({
      error: 'download_exception',
      data: err
    });
  });
});

export {
  downloader,
  dataPiper
};