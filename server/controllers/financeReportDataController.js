import { addMonths, getQuarter, startOfYear, format, parseISO, subMonths } from 'date-fns';
import lodash from 'lodash';
import FinanceReportModel from '../models/financeReportModel.js';
const { map } = lodash;

// format 'yyyy-MM-01'
const quarters = {};
let startMonth = startOfYear(new Date());
for (let x = 0; x < 12; x++) {
  const quarter = getQuarter(startMonth);
  if (!quarters[quarter]) {
    quarters[quarter] = [];
  }
  quarters[quarter].push(format(startMonth, 'MM'));
  startMonth = addMonths(startMonth, 1);
}

const dateFormat = 'yyyy-MM-01';

const getData = (date) => new Promise((resolve, reject) => {
  let parsedDate;
  try {
    parsedDate = parseISO(date);
  } catch (err) {
    reject('Cannot parse date');
  }

  FinanceReportModel.findOne({ date: format(parsedDate, dateFormat) }).exec((err, doc) => {
    if (err) reject(err);
    else resolve(doc);
  });
});

const getDataFrom = (date, limit) => new Promise((resolve, reject) => {
  let parsedDate;
  let dates = [];
  try {
    parsedDate = parseISO(date);
    let counter = 0;
    while(counter < parseInt(limit)) {
      dates.push(format(parsedDate, dateFormat));
      parsedDate = subMonths(parsedDate, 1);
      counter++;
    }
  } catch (err) {
    reject('Cannot parse date');
  }

  FinanceReportModel.find({ date: { '$in' : dates } }).exec((err, doc) => {
    if (err) reject(err);
    else resolve(doc);
  });
});

const getQuarterData = (quarter, year) => new Promise((resolve, reject) => {
  const dateQuery = map(quarters[quarter], (month) => (
    `${year}-${month}-01`
  ));
  FinanceReportModel.find({ date: { '$in' : dateQuery } }).exec((err, docs) => {
    if (err) reject(err);
    else resolve(docs);
  });
});

const upsertData = (data) => new Promise((resolve, reject) => {
  let parsedDate;
  try {
    parsedDate = parseISO(data.date);
  } catch (err) {
    reject('Cannot parse date');
  }

  FinanceReportModel.updateOne({ date: format(parsedDate, dateFormat) }, {
    ...data,
    date: format(parsedDate, dateFormat)
  }, { upsert: true, }, (err) => {
    if (err) {
      console.error(err);
      reject(err);
    } else {
      resolve();
    }
  });
});

const checkIfDataExist = async (date) => {
  try {
    const result = await getData(date);
    if (result) {
      return true;
    }
  } catch (err) {
    return false;
  }
};

const getMonthsWithData = () => new Promise((resolve, reject) => {
  FinanceReportModel.find({}, {
    date: 1,
    _id: 0
  }).exec((err, docs) => {
    if (err) reject(err);
    else {
      const arrayOfDates = docs.map(curDate => curDate.date).sort();
      resolve(arrayOfDates);
    }
  });
});

export {
  getData,
  getDataFrom,
  upsertData,
  getQuarterData,
  getMonthsWithData,
  checkIfDataExist
};