import fs from 'fs-extra';
import path from 'path';
import { reportError } from './loggingController.js';

const unlink = (filePath) => {
  try {
    if (fs.existsSync(filePath)) {
      fs.unlinkSync(filePath);
    }
  } catch (err) {
    reportError(new Date(), 'unlink', err);
  }
};

const clearPdfFolder = () => {
  try {
    const pdfPath = path.resolve(fs.realpathSync('.'), 'pdf');
    console.log('clearing pdf folder with path', pdfPath);
    fs.emptyDirSync(pdfPath);
  } catch (err) {
    reportError(new Date(), 'unlink', err);
  }
};

export {
  unlink,
  clearPdfFolder
};