import logger from '../config/winston.js';

const getLogs = (options = {
  from: new Date() - (24 * 60 * 60 * 1000),
  until: new Date(),
  limit: 10,
  start: 0,
  order: 'desc',
  fields: ['message']
}) => new Promise((resolve, reject) => {
  logger.query(options, (err, results) => {
    if (err) {
      reject(err);
    } else {
      resolve(results);
    }
  });
});

const reportError = (dateTime, func, err) => {
  logger.error({
    dateTime,
    func,
    err
  });
};

export {
  reportError,
  getLogs
};