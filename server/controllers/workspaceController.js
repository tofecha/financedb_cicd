import lodash from 'lodash';
// import { v4 as uuidv4 } from 'uuid';
import WorkspaceConfigModel from '../models/workspaceConfigModel.js';

export const getAllWorkspace = () => new Promise((resolve, reject) => {
  WorkspaceConfigModel.find({}).select({
    workspaceConfigId: 1,
    name: 1,
    author: 1
  }).exec((err, docs) => {
    if (err) reject(err);
    else resolve(docs);
  });
}); 

export const getWorkspace = (id) => new Promise((resolve, reject) => {
  WorkspaceConfigModel.findOne({ workspaceConfigId: id }).exec((err, doc) => {
    if (err) reject(err);
    else resolve(doc);
  });
});

export const upsertWorkspace = (req) => new Promise((resolve, reject) => {
  const { body, user } = req;
  const { workspaceConfigId } = body;
  console.log('upserted new workspace');
  WorkspaceConfigModel.updateOne(
    { workspaceConfigId: workspaceConfigId },
    { workspaceConfigId: workspaceConfigId, ...body, userId: user?.platform_account?.userId || null },
    { upsert: true },
    (err) => {
      if (err) reject(err);
      else {
        getWorkspace(workspaceConfigId).then((doc) => (
          resolve(doc)
        )).catch((err) => (
          reject(err)
        ));
      }
    });
});

export const deleteWorkspace = (req) => new Promise((resolve, reject) => {
  const { body } = req;
  const { workspaceConfigId } = body;
  WorkspaceConfigModel.deleteOne({ workspaceConfigId: workspaceConfigId }).exec((err) => {
    if (err) reject(err);
    else resolve();
  });
});