import { format, isSameDay, isWithinInterval, subDays } from 'date-fns';
import path from 'path';
import fs from 'fs';
import { checkIfFileAvailable, getData, upsertData } from './databaseController.js';
import { scrapper } from './scrapperController.js';
import { parser } from './parserController.js';
import { dataPiper, downloader } from './downloadController.js';
import lodash from 'lodash';
import { reportError } from './loggingController.js';
const { find, isEmpty } = lodash;

const newDateFormatApr62021 = new Date('2021 Apr 6');
let weekStartDate = undefined;
let dateToday = undefined;

const scrapeProcess = async (date, force, saveAfter = true) => {
  let urlDateFormat = undefined;
  let downloadedData = undefined;
  let pipedData = undefined;
  let currentData = undefined;
  const formattedDate = format(date, 'dd-LLL-yyyy');

  if (date >= newDateFormatApr62021 || isSameDay(date, newDateFormatApr62021)) {
    urlDateFormat = `/statistics/rerb/${format(date, 'ddLLLyyyy')}.pdf`;
  } else {
    urlDateFormat = `/statistics/rerb/${format(date, 'dd%20LLL%20yyyy')}.pdf`;
  }

  try {
    currentData = await getData(formattedDate) || { date: formattedDate, existsInBSP: true, scraped: false };
  } catch (error) {
    currentData = { date: formattedDate, existsInBSP: true, scraped: false };
  }

  if (force) {
    currentData.scraped = false;
    currentData.data = undefined;
  }
  
  if ((isWithinInterval(date, { start: weekStartDate, end: dateToday })) && currentData.existsInBSP === false) {
    const doesItExist = await checkIfFileAvailable(urlDateFormat);
    currentData.existsInBSP = doesItExist;
    if (doesItExist) {
      currentData.scraped = false;
    }
  }

  // scrapping failed since USD didnt exists set to rescrapping with inverted version
  if (currentData.data && !find(currentData.data, ['symbol', 'USD'])) {
    currentData.data = undefined;
    currentData.scraped = false;
  }

  let filePath = null;

  if (
    (currentData.existsInBSP && !currentData.scraped) || (currentData.existsInBSP && currentData.scraped && !currentData.data)
  ) {
    console.log('scraping this ', currentData.date, 'because', (currentData.existsInBSP && !currentData.scraped), (currentData.existsInBSP && currentData.scraped && !currentData.data));
    try {
      // check if file exists
      filePath = path.resolve(fs.realpathSync('.'), 'pdf', `${formattedDate}.pdf`);
      if (!fs.existsSync(filePath)) {
        downloadedData = await downloader(urlDateFormat);
        pipedData = await dataPiper(downloadedData, `${formattedDate}.pdf`);
        currentData.existsInBSP = pipedData.existsInBSP;
      } else {
        pipedData = {
          file_path: filePath,
          existsInBSP: true
        };
        currentData.existsInBSP = true;
      }      
    } catch (err) {
      reportError(new Date(), 'scrapeProcess, downloader, piper', currentData, err);
    }

    // test for scrapping
    try {
      if (pipedData.existsInBSP) {
        const scraperResult = await scrapper(pipedData.file_path);
        const parserResult = await parser(scraperResult);
        if (isEmpty(parserResult)) {
          currentData.data = [];
          currentData.scraped = false;
        } else {
          currentData.data = parserResult;
          currentData.scraped = true;
        }
      }
    } catch (err) {
      reportError(new Date(), 'scrapeProcess, scrapper, parser', currentData, err);
    }

  }

  if (currentData.existsInBSP && currentData.scraped && !currentData.data) {
    currentData.scraped = false;
  }

  // upsert to data beast
  if (saveAfter) {
    try {
      await upsertData(currentData);
    } catch (err) {
      reportError(new Date(), 'scrapeProcess upsertData', currentData, err);
    }
  }

  return currentData;
};

const setupDate = (date) => {
  dateToday = date;
  weekStartDate = subDays(date, 7);
};

export {
  dateToday,
  scrapeProcess,
  setupDate
};