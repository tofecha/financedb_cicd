import bspApi from '../config/bspApi.js';
import { format } from 'date-fns';
import lodash from 'lodash';
import ForexModel from '../models/forexModel.js';

const { includes } = lodash;

const getData = (date) => new Promise((resolve, reject) => {
  ForexModel.findOne({ date: date }, (err, doc) => {
    if (err) reject(err);
    else resolve(doc);
  });
});

const upsertData = (data) => new Promise((resolve, reject) => {
  ForexModel.updateOne({ date: data.date }, data, { upsert: true }, (err) => {
    if (err) reject(err);
    else resolve();
  });
});

const checkIfFileAvailable = (url) => new Promise((resolve, reject) => {
  bspApi.get(url, {}).then((response) => {
    if (response.ok) {
      if (includes(response.data, 'Your requested page is not available.')) {
        resolve(false);
      } else {
        resolve(true);
      }
    } else reject({
      error: 'download_error',
      data: response
    });
  }).catch((err) => {
    reject({
      error: 'download_exception',
      data: err
    });
  });
});

const findData = (date) => new Promise((resolve) => {
  ForexModel.findOne({ date: format(date, 'dd-LLL-yyyy'), existsInBSP: true, scraped: true }, (err, doc) => {
    if (err) resolve(undefined);
    else resolve(doc);
  });
});

const saveToDatabase = async (req, res) => {
  const formattedDate = format(new Date(req.body.date), 'dd-LLL-yyyy');
  const oldData = await getData(formattedDate);
  await upsertData({
    ...oldData,
    ...req.body,
    existsInBSP: true,
    scraped: true,
  });
  res.sendStatus(200);
};



export {
  getData,
  upsertData,
  checkIfFileAvailable,
  findData,
  saveToDatabase
};