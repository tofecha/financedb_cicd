import { addDays, format, subDays } from 'date-fns';
import lodash from 'lodash';
import { findData } from './databaseController.js';
const { find, includes } = lodash;

const startDate = new Date('2018-01-01');

const generate = async (query = new Date(), base = 'USD', symbols = 'PHP', fullData = false) => {
  let date = query;
  let nestData = undefined;

  if (fullData) {
    nestData = await findData(date);
    return nestData;
  } else {
    while (date > startDate) {
      nestData = await findData(date);
      if (nestData) break;
      date = subDays(date, 1);
    }
    
    if (!nestData) {
      return {
        error: `There is no data for dates older than ${format(date, 'yyyy-MM-dd')}.`
      };
    }

    const rates = generateRates(base, symbols, format(date, 'yyyy-MM-dd'), nestData.data);
    if (rates.error) {
      return rates;
    } else {
      return {
        base,
        rates: rates,
        date: format(date, 'yyyy-MM-dd')
      };
    }
  }
};

const generateRates = (base, symbols, date, data) => {
  const baseData = find(data, ['symbol', base]);
  const symbolData = find(data, ['symbol', symbols]);

  if (includes(['USD', 'EUR', 'PHP'], symbols) && baseData) {
    return {
      [symbols] : baseData[symbols]
    };
  } else {
    // condition for PHP
    if (baseData && baseData['USD'] !== 0 && symbolData && symbolData['USD'] !== 0) {
      return {
        [symbols] : (baseData['USD'] / symbolData['USD'])
      };
    } else {
      return {
        error: !baseData || baseData['USD'] === 0 ? `Base '${base}' is not supported.` : `Symbols '${symbols}' are invalid for date ${date}.`
      };
    }
  }
};

const getHistoricalRates = async (base, symbols, startDate, endDate) => {
  let date = new Date(startDate);
  let end = new Date(endDate);
  const generatedRates = {};

  while(date <= end) {
    const nestData = await findData(date);
    if (nestData) {
      const formattedDate = format(date, 'yyyy-MM-dd');
      const rates = generateRates(base, symbols, formattedDate, nestData.data);
      if (!rates.error) {
        generatedRates[formattedDate] = rates;
      }
    }
    date = addDays(date, 1);
  }

  return generatedRates;
};

export {
  generate,
  generateRates,
  getHistoricalRates,
};