import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Container, Divider, Grid, Paper, Toolbar } from '@material-ui/core';
import BreadcrumbsRouter from '../components/BreadcrumbsRouter';
import AdminUserManagementPanel from '../components/AdminUserManagementPanel/AdminUserManagementPanel';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
  },
  content: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    width: 'calc(100% - 180px)',
    padding: theme.spacing(2)
  },
  contentDrawer: {
    position: 'sticky',
    top: (theme.mixins.toolbar.minHeight * 2) + 32,
    height: `calc(100vh - ${(theme.mixins.toolbar.minHeight * 2)}px - 32px)`,
    width: 180,
    padding: theme.spacing(1),
    display: 'block'
  },
}));

const Admin = () => {
  const classes = useStyles();

  return (
    <Fragment>
      <Toolbar variant='dense'>
        <BreadcrumbsRouter />
      </Toolbar>
      <Divider />
      <Container maxWidth='md' className={classes.container}>
        <Paper variant='outlined' square className={classes.content}>
          <Grid container spacing={2}>
            <Grid item md={12}>
              <Box height={400}>
                <AdminUserManagementPanel />
              </Box>
            </Grid>
            <Grid item md={12}>
              <Box height={400}>
                {/* <RolesAndPermissionsPanel
                  fetchPermissions={() => ({
                    'CAN_UPDATE_PUBLIC_DASHBOARD': 'can-update-public-dashboard',
                    'CAN_GENERATE_REPORTS': 'can-generate-reports'
                  })}
                  fetchRoles={() => ({
                    'SUPER_ADMIN': 'super-admin',
                    'DEFAULT': 'default'
                  })}
                  fetchCurrentRolePermissions={() => ([{
                    role_name: 'super-admin',
                    permissions: ['can-update-public-dashboard', 'can-generate-reports']
                  }, {
                    role_name: 'default',
                    permissions: []
                  }])}
                /> */}
              </Box>
            </Grid>
          </Grid>
        </Paper>
        <nav className={classes.contentDrawer}>
          I am the nav bar on the right supposedly
        </nav>
      </Container>
    </Fragment>
  );
};

export default Admin;