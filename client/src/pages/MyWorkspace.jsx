import { Box } from '@material-ui/core';
import React, { lazy } from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import ErrorFallback from '../components/ErrorFallback';
import { retry } from '../helper/retryHelper';

const AsyncWorkspaceSystem = lazy(() => retry(() => import('../components/WorkspaceComponents/WorkspaceSystem'), 5, 3000));

const MyWorkspace = () => {
  return (
    <Box display='flex' justifyContent='center'>
      <ErrorBoundary
        FallbackComponent={ErrorFallback}
        onError={(props) => {
          console.error('ErrorBoundary', props);
        }}
      >
        <AsyncWorkspaceSystem />
      </ErrorBoundary>
    </Box>
  );
};

export default MyWorkspace;