import { Button, Box } from '@material-ui/core';
import React, {  } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import authApi from '../config/authApi';

const useStyles = makeStyles((theme) => ({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  },
}));

const Sandbox = () => {
  const fetchUsers = async () => {
    const users = await authApi.get('/client-user/pending-users');
  };

  return (
    <Box>
      <Button onClick={fetchUsers}>
        Fetch Users
      </Button>
    </Box>
  );
};

export default Sandbox;