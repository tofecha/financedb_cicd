import { Box, Button, Container, Divider, Grid, Paper, Toolbar, Typography, InputAdornment, IconButton, Popover, TextField } from '@material-ui/core';
import React, { useRef, useState, lazy } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { GetApp as GetAppIcon, Edit } from '@material-ui/icons';
import { useAppStore } from '../stores/appStore';
import { first, last, map } from 'lodash-es';
import { format, isSameDay, parseISO } from 'date-fns';
import { useNavigate } from '@reach/router';
import BreadcrumbsRouter from '../components/BreadcrumbsRouter';
import ReportDataBox from '../components/ReportDataBox';
import { Fragment } from 'react';
import { ThemeProvider as MaterialThemeProvider } from '@material-ui/core/styles';
import { lightMatTheme } from '../config/matThemes';
import { ArrowBack, ArrowForward } from '@material-ui/icons';
import { addMonths, subMonths } from 'date-fns';
import { retry } from '../helper/retryHelper';
import { getMonthsWithData, reportDateFormat } from '../config/bspApi';
import { CustomDateSelector } from '../components/CustomDateSelector';
import { useQuery } from 'react-query';

const PDFGenerator = lazy(() => import('../components/PDFGenerator'));

const BarAndLineGraph = lazy(() => retry(() => import('../components/Graphs/BarAndLineGraph'), 5, 5000));
const RevenueBarGraph = lazy(() => retry(() => import('../components/Graphs/RevenueBarGraph'), 5, 5000));
const SalesCollectionGraph = lazy(() => retry(() => import('../components/Graphs/SalesCollectionGraph'), 5, 5000));
const PercentageEarningBeforeIncomeTaxGraph = lazy(() => retry(() => import('../components/Graphs/PercentageEarningBeforeIncomeTaxGraph'), 5, 5000));
const ListTable = lazy(() => retry(() => import('../components/Graphs/ListTable'), 5, 5000));

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    overflow: 'hidden',
    minHeight: 200
  },
  toolbarSpacer: {
    '& > *' : {
      marginRight: theme.spacing(1)
    }
  },
  graphLabel: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: '0.8rem',
    textTransform: 'uppercase',
  },
  headerLabel: {
    marginBottom: theme.spacing(1)
  }
}));

const Dashboard = () => {
  const classes = useStyles();
  const pdfRef = useRef(undefined);
  const [ store, actions] = useAppStore();

  const navigate = useNavigate();
  const [ hideUIForPDF, setHideUIForPDF ] = useState(false);
  // const [ limit, setLimit ] = useState(3);
  const [ isFetching, setFetching ] = useState(false);
  const [ date, setDate ] = useState(new Date());

  const [ monthsWithData, setMonthsWithData ] = useState([]);

  const [ minDate, setMinDate ] = useState(null);
  const [ maxDate, setMaxDate ] = useState(null);

  const [anchorEl, setAnchorEl] = useState(null);

  const queryMonthsWithData = useQuery(['getMonthsWithData'], () => (
    getMonthsWithData()
  ), {
    cacheTime: 5000,
    onSuccess: (data) => {
      setMinDate(parseISO(first(data)));
      setMaxDate(parseISO(last(data)));
      setDate(parseISO(last(data)));
      setMonthsWithData(data);
    }
  });

  const handleOnClickDownloadPDF = (toPdf) => {
    actions.showPageStateLoading();
    setHideUIForPDF(true);
    setTimeout(() => {
      toPdf();
    }, 2000);
  };

  const handleOnPDFGenerationComplete = () => {
    actions.hidePageStateLoading();
    setHideUIForPDF(false);
  };

  const handleRedirectToDateEditor = () => {
    navigate('/dashboard/editor');
  };

  const onDateArrowClick = (e, action) => {
    e.stopPropagation();
    if (action === 'next') {
      const newDate = addMonths(date, 1);
      if (newDate > maxDate) {
        setDate(maxDate);
      } else {
        setDate(newDate);
      }
    } else {
      const newDate = subMonths(date, 1);
      setDate(newDate);
    }
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDateChange = (date) => {
    setDate(date);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'popover' : undefined;
 
  return (
    <Fragment>
      <Toolbar variant='dense'>
        <BreadcrumbsRouter />
      </Toolbar>
      <Divider />
      <Box m={3} mt={1}>
        <Paper variant='outlined' className={classes.toolbarPaper}>
          <Toolbar variant='dense' className={classes.toolbarSpacer}>
            <Button variant='contained' size='small' startIcon={(
              <Edit />
            )} onClick={handleRedirectToDateEditor}>
              Reports Editor
            </Button>
            <Box flexGrow={1} />
            <Fragment>
              <TextField
                onClick={handleClick}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position='start'>
                      <IconButton size='small' edge='start' disabled={isFetching || minDate === format(date, reportDateFormat)} onClick={(e) => onDateArrowClick(e, 'prev')}>
                        <ArrowBack/>
                      </IconButton>
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment position='end'>
                      <IconButton size='small' edge='end' disabled={isFetching || isSameDay(date, maxDate)} onClick={(e) => onDateArrowClick(e, 'next')}>
                        <ArrowForward/>
                      </IconButton>
                    </InputAdornment>
                  )
                }}
                size='small'
                margin='dense'
                variant='standard'
                value={format(date, 'LLL yyyy')}
                style={{
                  width: 160
                }}
                disabled={isFetching}
              />
            </Fragment>
            <PDFGenerator
              targetRef={pdfRef}
              filename={`report-${format(new Date(),'yyyy-MM-dd-hh-mm-ss')}`}
              scale={2}
              options={{
                orientation: 'l',
                unit: 'px',
                format: [2863, 1600],
                margin: 10,
                compressPdf: true
              }}
              onComplete={handleOnPDFGenerationComplete}
            >
              {({ toPdf, isProcessing }) => (
                <Button disabled={isProcessing} variant='contained' size='small' startIcon={(
                  <GetAppIcon />
                )} onClick={() => {
                  handleOnClickDownloadPDF(toPdf);
                }}>
                  { isProcessing ? 'Generating PDF' : 'Download as PDF'}
                </Button>
              )}
            </PDFGenerator>
          </Toolbar>
        </Paper>

        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <CustomDateSelector date={date} dates={monthsWithData} onDateChange={handleDateChange} />
        </Popover>

        <MaterialThemeProvider theme={lightMatTheme}>
          <Container maxWidth='lg'>
            <Box my={2} ref={pdfRef}>
              <Grid container spacing={2} justifyContent='center'>
                <Grid item xs={12}>
                  <Typography variant='h4' color={hideUIForPDF ? 'textSecondary' : 'initial'}>Sales Report</Typography>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Paper variant='outlined' className={classes.root} >
                    <ReportDataBox date={date} printMode={hideUIForPDF} limit={3} label='Total Monthly Sales Collection'>
                      {({ generatedData, isFetching }) => (
                        <SalesCollectionGraph isFetching={isFetching} data={generatedData} />
                      )}
                    </ReportDataBox>
                  </Paper>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Paper variant='outlined' className={classes.root} >
                    <ReportDataBox
                      date={date}
                      printMode={hideUIForPDF}
                      limit={3}
                      label='Actual vs Target Revenue'
                    >
                      {({ generatedData, isFetching }) => (
                        <RevenueBarGraph isFetching={isFetching} data={generatedData} />
                      )}
                    </ReportDataBox>
                  </Paper>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Paper variant='outlined' className={classes.root} >
                    <ReportDataBox date={date} printMode={hideUIForPDF} limit={3} label='Top Clients'>
                      {({ generatedData }) => (
                        <Grid container spacing={1}>
                          {
                            map(generatedData, (monthlyData) => {
                              return (
                                <Grid key={`client-list-${monthlyData.date}`} item xs={4}>
                                  <Typography align='center' className={classes.graphLabel}>{ format(parseISO(monthlyData.date), 'LLL yyyy') }</Typography>
                                  <Divider />
                                  <ListTable
                                    rows={monthlyData?.clients}
                                    readOnly={true}
                                    labelHeader='Client'
                                    valueLabel='Invoiced'
                                    labelKey='client_name'
                                    valueKey='payment'
                                  />
                                </Grid>
                              );
                            })
                          }
                        </Grid>
                      )}
                    </ReportDataBox>
                  </Paper>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Paper variant='outlined' className={classes.root} >
                    <ReportDataBox date={date} printMode={hideUIForPDF} limit={3} label='Top Operating Costs'>
                      {({ generatedData }) => (
                        <Grid container spacing={1}>
                          {
                            map(generatedData, (monthlyData) => {
                              return (
                                <Grid key={`client-list-${monthlyData.date}`} item xs={4}>
                                  <Typography align='center' className={classes.graphLabel}>{ format(parseISO(monthlyData.date), 'LLL yyyy') }</Typography>
                                  <Divider />
                                  <ListTable
                                    rows={monthlyData?.operating_costs}
                                    readOnly={true}
                                    labelHeader='Operation'
                                    valueLabel='Cost'
                                    labelKey='operation_name'
                                    valueKey='cost'
                                  />
                                </Grid>
                              );
                            })
                          }
                        </Grid>
                      )}
                    </ReportDataBox>
                  </Paper>
                </Grid>

              </Grid>
              <Grid container spacing={2} justifyContent='center'>
                <Grid item xs={12}>
                  <Typography variant='h4' color={hideUIForPDF ? 'textSecondary' : 'initial'}>Earnings and Expenses Report</Typography>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Paper variant='outlined' className={classes.root} >
                    <ReportDataBox date={date} printMode={hideUIForPDF} limit={3} label='Monthly Costs and Earnings'>
                      {({ generatedData, isFetching }) => (
                        <BarAndLineGraph isFetching={isFetching} data={generatedData} />
                      )}
                    </ReportDataBox>
                  </Paper>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Paper variant='outlined' className={classes.root} >
                    <ReportDataBox date={date} printMode={hideUIForPDF} limit={3} label='Percentage of Earnings Before Income Tax'>
                      {({ generatedData, isFetching }) => (
                        <PercentageEarningBeforeIncomeTaxGraph isFetching={isFetching} data={generatedData} />
                      )}
                    </ReportDataBox>
                  </Paper>
                </Grid>
                <Grid container item xs={12} justifyContent='center'>
                  <Grid item xs={hideUIForPDF ? 4 : 12}>
                    <Paper variant='outlined' className={classes.root} >
                      <ReportDataBox date={date} printMode={hideUIForPDF} minLimit={1} limit={1} label={'Fixed Assets'}>
                        {({ generatedData }) => (
                          <Grid container justifyContent='center' spacing={1}>
                            {
                              map(generatedData, (monthlyData) => {
                                return (
                                  <Grid key={`client-list-${monthlyData.date}`} item xs={12}>
                                    <Typography align='center' className={classes.graphLabel}>{ format(parseISO(monthlyData.date), 'LLL yyyy') }</Typography>
                                    <Divider />
                                    <ListTable
                                      rows={monthlyData?.fixed_assets}
                                      readOnly={true}
                                      labelHeader='Asset'
                                      valueLabel='Cost'
                                      labelKey='asset_name'
                                      valueKey='cost'
                                    />
                                  </Grid>
                                );
                              })
                            }
                          </Grid>
                        )}
                      </ReportDataBox>
                    </Paper>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Container>
        </MaterialThemeProvider>
      </Box>
    </Fragment>
  );
};

export default Dashboard;