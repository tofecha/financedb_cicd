import React, { useEffect } from 'react';
import { navigate } from '@reach/router';
import Loading from '../components/Loading';
import { useAuthStore } from '../stores/authStore';

const Auth = () => {
  const [ authStore ] = useAuthStore();

  useEffect(() => {
    if (authStore.user) {
      if (authStore.user.access === 'allowed') {
        navigate('/my-workspace');
      } else {
        navigate('/unauthorized', { replace: true });
      }
    }
  }, [authStore.user]);

  return (
    <Loading />
  );
};

export default Auth;