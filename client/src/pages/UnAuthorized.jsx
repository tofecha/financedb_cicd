import { Dialog, DialogContent, DialogTitle, Typography } from '@material-ui/core';
import { navigate } from '@reach/router';
import React, { Fragment, useEffect } from 'react';
import { useAuthStore } from '../stores/authStore';

const UnAuthorized = () => {
  const [authStore] = useAuthStore();

  // check if really unauthorized
  useEffect(() => {
    if (authStore.user) {
      if (authStore.user.access === 'allowed') {
        navigate('/my-workspace');
      } else {
        navigate('/unauthorized', { replace: true });
      }
    }
  }, [authStore.user]);

  return (
    <Fragment>
      <Dialog open={true}>
        <DialogTitle>Unauthorized access</DialogTitle>
        <DialogContent>
          <Typography variant='body1'>
            It seems like your access to this platform has changed. If you think this is a mistake, try refreshing the page or contact the platform management.  
          </Typography>
        </DialogContent>
      </Dialog>
    </Fragment>
  );
};

export default UnAuthorized;
