import React, { lazy, Suspense } from 'react';
import { Box, CircularProgress, Grid, Paper, Toolbar, Typography, useMediaQuery } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { ErrorBoundary } from 'react-error-boundary';
import ErrorFallback from '../components/ErrorFallback';
import { retry } from '../helper/retryHelper';

const AsyncForexItem = lazy(() => retry(() => import('../components/ForexItemComponent/ForexItem'), 5, 5000));

const defaultForexItems = [{
  base: 'USD',
  symbol: 'PHP'
}, {
  base: 'AUD',
  symbol: 'USD'
}, {
  base: 'AUD',
  symbol: 'PHP'
}];

const Forex = () => {
  const theme = useTheme();
  const isWidthLgAndUp = useMediaQuery(theme.breakpoints.up('lg'));

  return (
    <Box p={isWidthLgAndUp ? 3 : 0}>
      <Paper variant='outlined'>
        <Box mt={2} ml={2} mr={2} mb={0}>
          <Toolbar disableGutters variant='dense'>
            <Typography variant='h5' component='div'>Foreign Exchange</Typography>
          </Toolbar>
        </Box>
        <Box mt={0} ml={2} mr={2} mb={2}>
          <Grid container spacing={2}>
            {
              defaultForexItems && defaultForexItems.map((item, index) => (
                <Grid item xl={4} lg={4} md={6} sm={6} xs={12} key={index}>
                  <Paper variant='outlined'>
                    <ErrorBoundary
                      FallbackComponent={ErrorFallback}
                      onError={(props) => {
                        console.error('ErrorBoundary', props);
                      }}
                    >
                      <Suspense fallback={(
                        <Box height={420} display='flex' flexDirection='row' justifyContent='center' alignItems='center'>
                          <CircularProgress />
                        </Box>)}
                      >
                        <AsyncForexItem
                          defaultBase={item.base || 'USD'}
                          defaultSymbol={item.symbol || 'AUD'}
                        />
                      </Suspense>
                    </ErrorBoundary>
                  </Paper>
                </Grid>
              ))
            }
          </Grid>
        </Box>
      </Paper>
    </Box>
  );
};

export default Forex;