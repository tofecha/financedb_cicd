import { Box, Button, Divider, Grid, IconButton, InputAdornment, Paper, TextField, Toolbar, Tooltip, Typography } from '@material-ui/core';
import React, { Fragment, useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { DatePicker } from '@material-ui/pickers';
import ListBuilder from '../components/DataEditor/ListBuilder';
import { Field, FieldArray, Formik } from 'formik';
import { USDFormatter } from '../config/usdFormatter';
import NumberFormatCustom from '../components/DataEditor/NumberFormatCustom';
import { isEmpty, map } from 'lodash-es';
import { addMonths, subMonths } from 'date-fns';
import bspApi from '../config/bspApi';
import { useAppStore } from '../stores/appStore';
import { useSnackbar } from 'notistack';
import BreadcrumbsRouter from '../components/BreadcrumbsRouter';
import { ArrowBack, ArrowForward, ChevronLeft } from '@material-ui/icons';
import { useNavigate } from '@reach/router';

const REPORT_TYPE = {
  QUARTERLY : 'quarterly',
  MONTHLY : 'monthly'
};
Object.freeze(REPORT_TYPE);

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    overflow: 'hidden'
  },
  toolbarSpacer: {
    '& > *' : {
      marginRight: theme.spacing(1)
    }
  },
  graphLabel: {
    marginLeft: theme.spacing(2)
  },
  headerLabel: {
    marginBottom: theme.spacing(1)
  },
  paperPadding: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
  },
  textFieldMarginBottom: {
    '& > *' : {
      marginBottom: theme.spacing(2)
    }
  },
  listHeader: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    '& > *' : {
      marginRight: theme.spacing(1)
    },
    paddingBottom: theme.spacing(1)
  },
  dialogContent: {
    '& > *' : {
      marginBottom: theme.spacing(2)
    }
  },
  breakdownTooltip: {
    width: 350,
  }
}));

const defaultFormData = {
  date: new Date(),
  sales_collection: 0,
  target_revenue: 0,
  actual_revenue: 0,
  clients: [],
  operating_costs: [],
  fixed_assets: []
};

const DashboardEditor = () => {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const navigate = useNavigate();

  const formikRef = useRef(undefined);

  const [ monthlyData, setMonthlyData ] = useState(defaultFormData);
  const [ selectedDate, setSelectedDate ] = useState(new Date());
  const [ dateToCopy, setDateToCopy ] = useState(subMonths(new Date(), 1));
  const [ isFetching, setFetching ] = useState(false);
  const [ store, actions] = useAppStore();
  const minDate = new Date('2021-01-01');
  const maxDate = store.date;

  const isEditing = monthlyData;
  // const hasChanges = !isEqual(formikRef.current?.values, monthlyData);

  const calculateActualRevenue = (values) => {
    return values.actual_revenue;
    // if (
    //   values.regular ||
    //   values.adhoc ||
    //   values.pending_launch
    // ) {
    //   return parseFloat(values?.regular) + parseFloat(values?.adhoc) + parseFloat(values?.pending_launch);
    // } else if (values.actual_revenue) {
    //   return values.actual_revenue;
    // }
    // return 0;
  };

  const earningsBeforeTax = (values) => {
    const actualRevenue = calculateActualRevenue(values);
    const operationCosts = values?.operating_costs;

    if (actualRevenue && !isEmpty(operationCosts)) {
      const totalOperationCosts = values?.operating_costs?.reduce((acc, cur) => (
        acc + cur.cost
      ), 0);
      return actualRevenue - totalOperationCosts;
    }
    return 0;
  };

  const fetchData = async (date) => {
    setFetching(true);
    const result = await bspApi.get('/report/get', {
      date
    });
    if (result.ok) {
      const data = {
        ...defaultFormData,
        ...result.data
      } || {
        ...defaultFormData,
        date: date
      };
      setMonthlyData(data);
    }
    setFetching(false);
  };

  const fetchDataToCopy = async (date) => {
    setFetching(true);
    const result = await bspApi.get('/report/get', {
      date
    });
    if (result.ok) {
      setMonthlyData(result.data ? {
        ...monthlyData,
        clients: map(result.data.clients, ({ client_name, payment }) => ({
          client_name: client_name,
          payment: payment
        })),
        operating_costs: map(result.data.operating_costs, ({ operation_name, cost }) => ({
          operation_name: operation_name,
          cost: cost
        })),
        fixed_assets: map(result.data.fixed_assets, ({ asset_name, cost }) => ({
          asset_name: asset_name,
          cost: cost
        })),
        date: selectedDate
      } : {
        ...defaultFormData,
        date: selectedDate
      });
    }
    setFetching(false);
  };

  const saveToDatabase = async (data) => {
    console.log('data to save', data);
    actions.showPageStateLoading();
    setFetching(true);
    const result = await bspApi.post('/report/save', {
      ...data,
      actual_revenue: calculateActualRevenue(data)
    });
    if (result.ok) {
      console.log('save result', result.data);
    } else {
      console.error('error', result);
    }
    setFetching(false);
    actions.hidePageStateLoading();
  };

  const onDateArrowClick = (e, action) => {
    e.stopPropagation();
    let newDate;
    if (action === 'next') {
      newDate = addMonths(selectedDate, 1);
      newDate.setDate(1);
      if (newDate > maxDate) {
        setSelectedDate(maxDate);
      } else {
        setSelectedDate(newDate);
      }
    } else {
      newDate = subMonths(selectedDate, 1);
      setSelectedDate(newDate);
    }
    fetchData(newDate);
  };

  const handleOnOpenClick = (date) => {
    fetchData(date);
  };

  const handleOnCopyFromClick = (date) => {
    fetchDataToCopy(date);
  };

  const handleOnSaveClick = () => {
    if (formikRef) {
      saveToDatabase(formikRef.current.values);
    }
  };

  const handleOnCancelClick = () => {
    setMonthlyData(undefined);
  };

  const handleRedirectToReports = () => {
    navigate('/dashboard');
  };

  const isMaxDate = selectedDate.getFullYear() === maxDate.getFullYear() && selectedDate.getMonth() === maxDate.getMonth();
  const isMinDate = selectedDate.getFullYear() === minDate.getFullYear() && selectedDate.getMonth() === minDate.getMonth();

  const shouldDisableActualRevenueField = (values) => (
    Boolean(values?.pending_launch != 0 || values?.adhoc != 0 || values?.regular != 0)
  );

  return (
    <Fragment>
      <Toolbar>
        <BreadcrumbsRouter />
      </Toolbar>
      <Divider />
      <Box m={3} mt={1}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Paper variant='outlined' className={classes.toolbarPaper}>
              <Toolbar variant='dense' className={classes.toolbarSpacer}>
                <Button size='small' startIcon={<ChevronLeft />} variant='contained' onClick={handleRedirectToReports}>
                  Back
                </Button>
                <Typography variant='body1'>Select Year and month : </Typography>
                <DatePicker
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position='start'>
                        <IconButton edge='start' disabled={isMinDate} onClick={(e) => onDateArrowClick(e, 'prev')}>
                          <ArrowBack />
                        </IconButton>
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <InputAdornment position='end'>
                        <IconButton edge='end' disabled={isMaxDate} onClick={(e) => onDateArrowClick(e, 'next')}>
                          <ArrowForward />
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                  disableToolbar
                  disableFuture
                  size='small'
                  margin='dense'
                  variant='inline'
                  views={['month', 'year']}
                  minDate={minDate}
                  value={selectedDate}
                  onChange={(date) => (
                    setSelectedDate(date || null)
                  )}
                  onYearChange={(date) => {
                    setSelectedDate(date || null);
                    fetchData(date);
                  }}
                  autoOk
                  animateYearScrolling
                  style={{
                    width: 220
                  }}
                  disabled={isFetching}
                />

                {
                  isEditing ? (
                    <Fragment>
                      <Button variant='contained' size='small' onClick={handleOnCancelClick}>
                        Cancel
                      </Button>
                      <Button variant='contained' size='small' onClick={handleOnSaveClick}>
                        Save
                      </Button>
                    </Fragment>
                  ) : (
                    <Button disabled={isFetching || !selectedDate} variant='contained' size='small' onClick={() => (
                      handleOnOpenClick(selectedDate)
                    )}>
                      Open
                    </Button>
                  )
                }
                <Divider orientation='vertical' flexItem />
                <Box flexGrow={1} />
                {
                  isEditing && (
                    <Fragment>
                      <Divider orientation='vertical' flexItem />
                      <Button disabled={isFetching} variant='contained' size='small' onClick={() => (
                        handleOnCopyFromClick(dateToCopy)
                      )}>
                        Copy from
                      </Button>
                      <DatePicker
                        disableToolbar
                        disableFuture
                        minDate={minDate}
                        margin='dense'
                        variant='inline'
                        views={['year', 'month']}
                        value={dateToCopy}
                        onChange={(date) => (
                          setDateToCopy(date || null)
                        )}
                        autoOk
                        animateYearScrolling
                        style={{
                          width: 160
                        }}
                      />
                    </Fragment>
                  )
                }
              </Toolbar>
            </Paper>
          </Grid>

          <Formik
            initialValues={monthlyData || defaultFormData}
            innerRef={formikRef}
            enableReinitialize
          >
            {({ values }) => (
              <Grid item xs={12}>
                {
                  isEditing ? (
                    <Paper variant='outlined' className={classes.paperPadding}>
                      <Grid container spacing={2}>
                        <Grid item xs={2} className={classes.textFieldMarginBottom}>
                          <Typography variant='h6'>Sales report</Typography>
                          <Field
                            as={TextField}
                            label='Sales collection'
                            name='sales_collection'
                            fullWidth
                            InputLabelProps={{
                              shrink: true
                            }}
                            InputProps={{
                              inputComponent: NumberFormatCustom,
                            }}
                          />
                          <Field
                            as={TextField}
                            label='Target revenue'
                            name='target_revenue'
                            fullWidth
                            InputLabelProps={{
                              shrink: true
                            }}
                            InputProps={{
                              inputComponent: NumberFormatCustom,
                            }}
                          />
                          <Field
                            as={TextField}
                            label='Actual revenue'
                            name='actual_revenue'
                            fullWidth
                            InputLabelProps={{
                              shrink: true
                            }}
                            InputProps={{
                              inputComponent: NumberFormatCustom,
                            }}
                          />
                          <Paper variant='outlined'>
                            <Box display='flex' flexDirection='column' p={1}>
                              <Typography variant='subtitle1'>Earnings before income tax</Typography>
                              <Divider />
                              <Tooltip
                                interactive
                                placement='right'
                                classes={{ tooltip: classes.breakdownTooltip }}
                                title={
                                  <Box p={1} display='flex' flexDirection='column'>
                                    <Typography variant='subtitle1'>Sum of operation costs: </Typography>
                                    <Typography variant='body1'>
                                      {
                                        USDFormatter.format(values?.operating_costs?.reduce((acc, cur) => (
                                          acc + cur.cost
                                        ), 0) || 0)
                                      }
                                    </Typography>
                                    <Typography variant='subtitle1'>Actual revenue: </Typography>
                                    <Typography variant='body1'>{USDFormatter.format(calculateActualRevenue(values) || 0)}</Typography>
                                    <Typography variant='subtitle1'>Calculation: </Typography>
                                    <Typography variant='body1'>
                                      {
                                        `${USDFormatter.format(calculateActualRevenue(values) || 0)} - ${USDFormatter.format(values?.operating_costs?.reduce((acc, cur) => (
                                          acc + cur.cost
                                        ), 0) || 0)
                                        } = ${USDFormatter.format(earningsBeforeTax(values))}`
                                      }
                                    </Typography>
                                    <Typography variant='body1'>
                                      {
                                        `(${USDFormatter.format(earningsBeforeTax(values))} / ${USDFormatter.format(calculateActualRevenue(values))}) * 100 = ${(((earningsBeforeTax(values) / calculateActualRevenue(values)) * 100) || 0).toPrecision(4)} %`
                                      }
                                    </Typography>
                                    <Typography variant='h6'>Result: </Typography>
                                    <Typography variant='body1'>
                                      {
                                        `${USDFormatter.format(earningsBeforeTax(values))}`
                                      }
                                    </Typography>
                                    <Typography variant='body1'>
                                      {
                                        `(${(((earningsBeforeTax(values) / calculateActualRevenue(values)) * 100) || 0).toPrecision(4)} %)`
                                      }
                                    </Typography>
                                  </Box>
                                }
                              >
                                <Box>
                                  <Typography
                                    align='center'
                                    noWrap
                                    variant='h6'>
                                    {
                                      `${USDFormatter.format(earningsBeforeTax(values))}`
                                    }
                                  </Typography>
                                  <Typography align='center' variant='h6'>
                                    {
                                      `(${(((earningsBeforeTax(values) / calculateActualRevenue(values)) * 100) || 0).toPrecision(4)} %)`
                                    }
                                  </Typography>
                                </Box>
                              </Tooltip>
                              <Typography align='center' variant='caption'>Hover on the value for the breakdown</Typography>
                            </Box>
                          </Paper>
                        </Grid>
                        <Grid item xs={10} container spacing={2}>
                          <Grid item xs={4}>
                            <Typography variant='h6'>Top clients</Typography>
                            <Box height={'60vh'}>
                              <FieldArray
                                name='clients'
                                render={(arrayHelpers) => (
                                  <ListBuilder
                                    rows={values?.clients}
                                    onAddRow={(row) => (
                                      arrayHelpers.push({
                                        client_name: row.client_name,
                                        payment: row.payment
                                      })
                                    )}
                                    onRemoveRow={(row) => (
                                      row?.id !== -1 && arrayHelpers.remove(row?.id)
                                    )}
                                    onEditRow={(row) => (
                                      arrayHelpers.replace(row?.id, row)
                                    )}
                                    labelHeader='Client'
                                    // valueLabel='Payment'
                                    valueLabel='Invoiced'
                                    labelKey='client_name'
                                    valueKey='payment'
                                  />
                                )}
                              />
                            </Box>
                          </Grid>
                          <Grid item xs={4}>
                            <Typography variant='h6'>Operating costs</Typography>
                            <Box height={'60vh'}>
                              <FieldArray
                                name='operating_costs'
                                render={(arrayHelpers) => (
                                  <ListBuilder
                                    rows={values?.operating_costs}
                                    onAddRow={(row) => (
                                      arrayHelpers.push({
                                        operation_name: row.operation_name,
                                        cost: row.cost
                                      })
                                    )}
                                    onRemoveRow={(row) => (
                                      arrayHelpers.remove(row?.id)
                                    )}
                                    onEditRow={(row) => (
                                      arrayHelpers.replace(row?.id, row)
                                    )}
                                    labelHeader='Operation'
                                    valueLabel='Cost'
                                    labelKey='operation_name'
                                    valueKey='cost'
                                  />
                                )}
                              />
                            </Box>
                          </Grid>
                          <Grid item xs={4}>
                            <Typography variant='h6'>Fixed assets</Typography>
                            <Box height={'60vh'}>
                              <FieldArray
                                name='fixed_assets'
                                render={(arrayHelpers) => (
                                  <ListBuilder
                                    rows={values?.fixed_assets}
                                    onAddRow={(row) => (
                                      arrayHelpers.push({
                                        asset_name: row.asset_name,
                                        cost: row.cost
                                      })
                                    )}
                                    onRemoveRow={(row) => (
                                      row?.id !== -1 && arrayHelpers.remove(row?.id)
                                    )}
                                    onEditRow={(row) => (
                                      arrayHelpers.replace(row?.id, row)
                                    )}
                                    labelHeader='Assets'
                                    valueLabel='Cost'
                                    labelKey='asset_name'
                                    valueKey='cost'
                                  />
                                )}
                              />
                            </Box>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Paper>
                  ) : (
                    <Box height={'60vh'} display='flex' alignItems='center' justifyContent='center'>
                      <Typography variant='h6'>
                        Please select a date above and click &quot;Open&quot;
                      </Typography>
                    </Box>
                  )
                }
              </Grid>
            )}
          </Formik>
        </Grid>
      </Box>
    </Fragment>
  );
};

export default DashboardEditor;