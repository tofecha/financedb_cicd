import { Breadcrumbs, Button, ButtonGroup, Grid, Paper, Link, Box, Typography, Toolbar, InputAdornment, IconButton, Divider, TextField, Tooltip, Dialog } from '@material-ui/core';
import { Add as AddIcon, ArrowBack as ArrowBackIcon, ArrowForward as ArrowForwardIcon, Check as CheckIcon, Refresh as RefreshIcon, Remove as RemoveIcon, RotateLeft as RotateLeftIcon } from '@material-ui/icons';
import { DataGrid } from '@material-ui/data-grid';
import { DatePicker } from '@material-ui/pickers';
import { addDays, format, isSameDay, subDays } from 'date-fns';
import React, { useEffect, useMemo, useState } from 'react';
// import { Link as RouterLink } from 'react-router-dom';
import bspApi, { BSPoldestDate } from '../config/bspApi';
import { makeStyles } from '@material-ui/core/styles';
import { cloneDeep, find, findIndex, isEqual, map, pick, keys } from 'lodash-es';
import { useAppStore } from '../stores/appStore';
import { Fragment } from 'react';

const useStyles = makeStyles((theme) => ({
  spacer: {
    flexGrow: 1,
  },
  scrapeSelectHeaderText: {
    margin: theme.spacing(0, 1, 1, 0)
  },
  resetIcon: {
    fontSize: 20
  }
}));

const scales = [0.5, 0.75, 1, 1.5];
const scalesMaxIndex = (scales.length - 1);

const MonetaryCell = (props) => {
  const classes = useStyles();
  const { field, value, rowData, originalData, onChange } = props;
  const [isEditing, setEditing] = useState(false);
  const [currentValue, setCurrentValue] = useState(0);

  let hasChanges = false;
  try {
    hasChanges = !isEqual(rowData[field], originalData[field]);
  } catch (err) {
    hasChanges = true;
  }

  useEffect(() => {
    setCurrentValue(value);
  }, [value]);

  const applyChange = () => {
    onChange(rowData.symbol, field, parseFloat(currentValue));
    setEditing(false);
  };

  const resetChange = () => {
    onChange(rowData.symbol, field, originalData[field]);
    setEditing(false);
  };

  return (
    <Box width='100%' display='flex' flexDirection='row' alignItems='center' onDoubleClick={() => setEditing(true)}>
      {
        isEditing ? (
          <Fragment>
            <TextField value={currentValue} type='number' inputProps={{ min: 0.0 }} onChange={(e) => setCurrentValue(parseFloat(e.target.value))}/>
            <IconButton size='small' onClick={applyChange}>
              <CheckIcon color='primary' className={classes.resetIcon}/>
            </IconButton>
          </Fragment>
        ) : (
          hasChanges ? (
            <Box width='100%' display='flex' flexDirection='row' alignItems='center' >
              <Tooltip title={`Previous value : ${(originalData && originalData[field]) ? originalData[field] : 'n/a'}`}>
                <Typography variant='body1'><i>{`* ${currentValue}`}</i></Typography>
              </Tooltip>
              <div className={classes.spacer} />
              <IconButton size='small' onClick={resetChange}>
                <RotateLeftIcon className={classes.resetIcon}/>
              </IconButton>
            </Box>
          ) : (
            <Typography variant='body1'> { currentValue } </Typography>
          )
        )
      }
    </Box>
  );
};

const BSPEditor = () => {
  const [ store ] = useAppStore();
  const classes = useStyles();
  const maxDate = store.date;
  const [date, setDate] = useState(new Date());
  const [scale, setScale] = useState(2);
  const [pageNumber] = useState(1);
  const [bspData, setBSPData] = useState({});
  const [editableData, setEditableData] = useState({});
  const [isProcessing, setProcessing] = useState(false);
  const [pdfExists, setPDFExists] = useState(false);
  // const [pdfFile, setPdfFile] = useState(undefined);

  const handleDateChange = (date) => setDate(date);
  const incScale = () => setScale((scale - 1) < 0 ? 0 : scale - 1);
  const decScale = () => setScale((scale + 1) > scalesMaxIndex ? scalesMaxIndex : scale + 1);

  // `${bspApi.getBaseURL()}/pdf/date=${format(date, 'yyyy-MM-dd')}`;
  const getData = async (date) => {
    setProcessing(true);

    setBSPData({});
    setEditableData({});

    const formattedDate = format(date, 'yyyy-MM-dd');

    const bspData = await bspApi.get(`/forex/${formattedDate}`, { full: 'true' });
    if (bspData.ok && bspData.data) {
      const sanitizedData = map(bspData.data.data, (data) => ({
        ...data,
        id: data.symbol
      }));

      setBSPData({
        data: sanitizedData,
        date: bspData.data.date
      });
      setEditableData({
        data: sanitizedData,
        date: bspData.data.date
      });
    }
    // const pdfFile = await bspApi.get(`/forex/pdf?date=${formattedDate}`, {}, { responseType: 'blob' });
    // if (pdfFile.ok && pdfFile.data) {
    //   const url = window.URL.createObjectURL(new Blob([pdfFile.data]));
    //   setPdfFile(url);
    // } else {
    //   setPdfFile(undefined);
    // }
    setProcessing(false);
  };

  const saveData = async () => {
    const newData = {
      ...editableData,
      data: map(editableData.data, (data) => pick(data, keys({ symbol: null, EUR: null, USD: null, PHP: null})))
    };
    const req = await bspApi.post('/forex/save', newData);
    if (req.ok) {
      const sanitizedData = map(newData.data, (data) => ({
        ...data,
        id: data.symbol
      }));
  
      setBSPData({
        data: sanitizedData,
        date: newData.date
      });
      setEditableData({
        data: sanitizedData,
        date: newData.date
      });
    }
  };

  const rescrape = async (date) => {
    setProcessing(true);
    setEditableData({});

    const bspData = await bspApi.get('/forex/pdf/scrape', { date: format(date, 'yyyy-MM-dd'), full: 'true' });
    if (bspData.ok && bspData.data) {
      const sanitizedData = map(bspData.data.data, (data) => ({
        ...data,
        id: data.symbol
      }));

      setEditableData({
        data: sanitizedData,
        date: bspData.data.date
      });
    }

    setProcessing(false);
  };

  const onDateArrowClick = (e, action) => {
    e.stopPropagation();
    if (action === 'next') {
      const newDate = addDays(date, 1);
      if (newDate > maxDate) {
        setDate(maxDate);
      } else {
        setDate(newDate);
      }
    } else {
      const newDate = subDays(date, 1);
      if (newDate < BSPoldestDate) {
        setDate(BSPoldestDate);
      } else {
        setDate(newDate);
      }
    }

  };

  const onRefreshClick = (e) => {
    e.stopPropagation();
    getData(date);
  };

  const hasChanges = useMemo(() => !isEqual(bspData, editableData), [bspData, editableData]);

  const CustomFooter = () => {
    return (
      <Box>
        <Divider/>
        {
          bspData ? (
            <Box display='flex' alignItems='center' flexDirection='row' ml={2} mr={2} mt={1} mb={1}>
              {
                isProcessing && (
                  <Typography variant='body2'><b>Data is Processing. This may take a while.</b></Typography>
                )
              }
              <div className={classes.spacer} />
              <Button disabled={!hasChanges} color='primary' onClick={saveData}>Save Changes</Button>
            </Box>
          ) : undefined
        }
      </Box>
    );
  };

  const onChangeCellData = (symbol, field, newValue) => {
    const newDataSet = cloneDeep(editableData);
    const currentDataIndex = findIndex(newDataSet.data, ['symbol', symbol]);
    newDataSet.data[currentDataIndex][field] = newValue;
    setEditableData(newDataSet);
  };
  
  const CustomCell = (params) => (
    <MonetaryCell
      field={params.field}
      value={params.value}
      rowData={params.row}
      originalData={find(bspData.data, ['symbol', params.row.symbol])}
      onChange={onChangeCellData}
    />
  );

  useEffect(() => {
    getData(date);
  }, [date]);

  return (
    <Grid container spacing={2} justifyContent='space-between'>
      <Grid item xs={12}>
        <Breadcrumbs aria-label='breadcrumb'>
          {/* <Link component={RouterLink} color='inherit' to='/'>
            Dashboard
          </Link>
          <Link component={RouterLink} color='textPrimary' to='/bspeditor'>
            BSP Data Editor
          </Link> */}
        </Breadcrumbs>
      </Grid>

      <Grid item md={6} xs={12}>
        <Toolbar disableGutters variant='dense'>
          <DatePicker
            disabled={isProcessing}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <IconButton disabled={isProcessing || isSameDay(date, BSPoldestDate)} onClick={(e) => onDateArrowClick(e, 'prev')}>
                    <ArrowBackIcon/>
                  </IconButton>
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position='end'>
                  <IconButton disabled={isProcessing} onClick={(e) => onRefreshClick(e)}>
                    <RefreshIcon/>
                  </IconButton>

                  <IconButton disabled={isProcessing || isSameDay(date, maxDate)} onClick={(e) => onDateArrowClick(e, 'next')}>
                    <ArrowForwardIcon/>
                  </IconButton>
                </InputAdornment>
              )
            }}
            variant='inline'
            disableFuture
            inputVariant='outlined'
            size='small'
            autoOk
            value={date}
            minDate={BSPoldestDate}
            maxDate={maxDate}
            onChange={handleDateChange} format='LLL dd yyyy'
          />
          <div className={classes.spacer} />
          <Typography variant='h6' className={classes.scrapeSelectHeaderText}>Scrape data using :</Typography>
          <ButtonGroup size='medium'>
            <Button disabled={isProcessing || !pdfExists} size='large' variant='outlined' onClick={() => rescrape(date)}><Typography variant='body1'>Scrape data</Typography></Button>
          </ButtonGroup>
        </Toolbar>
      </Grid>
      
      {/* <Grid item xs={6}>
        <Toolbar disableGutters variant='dense'>
          <div className={classes.spacer} />
          <ButtonGroup size='medium'>
            <Button disabled={scale === 0} onClick={incScale}>
              <RemoveIcon />
            </Button>
            <Button disabled={scale === scalesMaxIndex} onClick={decScale}>
              <AddIcon />
            </Button>
          </ButtonGroup>
        </Toolbar>
      </Grid> */}

      <Grid item md={6} xs={12}>
        <Paper style={{ height: 600}}>
          <DataGrid
            loading={isProcessing}
            disableColumnMenu
            disableColumnReorder
            disableColumnFilter
            disableColumnSelector
            disableColumnResize
            disableSelectionOnClick
            sortingOrder={null}
            rows={editableData?.data || []}
            columns={[
              { width: 100, field: 'symbol', numeric: false, headerName: 'CURRENCY' },
              { width: 200, field: 'EUR', numeric: true, headerName: 'EURO Equivalent', renderCell: CustomCell },
              { width: 200, field: 'USD', numeric: true, headerName: 'USD Equivalent', renderCell: CustomCell },
              { width: 200, field: 'PHP', numeric: true, headerName: 'PHP Equivalent', renderCell: CustomCell },
            ]}
            components={{
              Footer: CustomFooter
            }}
          />
          {/* <Dialog open={open} onClose={handleClose} >

          </Dialog> */}
        </Paper>
      </Grid>

      {/* <Grid item xs={6}>
        <Paper variant='outlined' style={{ height: 600, overflow:'auto' }}>
          <Box pt={4} pb={4} display='flex'>
            <Box m='auto' >
              <Document
                file={pdfFile}
                onLoadSuccess={() => setPDFExists(true)}
                onLoadError={() => setPDFExists(false)}
                error={(err) => (
                  <Box display='flex' flexDirection='column' justifyItems='center' height={400}>
                    <Typography variant='subtitle1'>
                      The File seems to be not existing
                    </Typography>
                    <Typography variant='body1'>
                      { err }
                    </Typography>
                  </Box>
                )}
              >
                <Page
                  width={612}
                  renderInteractiveForms={false}
                  renderAnnotationLayer={false}
                  renderTextLayer={true}
                  pageNumber={pageNumber}
                  scale={scales[scale]}
                  onLoadError={(err) => console.error('onLoadError', err)}
                  onRenderError={(err) => console.error('onRenderError', err)}
                  onGetAnnotationsError={(err) => console.error('onGetAnnotationsError', err)}
                />
              </Document>
            </Box>
          </Box>
        </Paper>
      </Grid> */}
    </Grid>
  );
};

export default BSPEditor;