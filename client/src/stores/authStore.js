import { createHook, createStore, createSubscriber } from 'react-sweet-state';
import bspApi from '../config/bspApi';
import { v1 as uuidv1 } from 'uuid';

const fetchUser = () => async ({ setState }) => {
  const result = await bspApi.get('/user');
  if (result.ok && result.status === 200 && result.data) {
    // console.log('user data', result.data);
    setState({
      user: result.data
    });
  } else if (result.status === 401) {
    setState({
      user: null,
    });
  }
};

const AuthStore = createStore({
  initialState: {
    user: null,
    hasToken: false,
  },
  actions: {
    getFullname: () => ({ getState }) => {
      return getState().user ? `${getState().user.first_name} ${getState().user.last_name}` : '' ;
    },
    setTokenExistence: (hasToken) => ({ setState }) => {
      setState({
        hasToken: hasToken
      });
    },
    fetchUser: () => async({ dispatch }) => {
      await dispatch(fetchUser());
    },
    isAllowedAccessToPlatform: () => async ({ getState, dispatch }) => {
      if (!getState().user) {
        await dispatch(fetchUser());
      }
      return Boolean(getState().user && getState().user.access === 'allowed');
    },
  }
});

const getUserPlatformAccount = (state) => ({
  platform_account: state.user?.platform_account || null
});

export const useUserPlatformAccount = createHook(AuthStore, {
  selector: getUserPlatformAccount,
});

const AuthStoreSubscriber = createSubscriber(AuthStore);
const useAuthStore = createHook(AuthStore);

export {
  AuthStoreSubscriber,
  useAuthStore
};