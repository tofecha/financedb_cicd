import { createHook, createStore, createSubscriber } from 'react-sweet-state';

const AppStore = createStore({
  initialState: {
    date: new Date(),
    theme: 'light',
    pageStateLoading: false,
  },
  actions: {
    showPageStateLoading: () => ({ setState }) => ( setState({ pageStateLoading: true }) ),
    hidePageStateLoading: () => ({ setState }) => ( setState({ pageStateLoading: false }) ),
    setDate: (date) => ({ setState }) => {
      setState({ date : date });
    },
    setTheme: (theme) => ({ setState }) => (
      setState({ theme: theme })
    ),
  },
  name: 'AppStore'
});

const AppStoreSubscriber = createSubscriber(AppStore);
const useAppStore = createHook(AppStore);

const getDate = (state) => ({
  date: state.date
});

export const useDate = createHook(AppStore, {
  selector: getDate,
});

const getTheme = (state) => ({
  theme: state.theme
});

export const useTheme = createHook(AppStore, {
  selector: getTheme
});

export {
  AppStoreSubscriber,
  useAppStore,
};