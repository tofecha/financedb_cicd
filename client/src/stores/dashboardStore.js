import { createHook, createStore, createSubscriber } from 'react-sweet-state';

const DashboardStore = createStore({
  initialState: {
    dateTime: new Date(),
  },
  actions: {
    setDateTime: (dateTime) => ({ setState }) => {
      setState({ dateTime : dateTime });
    },
  },
  name: 'DashboardStore'
});

const DashboardStoreSubscriber = createSubscriber(DashboardStore);
const useDashboardStore = createHook(DashboardStore);

export {
  DashboardStoreSubscriber,
  useDashboardStore,
};