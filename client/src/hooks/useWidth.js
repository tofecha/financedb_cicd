import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useWidth = () => {
  const theme = useTheme();
  const keys = [...theme.breakpoints.keys].reverse();
  return ( 'xs'
  // keys.reduce((output, key) => {
  //   const matches = useMediaQuery(theme.breakpoints.up(key));
  //   return !output && matches ? key : output;
  // }, null) || 'xs'
  );
};

export default useWidth;