import { AppBar, Backdrop, Box, Container, Divider, Fade, LinearProgress, Modal, Toolbar } from '@material-ui/core';
import React, { Fragment } from 'react';
import TopBar from '../components/TopBar';
import { makeStyles } from '@material-ui/core/styles';
import Sidebar from '../components/Sidebar';
import { useAppStore } from '../stores/appStore';
import NavBar from '../components/NavBar';

const useStyles = makeStyles((theme) => ({
  '@global': {
    '*::-webkit-scrollbar-track': {
      backgroundColor: theme.palette.background.paper,
    },
    '*::-webkit-scrollbar-thumb': {
      backgroundColor: theme.palette.type === 'dark' ? '#2F3D42' : theme.palette.primary.light,
    },
    '*::-webkit-scrollbar': {
      backgroundColor: theme.palette.background.paper,
      width: '6px'
    },
  },
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  content: {
    flexGrow: 1,
  },
  backdrop: {
    outline: 0,
  },
  appStateModal: {
    zIndex: 1900
  },
}));

const DefaultLayout = (props) => {
  const classes = useStyles();
  const { children } = props;
  const [ store ] = useAppStore();

  const pageLoad = store.pageStateLoading;

  const onModalClose = (e, reason) => {};

  return (
    <Box className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <TopBar />
      </AppBar>
      <Container component='main' maxWidth={false} disableGutters className={`${classes.content}`}>
        <Toolbar/>
        { children }
      </Container>
      <Modal
        BackdropComponent={Backdrop}
        BackdropProps={{ timeout: 200 }}
        disableAutoFocus
        disableEnforceFocus
        disableEscapeKeyDown
        open={pageLoad}
        onClose={onModalClose}
        className={`${classes.backdrop} ${classes.appStateModal}`}
      >
        <Fade in={pageLoad}>
          <LinearProgress />
        </Fade>
      </Modal>
    </Box>
  );
};

export default DefaultLayout;