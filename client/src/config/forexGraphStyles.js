import * as echarts from 'echarts/core';

const redLine = {
  lineStyle: {
    color: '#EB222E'
  },
  areaStyle: {
    opacity: 0.8,
    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      offset: 0,
      color: '#EB222E'
    }, {
      offset: 1,
      color: '#EB222E00'
    }])
  }
};

const greenLine = {
  lineStyle: {
    color: '#28b04d'
  },
  areaStyle: {
    opacity: 0.8,
    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      offset: 0,
      color: '#28b04d'
    }, {
      offset: 1,
      color: '#28b04d00'
    }])
  }
};

const greyLine = {
  lineStyle: {
    color: '#a7a7a7'
  },
  areaStyle: {
    opacity: 0.8,
    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      offset: 0,
      color: '#a7a7a7'
    }, {
      offset: 1,
      color: '#c7c7c700'
    }])
  }
};

export {
  redLine,
  greenLine,
  greyLine,
};