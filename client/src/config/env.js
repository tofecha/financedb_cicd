/* eslint-disable no-undef */
export default {
  ENVIRONMENT: process.env.NODE_ENV,
  PORT: import.meta.env.VITE_PORT,
  APP_URL: import.meta.env.VITE_URL,
  API_HOST: import.meta.env.VITE_API_HOST,
  AUTH_DISABLED: import.meta.env.AUTH_DISABLED,
  AUTH_URL: import.meta.env.VITE_AUTH_URL,
  CLIENT_ID: import.meta.env.VITE_CLIENT_ID,
  AUTH_ROUTE: import.meta.env.VITE_AUTH_ROUTE,
  TOKEN_ROUTE: import.meta.env.VITE_TOKEN_ROUTE,
  REDIRECT_URI: import.meta.env.VITE_REDIRECT_URI,
  AUTH: import.meta.env.VITE_AUTH === 'true'
};
