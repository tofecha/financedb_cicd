import { colors } from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles';

const typography = {
  fontFamily: [
    '"Noto Serif"',
    'serif',
    '"Roboto Condensed"',
    'sans-serif'
  ].join(','),
  fontSize: 12,
  h1: {
    fontFamily: 'Roboto Condensed',
    fontSize: 90,
    fontWeight: 300
  },
  h2: {
    fontFamily: 'Roboto Condensed',
    fontSize: 52,
    fontWeight: 300
  },
  h3: {
    fontFamily: 'Noto Serif',
    fontSize: 46,
    fontWeight: 700
  },
  h4: {
    fontFamily: 'Roboto Condensed',
    fontSize: 30,
    fontWeight: 400
  },
  h5: {
    fontFamily: 'Roboto Condensed',
    fontSize: 26,
    fontWeight: 400
  },
  h6: {
    fontFamily: 'Roboto Condensed',
    fontSize: 20,
    fontWeight: 700
  },
  subtitle1: {
    fontFamily: 'Roboto Condensed',
    fontSize: 14,
    fontWeight: 400
  },
  subtitle2: {
    fontFamily: 'Roboto Condensed',
    fontSize: 12,
    fontWeight: 700
  },
  body1: {
    fontFamily: 'Noto Serif',
    fontWeight: 700
  },
  body2: {
    fontFamily: 'Roboto Condensed',
    fontWeight: 300
  },
  button: {
    fontFamily: 'Roboto Condensed',
    fontSize: 12,
    fontWeight: 700,
    textTransform: 'uppercase'
  },
  caption: {
    fontFamily: 'Roboto Condensed',
    fontSize: 12,
    fontWeight: 400
  },
  overline: {
    fontFamily: 'Roboto Condensed',
    fontSize: 12,
    fontWeight: 400,
    textTransform: 'uppercase'
  }
};

// red - #EB222E
// blue - #366DC6 / #5780E9(not official, di namin talga alam yung blue ng nms, pero yan ginagamit namin)
// white - #ffffff

const lightMatTheme = createTheme({
  drawerWidth: 260,
  typography: typography,
  palette: {
    type: 'light',
    primary: {
      main: '#366DC6',
    },
    secondary: {
      main: '#5780E9'
    },
    error: {
      main: '#ff6859'
    },
    warning: {
      main: '#ffcf44'
    },
    info: {
      main: '#b15dff'
    },
    success: {
      main: '#72deff'
    },
    background: {
      default: '#fafafa',
      paper: '#fefefe'
    }
  },
  shape: {
    borderRadius: 10
  },
  mixins: {
    toolbar: {
      height: 48,
      minHeight: 48,
    }
  },
});

const darkMatTheme = createTheme({
  drawerWidth: 260,
  typography: typography,
  palette: {
    type: 'dark',
    primary: {
      main: '#366DC6',
    },
    secondary: {
      main: '#5780E9'
    },
    error: {
      main: '#ff6859'
    },
    warning: {
      main: '#ffcf44'
    },
    info: {
      main: '#b15dff'
    },
    success: {
      main: '#72deff'
    },
    background: {
      paper: '#1b2327',
      default: '#0d1118'
    },
  },
  shape: {
    borderRadius: 10
  },
  mixins: {
    toolbar: {
      height: 48,
      minHeight: 48,
    }
  },
});

export {
  lightMatTheme,
  darkMatTheme
};
