const gravatarLink = (md5, size = 256) => {
  if (md5) {
    return `https://www.gravatar.com/avatar/${md5}?&s=${size}&d=404`;
  } else {
    return undefined;
  }
};

export default gravatarLink;