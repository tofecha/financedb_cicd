const labelOnBarOptions = (labelOnBar) => labelOnBar ? {
  position: 'insideBottom',
  align: 'left',
  verticalAlign: 'middle',
  distance: 15,
  rotate: 90
} : {
  rotate: 0
};

export default labelOnBarOptions;