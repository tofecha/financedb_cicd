import { getTimeZones } from '@vvo/tzdb';
import { forEach, groupBy, join, map } from 'lodash-es';

const simplifiedTimeZones = map(groupBy(getTimeZones(), 'alternativeName'), (zone) => {
  const { alternativeName, currentTimeFormat, name } = zone[0];
  const cities = [];
  forEach(zone, ({ mainCities }) => {
    forEach(mainCities, (city) => {
      cities.push(city);
    });
  });
  const sample = join(cities, ', ');
  return {
    display: `${currentTimeFormat.substring(0, 6)} ${alternativeName} - ${sample}`,
    cities,
    name,
  };
});

export default simplifiedTimeZones;