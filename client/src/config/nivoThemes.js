const lightNivoTheme = {
  background: '#ffffff',
  textColor: '#333333',
  fontSize: 11,
  axis: {
    domain: {
      line: {
        stroke: '#777777',
        strokeWidth: 1
      }
    },
    ticks: {
      line: {
        stroke: '#777777',
        strokeWidth: 1
      }
    }
  },
  grid: {
    line: {
      stroke: '#dddddd',
      strokeWidth: 1
    }
  }
};

const darkNivoTheme = {
  background: '#1b2327',
  textColor: '#ffffff',
  fontSize: 11,
  axis: {
    domain: {
      line: {
        stroke: '#393a3b',
        strokeWidth: 1.5
      }
    },
    ticks: {
      line: {
        stroke: '#393a3b',
        strokeWidth: 1.5
      }
    }
  },
  grid: {
    line: {
      stroke: '#393a3b',
      strokeWidth: 1.5
    }
  },
  crosshair: {
    line: {
      stroke: '#FFF',
      strokeWidth: 1.5,
      strokeOpacity: 0.75,
    },
  }
};

export {
  lightNivoTheme,
  darkNivoTheme
};