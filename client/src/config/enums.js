const Mode = {
  'NEW': 1,
  'EDIT': 0,
  'FETCHING': 2,
  'VIEW': 3
};

const RPMode = {
  'DEFAULT': 0,
  'ROLE_TO_PERMISSIONS': 1,
  'PERMISSION_TO_ROLES': 2
};

const PERMISSIONS = {
  'CAN_UPDATE_ACCOUNT': 'can-update-account',
  'CAN_UPDATE_OTHER_ACCOUNT': 'can-update-other-account',
  'CAN_CREATE_ACCOUNT': 'can-create-account',
  'CAN_CHANGE_PLATFORM_OWNER': 'can-change-platform-owner'
};

const ROLES = {
  'SUPER_ADMIN': 'super-admin',
  'IT_ADMIN': 'it-admin',
  'IT_PLATFORM_MANAGER': 'it-platform-manager',
  'ACCOUNT_MANAGER': 'account-manager',
  'NORMAN': 'norman'
};

const RESOLVE = {
  HIDE: 'hidden',
  DISABLE: 'disabled',
};

Object.freeze(RESOLVE);
Object.freeze(RPMode);
Object.freeze(Mode);
Object.freeze(PERMISSIONS);
Object.freeze(ROLES);

export {
  RESOLVE,
  Mode,
  RPMode,
  PERMISSIONS,
  ROLES
};
