export const echartsDark = {
  'color': [
    '#5470c6',
    '#77b35b',
    '#ee6666',
    '#73c0de',
    '#3ba272',
    '#fc8452',
    '#9a60b4',
    '#ea7ccc'
  ],
  'backgroundColor': '#1b2327',
  'textStyle': {},
  'title': {
    'textStyle': {
      'color': '#ffffff'
    },
    'subtextStyle': {
      'color': '#fafafa'
    }
  },
  'line': {
    'itemStyle': {
      'borderWidth': 1
    },
    'lineStyle': {
      'width': 2
    },
    'symbolSize': 4,
    'symbol': 'emptyCircle',
    'smooth': false
  },
  'radar': {
    'itemStyle': {
      'borderWidth': 1
    },
    'lineStyle': {
      'width': 2
    },
    'symbolSize': 4,
    'symbol': 'emptyCircle',
    'smooth': false
  },
  'bar': {
    'itemStyle': {
      'barBorderWidth': '0',
      'barBorderColor': '#ffffff'
    }
  },
  'pie': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    }
  },
  'scatter': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    }
  },
  'boxplot': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    }
  },
  'parallel': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    }
  },
  'sankey': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    }
  },
  'funnel': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    }
  },
  'gauge': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    }
  },
  'candlestick': {
    'itemStyle': {
      'color': '#eb5454',
      'color0': '#47b262',
      'borderColor': '#eb5454',
      'borderColor0': '#47b262',
      'borderWidth': '1'
    }
  },
  'graph': {
    'itemStyle': {
      'borderWidth': '0',
      'borderColor': '#ffffff'
    },
    'lineStyle': {
      'width': 1,
      'color': '#aaa'
    },
    'symbolSize': 4,
    'symbol': 'emptyCircle',
    'smooth': false,
    'color': [
      '#5470c6',
      '#cc9c31',
      '#77b35b',
      '#ee6666',
      '#73c0de',
      '#3ba272',
      '#fc8452',
      '#9a60b4',
      '#ea7ccc'
    ],
    'label': {
      'color': '#f9f9f9'
    }
  },
  'map': {
    'itemStyle': {
      'areaColor': '#eee',
      'borderColor': '#444',
      'borderWidth': 0.5
    },
    'label': {
      'color': '#000'
    },
    'emphasis': {
      'itemStyle': {
        'areaColor': 'rgba(255,215,0,0.8)',
        'borderColor': '#444',
        'borderWidth': 1
      },
      'label': {
        'color': 'rgb(100,0,0)'
      }
    }
  },
  'geo': {
    'itemStyle': {
      'areaColor': '#eee',
      'borderColor': '#444',
      'borderWidth': 0.5
    },
    'label': {
      'color': '#000'
    },
    'emphasis': {
      'itemStyle': {
        'areaColor': 'rgba(255,215,0,0.8)',
        'borderColor': '#444',
        'borderWidth': 1
      },
      'label': {
        'color': 'rgb(100,0,0)'
      }
    }
  },
  'categoryAxis': {
    'axisLine': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisTick': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisLabel': {
      'show': true,
      'color': '#a0a3b0'
    },
    'splitLine': {
      'show': true,
      'lineStyle': {
        'color': [
          '#2f2f2f'
        ]
      }
    },
    'splitArea': {
      'show': false,
      'areaStyle': {
        'color': [
          'rgba(250,250,250,0.2)',
          'rgba(210,219,238,0.2)'
        ]
      }
    }
  },
  'valueAxis': {
    'axisLine': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisTick': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisLabel': {
      'show': true,
      'color': '#a0a3b0'
    },
    'splitLine': {
      'show': true,
      'lineStyle': {
        'color': [
          '#2f2f2f'
        ]
      }
    },
    'splitArea': {
      'show': false,
      'areaStyle': {
        'color': [
          'rgba(250,250,250,0.2)',
          'rgba(210,219,238,0.2)'
        ]
      }
    }
  },
  'logAxis': {
    'axisLine': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisTick': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisLabel': {
      'show': true,
      'color': '#a0a3b0'
    },
    'splitLine': {
      'show': true,
      'lineStyle': {
        'color': [
          '#2f2f2f'
        ]
      }
    },
    'splitArea': {
      'show': false,
      'areaStyle': {
        'color': [
          'rgba(250,250,250,0.2)',
          'rgba(210,219,238,0.2)'
        ]
      }
    }
  },
  'timeAxis': {
    'axisLine': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisTick': {
      'show': true,
      'lineStyle': {
        'color': '#2f2f2f'
      }
    },
    'axisLabel': {
      'show': true,
      'color': '#a0a3b0'
    },
    'splitLine': {
      'show': true,
      'lineStyle': {
        'color': [
          '#2f2f2f'
        ]
      }
    },
    'splitArea': {
      'show': false,
      'areaStyle': {
        'color': [
          'rgba(250,250,250,0.2)',
          'rgba(210,219,238,0.2)'
        ]
      }
    }
  },
  'toolbox': {
    'iconStyle': {
      'borderColor': '#999'
    },
    'emphasis': {
      'iconStyle': {
        'borderColor': '#666'
      }
    }
  },
  'legend': {
    'textStyle': {
      'color': '#e8e8e8'
    }
  },
  'tooltip': {
    'axisPointer': {
      'lineStyle': {
        'color': '#ccc',
        'width': 1
      },
      'crossStyle': {
        'color': '#ccc',
        'width': 1
      }
    }
  },
  'timeline': {
    'lineStyle': {
      'color': '#DAE1F5',
      'width': 2
    },
    'itemStyle': {
      'color': '#A4B1D7',
      'borderWidth': 1
    },
    'controlStyle': {
      'color': '#A4B1D7',
      'borderColor': '#A4B1D7',
      'borderWidth': 1
    },
    'checkpointStyle': {
      'color': '#316bf3',
      'borderColor': 'fff'
    },
    'label': {
      'color': '#A4B1D7'
    },
    'emphasis': {
      'itemStyle': {
        'color': '#FFF'
      },
      'controlStyle': {
        'color': '#A4B1D7',
        'borderColor': '#A4B1D7',
        'borderWidth': 1
      },
      'label': {
        'color': '#A4B1D7'
      }
    }
  },
  'visualMap': {
    'color': [
      '#bf444c',
      '#d88273',
      '#f6efa6'
    ]
  },
  'dataZoom': {
    'handleSize': 'undefined%',
    'textStyle': {}
  },
  'markPoint': {
    'label': {
      'color': '#f9f9f9'
    },
    'emphasis': {
      'label': {
        'color': '#f9f9f9'
      }
    }
  }
};
