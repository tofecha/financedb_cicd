import { create } from 'apisauce';
import env from './env';

const authApi = create({
  baseURL: `${env.AUTH_URL}/api/authorize/`
});

export default authApi;
