const breadcrumbNameMap = {
  '/forex': 'Forex',
  '/dashboard': 'Dashboard',
  '/dashboard/editor': 'Dashboard Editor',
  '/my-workspace': 'My Workspace',
  '/admin': 'Admin',
};

const topLevel = {
  '/forex': 'Forex',
  '/dashboard': 'Dashboard',
  '/my-workspace': 'My Workspace',
  '/admin': 'Admin',

};

export {
  topLevel
};
export default breadcrumbNameMap;