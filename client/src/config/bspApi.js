import { create } from 'apisauce';
import env from './env';

const bspApi = create({
  baseURL: env.API_HOST,
});

bspApi.addResponseTransform((response) => {
  if (response.status === 401) {
    console.log('bspApi | user is unauthorized');
  }
});

export const reportDateFormat = 'yyyy-MM-01';
export const forexDateFormat = 'yyyy-MM-dd';

export const setApiToken = (token) => (
  bspApi.setHeader('Authorization', `Bearer ${token}`)
);

export const BSPoldestDate = new Date('2018-01-01');

// report finance data
export const getReport = (limit = null, date) => (
  bspApi.get('/report/get', {
    limit,
    date
  }).then(({ data }) => data)
);
export const setReport = (data) => (
  bspApi.post('/report/save', data).then((result) => {
    return result.data;
  })
);
export const getMonthsWithData = () => (
  bspApi.get('/report/months').then(({ data }) => {
    return data;
  })
);

// user management data
export const getUser = () => (
  bspApi.get('/user').then(({ data }) => data)
);
export const getUsers = () => (
  bspApi.get('/user/all').then(({ data }) => data)
);
export const setUserAccess = (action, userId) => (
  bspApi.post(`/user/${action}`, {
    user_account_id: userId
  }).then((result) => {
    if (result.ok) {
      const { access } = result.data;
      return {
        user_account_id: userId,
        access: access
      };
    } else {
      return result.data;
    }
  })
);
export const updateUser = (detailsToUpdate) => (
  bspApi.post('/user/update', detailsToUpdate).then((result) => result)
);

// forex data
export const getHistoricalForexData = (startDate, endDate, base, symbol) => (
  bspApi.get(`/forex/history?start_at=${startDate}&end_at=${endDate}&base=${base}&symbols=${symbol}`).then((result) => {
    return result.data;
  })
);
export const getCurrentForexRate = (date, base, symbol) => (
  bspApi.get(`/forex/${date}?base=${base}&symbols=${symbol}`).then((result) => {
    return result.data;
  })
);

// workspace
export const saveWorkspace = (workspace) => (
  bspApi.post('/workspace/save', {
    ...workspace
  }).then((result) => {
    if (result.ok) {
      return result.data;
    } else {
      return result.data;
    }
  })
);

export const getWorkspace = (id) => (
  bspApi.get(`/workspace?id=${id}`).then((result) => {
    return result;
  }).catch((err) => {
    console.error('getWorkspace', err);
  })
);

export const getAllWorkspace = () => (
  bspApi.get('/workspace/all').then((result) => {
    return result;
  }).catch((err) => {
    console.error('getAllWorkspace', err);
  })
);

export const deleteWorkspace = (id) => (
  bspApi.post('/workspace/delete', {
    workspaceConfigId: id
  }).then((result) => {
    return result;
  }).catch((err) => {
    console.error('deleteWorkspace', err);
  })
);

export default bspApi;