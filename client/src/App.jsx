import React, { useEffect, useRef, lazy, Suspense } from 'react';

import Providers from './components/Providers';
import DefaultLayout from './layouts/DefaultLayout';
import { Router, navigate, globalHistory } from '@reach/router';

import { ErrorBoundary } from 'react-error-boundary';
import Loading from './components/Loading';
import { Fragment } from 'react';
import useWindowFocus from './hooks/useWindowFocus';
import { useAuthStore } from './stores/authStore';
import { includes } from 'lodash-es';
import { authenticate, hasToken } from './helper/auth';
import { retry } from './helper/retryHelper';
import { Box } from '@material-ui/core';
import env from './config/env';
import * as echarts from 'echarts/core';

import Auth from './pages/Auth';
import UnAuthorized from './pages/UnAuthorized';
import ForexPage from './pages/Forex';
import DashboardPage from './pages/Dashboard';
import MyWorkspacePage from './pages/MyWorkspace';
import ErrorFallback from './components/ErrorFallback';
import { echartsLight } from './config/echartsLight';
import { echartsDark } from './config/echartsDark';

const AsyncDashboardEditor = lazy(() => retry(() => import('./pages/DashboardEditor'), 5, 5000));
const AsyncAdmin = lazy(() => retry(() => import('./pages/Admin'), 5, 5000));
const AsyncForexEditor = lazy(() => retry(() => import('./pages/ForexEditor'), 5, 5000));
const AsyncSandbox = lazy(() => retry(() => import('./pages/Sandbox'), 5, 5000));

echarts.registerTheme('light', echartsLight);
echarts.registerTheme('dark', echartsDark);

const Home = () => {
  const [authStore] = useAuthStore();

  useEffect(() => {
    if (authStore.user) {
      if (authStore.user.access === 'allowed') {
        navigate('/my-workspace');
      } else {
        navigate('/unauthorized', { replace: true });
      }
    }
  }, [authStore.user]);

  return (
    <Box />
  );
};

const App = () => {
  const isAuthenticating = useRef(false);
  const [authStore, authActions] = useAuthStore();
  const windowFocused = useWindowFocus();
  const shouldAuth = env.AUTH;

  const initializeAuthentication = async (authActions) => {
    if (!isAuthenticating.current) {
      isAuthenticating.current = true;
      // maybe set authenticate to return the user.access?
      await authenticate();
      if (await hasToken()) {
        authActions.setTokenExistence(true);
      } else {
        authActions.setTokenExistence(false);
      }
      isAuthenticating.current = false;
    }
  };

  const silentlyCheckAuthorization = async (fetchUser) => {
    await fetchUser();
  };

  useEffect(() => {
    if (shouldAuth) {
      initializeAuthentication(authActions);
    }
  }, [authActions, shouldAuth]);

  useEffect(() => {
    if (shouldAuth) {
      if (!authStore.user && authStore.hasToken) {
        authActions.fetchUser();
      }
    }
  }, [authStore.user, authStore.hasToken, authActions, shouldAuth]);

  useEffect(() => {
    if (shouldAuth) {
      if (authStore.user && authStore.user.access !== 'allowed') {
        navigate('/unauthorized', { replace: true });
      }
    }
  }, [authStore.user, shouldAuth]);

  useEffect(() => {
    if (shouldAuth) {
      if (windowFocused && !isAuthenticating.current) {
        silentlyCheckAuthorization(authActions.fetchUser);
      }
    }
  }, [authActions.fetchUser, shouldAuth, windowFocused]);

  useEffect(() => {
    const globalHistoryListener = globalHistory.listen((history) => {
      if (includes(['PUSH', 'POP'], history.action) && !includes(['/unauthorized'], history.location.pathname)) {
        if (shouldAuth) {
          silentlyCheckAuthorization(authActions.fetchUser);
        }
      }
    });
    
    return () => {
      globalHistoryListener();
    };
  }, [authActions.fetchUser, shouldAuth]);

  return (
    <Providers>
      <DefaultLayout >
        <ErrorBoundary
          FallbackComponent={ErrorFallback}
          onError={(props) => {
            console.error('ErrorBoundary', props);
          }}
        >
          <Suspense fallback={<Loading/>}>
            <Router>
              <Auth path='/auth' />
              <Home default path='/' />
              <UnAuthorized path='/unauthorized' />
              {
                (!shouldAuth || Boolean(authStore.user && authStore.user.access)) && (
                  <Fragment>
                    <ForexPage path='/forex' />
                    <DashboardPage path='/dashboard' />
                    <AsyncDashboardEditor path='/dashboard/editor' />
                    <MyWorkspacePage path='/my-workspace' />
                    <AsyncForexEditor path='/forex-editor' />
                    <AsyncSandbox path='/sandbox' />
                    <AsyncAdmin path='/admin' />
                  </Fragment>
                )
              }
            </Router>
          </Suspense>
        </ErrorBoundary>
      </DefaultLayout>
    </Providers>
  );
};

export default App;