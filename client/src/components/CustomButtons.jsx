import { Button } from '@material-ui/core';
import { green, grey, red } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';

export const SaveButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(green[700]),
    backgroundColor: green[700],
    '&:hover': {
      backgroundColor: green[900],
    },
  },
}))(Button);

export const ClearButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(red[700]),
    backgroundColor: red[700],
    '&:hover': {
      backgroundColor: red[900],
    },
  },
}))(Button);

export const LoadButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(grey[700]),
    backgroundColor: grey[700],
    '&:hover': {
      backgroundColor: grey[900],
    },
  },
}))(Button);
