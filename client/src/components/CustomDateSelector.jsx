import { Box, CircularProgress, Button, Divider, Grid, IconButton, Toolbar, Typography } from '@material-ui/core';
import { NavigateNext, NavigateBefore } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { format, isSameDay, max, min } from 'date-fns';
import React, { Fragment, useState, useEffect } from 'react';
import { includes } from 'lodash-es';
import { reportDateFormat, getReport, getMonthsWithData } from '../config/bspApi';

const months = [0,1,2,3,4,5,6,7,8,9,10,11];

const useStyles = makeStyles(() => ({
  toolbar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  gridItems: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
}));

export const CustomDateSelector = (props) => {
  const classes = useStyles();

  const { date = null, dates: datesProps = null, onDateChange = () => {} } = props;
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());
  const [minYear, setMinYear] = useState(1993);
  const [maxYear, setMaxYear] = useState(new Date().getFullYear());

  const getMinDate = (dates) => (
    min(dates)
  );

  const getMaxDate = (dates) => (
    max(dates)
  );

  const handleOnMonthClick = (newDate) => {
    if (onDateChange && typeof onDateChange === 'function') {
      onDateChange(newDate);
    }
  };

  const doesDateHaveData = (dateToCompare) => (
    !includes(datesProps, dateToCompare)
  );

  const subYear = () => (
    setSelectedYear((selectedYear - 1) < minYear ? minYear : (selectedYear - 1))
  );

  const addYear = () => (
    setSelectedYear((selectedYear + 1) > maxYear ? maxYear : (selectedYear + 1))
  );

  useEffect(() => {
    if (datesProps) {
      const dates = datesProps.map((date) => new Date(date));
      setMinYear(getMinDate(dates).getFullYear());
      setMaxYear(getMaxDate(dates).getFullYear());
      setSelectedYear(getMaxDate(dates).getFullYear());
    }
  }, [datesProps]);

  return (
    <Box width={240} height={240}>
      {
        datesProps ? (
          <Grid spacing={1} container justifyContent='center' alignItems='center'>
            <Grid item xs={12}>
              <Toolbar className={classes.toolbar} >
                <IconButton disabled={minYear === selectedYear} onClick={subYear} edge='start' size='small'>
                  <NavigateBefore />
                </IconButton>
                <Typography variant='h6'>{selectedYear}</Typography>
                <IconButton disabled={maxYear === selectedYear} onClick={addYear} edge='end' size='small'>
                  <NavigateNext />
                </IconButton>
              </Toolbar>
              <Divider/>
            </Grid>
            {
              months.map((month) => {
                const newDate = new Date(selectedYear, month, 1);
                return (
                  <Grid className={classes.gridItems} key={`month-${month}`} item xs={4}>
                    <Button
                      disabled={doesDateHaveData(format(newDate, reportDateFormat))}
                      variant={ isSameDay(date, newDate) ? 'outlined' :'text'}
                      onClick={() => handleOnMonthClick(newDate)}
                    >
                      {format(newDate, 'LLL')}
                    </Button>
                  </Grid>
                );
              })
            }
          </Grid>
        ) : (
          <CircularProgress />
        )
      }
    </Box>
  );
};