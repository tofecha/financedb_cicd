import React, { Fragment, lazy, useState, useCallback, useMemo, Suspense } from 'react';

const getHOCDisplayName = WrappedComponent => {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
};

const DefaultLoaderView = () => (
  <Fragment>Loading...</Fragment>
);

const DefaultRetryView = ({ retry }) => (
  <button type="button" onClick={retry}>
    Retry
  </button>
);

const LazyRetry = (promise, LoaderView = DefaultLoaderView, ErrorRetryView = DefaultRetryView) => {
  const RetryWrapper = props => {
    const [loading, setLoading] = useState(true);
    const retry = useCallback(() => setLoading(true), []);
    const LazyComponent = useMemo(
      () =>
        lazy(() =>
          promise().catch(() => {
            setLoading(false);
            return { default: () => <ErrorRetryView retry={retry} /> };
          })
        ),
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [promise, loading]
    );
    return (
      <Suspense fallback={<LoaderView />}>
        <LazyComponent {...props} />
      </Suspense>
    );
  };

  RetryWrapper.displayName = `RetryableLazy(${getHOCDisplayName(RetryWrapper)})`;

  return RetryWrapper;
};

export default LazyRetry;