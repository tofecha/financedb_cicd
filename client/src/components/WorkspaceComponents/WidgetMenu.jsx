import React, { Fragment } from 'react';
import { Button, Menu, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PopupState, { bindMenu, bindToggle } from 'material-ui-popup-state';

const useStyles = makeStyles(() => ({
  widgetMenuList: {
    zIndex: '4200 !important'
  }
}));

export const WidgetMenu = (props) => {
  const classes = useStyles();
  const { startIcon, endIcon, buttonLabel = 'Widget Category', widgets = [], onMenuItemClick } = props; 

  const handleOnMenuItemClick = (popupState, widget) => {
    if (onMenuItemClick && widget) {
      onMenuItemClick(widget);
    }
    popupState.close();
  };
  
  return (
    <PopupState variant='popover' popupId={buttonLabel}>
      {(popupState) => (
        <Fragment>
          <Button startIcon={startIcon} endIcon={endIcon} variant='contained' size='small' color='primary' {...bindToggle(popupState)}>
            { buttonLabel }
          </Button>
          <Menu 
            {...bindMenu(popupState)}
            className={classes.widgetMenuList}
          >
            {
              widgets.map((widget) => (
                <MenuItem key={`add-${widget.id}-widget-button`} onClick={() => {
                  handleOnMenuItemClick(popupState, widget);
                }}>
                  {`Add ${widget.name} Widget`}
                </MenuItem>
              ))
            }
          </Menu>
        </Fragment>
      )}
    </PopupState>
  );
};