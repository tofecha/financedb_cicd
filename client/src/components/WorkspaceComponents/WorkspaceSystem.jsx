import 'react-grid-layout/css/styles.css';

import { Box, Fab, Grid, LinearProgress, Paper, Divider, TextField, Button, Typography, MenuItem, Menu, Dialog, DialogContent, DialogActions, FormControlLabel, Checkbox, CircularProgress, Tooltip, DialogContentText } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';

import React, { Fragment, useEffect, useRef, useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Responsive as ResponsiveReactGridLayout } from 'react-grid-layout';
import { v1 as uuidv1 } from 'uuid';
import { clone, endsWith, find, remove, toLower, trim } from 'lodash-es';
import WidgetContainer from './WidgetContainer';
import { allWidgets } from '../Widgets/WidgetBlock';
import { Add as AddIcon, AddCircle as AddCircleIcon, ArrowDropDown as ArrowDropDownIcon, ArrowDropUp as ArrowDropUpIcon, Dashboard as DashboardIcon, Delete as DeleteIcon, Edit as EditIcon, Save as SaveIcon } from '@material-ui/icons';
import useWindowSize from '../../hooks/useWindowSize';
import { useAppStore } from '../../stores/appStore';
import { useAuthStore, useUserPlatformAccount } from '../../stores/authStore';

import PopupState, { bindMenu, bindToggle } from 'material-ui-popup-state';

import { deleteWorkspace, getAllWorkspace, getWorkspace, saveWorkspace } from '../../config/bspApi';
import { useQuery } from '../../hooks/useQuery';
import { WorkspaceBottomBar } from './WorkspaceBottomBar';
import { navigate } from '@reach/router';
import { slugGenerator } from '../../helper/slugGenerator';
import { RedFab } from '../CustomFab';
import { useLocalStorage } from '../../hooks/useLocalStorage';
import env from '../../config/env';

const filter = createFilterOptions();

const useStyles = makeStyles((theme) => ({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  },
  dashboard: {
    overflowY: 'scroll',
    display: 'flex',
    justifyContent: 'center',
  },
  fabContainer: {
    position: 'absolute',
    bottom: theme.spacing(3),
    right: theme.spacing(3),
    zIndex: 4100,
    '& > *': {
      marginLeft: theme.spacing(1)
    },
  },
  bottomContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: 4000,
  },
  bottomToolbar: {
    '& > *': {
      marginRight: theme.spacing(1)
    },
    '& :last-child': {
      marginRight: theme.spacing(0)
    }
  },
  widgetMenuList: {
    zIndex: '4200 !important'
  },
  workspaceSelectBox: {
    '& > *': {
      marginBottom: theme.spacing(1),
    },
  },
  fakeDropdownLabel: {
    textDecoration: 'underline'
  }
}));

const initialLayouts = {
  md: []
};

const gridItemMargin = [10, 10];
const gridLayoutPadding = [10, 10];

const WorkspaceSystem = (props) => {
  const workspaceIdFromQuery = useQuery('id');
  const classes = useStyles();
  const theme = useTheme();
  
  // workspace states
  const [defaultWorkspace, setDefaultWorkspace] = useLocalStorage('defaultWorkspace', null);
  const [breakpoint, setBreakpoint] = useState('lg');
  const [cols, setCols] = useState(12);
  const [rowHeight, setRowHeight] = useState(0);
  const { width, height } = useWindowSize();
  const [anchorEl, setAnchorEl] = useState(undefined);
  const [dashboardBox, setDashboardBox] = useState({
    width: 1600,
    height: 900
  });
  const [ hideUIForPDF, setHideUIForPDF ] = useState(false);
  const [ , actions] = useAppStore();
  const [ account ] = useUserPlatformAccount();
  const pdfRef = useRef(undefined);
  const isEditing = Boolean(anchorEl);
  const [workspace, setWorkspace] = useState(null);
  
  // workspace loading states
  const [loadingWorkspace, setLoadingWorkspace] = useState(false);
  const [listOfWorkspaces, setListOfWorkspaces] = useState(null);
  const [selectedWorkspace, setSelectedWorkspace] = useState(null);
  const [newWorkspaceDialog, setNewWorkspaceDialog] = useState(false);
  const [isProcessing, setProcessing] = useState(false);
  const [open, setOpen] = React.useState(false);
  const fetchingWorkspaces = open && listOfWorkspaces.length === 0;

  const handleOnClickDownloadPDF = (toPdf) => {
    actions.showPageStateLoading();
    setHideUIForPDF(true);
    setTimeout(() => {
      toPdf();
    }, 2000);
  };

  const handleOnPDFGenerationComplete = () => {
    actions.hidePageStateLoading();
    setHideUIForPDF(false);
  };

  const clearLayout = () => {
    setWorkspace({
      ...workspace,
      layout: initialLayouts,
      items: []
    });
  };

  const onLayoutChange = (e, allLayouts) => {
    setWorkspace({
      ...workspace,
      layout: allLayouts,
    });
  };

  const onBreakpointChange = (breakpoint, cols) => {
    setBreakpoint(breakpoint);
    setCols(cols);
  };

  const addWidgetItem = (widget) => {
    if (widget) {
      const newItems = clone(workspace.items);
      if (widget) {
        const newWidget = {
          i: uuidv1(),
          widgetId: widget.id,
          widgetProps: widget.defaultProps
        };
        newItems.push(newWidget);
        setWorkspace({
          ...workspace,
          items: newItems
        });
      }
    }
  };

  const deleteWidgetItem = (id) => {
    const newItems = clone(workspace.items).filter((item) => (
      item.i !== id
    ));
    setWorkspace({
      ...workspace,
      items: newItems
    });
  };

  const handleUpdateProps = (data) => {
    const { i, ...widgetProps } = data;
    const itemsUpdate = clone(workspace.items);
    const itemToUpdate = remove(itemsUpdate, (item) => item.i === i);
    itemToUpdate[0] = {
      ...itemToUpdate[0],
      widgetProps: widgetProps
    };
    const combined = [...itemsUpdate, ...itemToUpdate];
    const updatedWorkspace = {
      ...workspace,
      items: combined
    };
    setWorkspace(updatedWorkspace);
    saveWorkspace(updatedWorkspace);
  };

  const unlockGrid = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const lockGrid = () => {
    setAnchorEl(null);
    saveWorkspace(workspace);
  };

  // workspace generation system here
  const loadWorkspaceFromDatabase = async (id) => {
    setLoadingWorkspace(true);
    const { ok, data } = await getWorkspace(id);
    if (ok && data) {
      setWorkspace({
        ...data,
        layout: data?.layout || initialLayouts,
        items: data?.items || []
      });
    }
    setLoadingWorkspace(false);
  };
  
  const handleDialogOnAffirmative = async () => {
    setProcessing(true);
    if (selectedWorkspace) {
      if (Object.keys(selectedWorkspace).length === 1 && selectedWorkspace.name) {
        // create
        const trimmedName = trim(selectedWorkspace.name);
        const workspaceConfigId = slugGenerator(trimmedName);

        const newWorkspace = await saveWorkspace({
          workspaceConfigId: workspaceConfigId,
          name: trimmedName,
        });

        if (newWorkspace) {
          navigate(`/my-workspace?id=${workspaceConfigId}`);
        }
      } else {
        // load it
        navigate(`/my-workspace?id=${selectedWorkspace.workspaceConfigId}`);
      }
      handleWorkspaceCreateDialog(false);
      setNewWorkspaceDialog(false);
    }
    setProcessing(false);
  };

  const handleWorkspaceCreateDialog = (open) => {
    // reset
    setSelectedWorkspace(null);
    setNewWorkspaceDialog(open);
  };

  const handleWorkspaceSelect = (workspaceConfigId) => (
    navigate(`/my-workspace?id=${workspaceConfigId}`)
  );

  const handleOnClickDeleteFab = async () => {
    await deleteWorkspace(workspace.workspaceConfigId);
    if (defaultWorkspace === workspace.workspaceConfigId) {
      setDefaultWorkspace(null);
    }
    navigate('/my-workspace');
  };

  const handleOnClickSetAsDefaultWorkspace = () => {
    if (workspace) {
      setDefaultWorkspace(workspace.workspaceConfigId);
    }
  };

  const workspaceDialogButtonText = selectedWorkspace && Object.keys(selectedWorkspace).length === 1 ? 'Create' : 'Open';
  const isDefaultWorkspace = workspace?.workspaceConfigId === defaultWorkspace;

  useEffect(() => {
    setDashboardBox({
      width: (width - 100),
      height: ((height - 100) / 1.78),
    });
  }, [width, height, workspace]);

  useEffect(() => {
    if (cols !== 0) {
      const gridItemWidth = ((dashboardBox.width - gridItemMargin[0] * (cols - 1) - gridLayoutPadding[0] * 2) / cols);
      setRowHeight(gridItemWidth);
    }
  }, [dashboardBox, cols]);

  useEffect(() => {
    if (workspaceIdFromQuery) {
      loadWorkspaceFromDatabase(workspaceIdFromQuery);
    } else if (defaultWorkspace) {
      loadWorkspaceFromDatabase(defaultWorkspace);
    } else if (account.currentWorkspace) {
      loadWorkspaceFromDatabase(account.currentWorkspace);
    } else {
      setWorkspace(null);
    }
  }, [account.currentWorkspace, defaultWorkspace, workspaceIdFromQuery]);

  useEffect(() => {
    let active = true;

    if (!fetchingWorkspaces) {
      return undefined;
    }

    (async () => {
      const response = await getAllWorkspace();
      const { ok, data } = response;  

      if (active && ok) {
        setListOfWorkspaces(data);
      }
    })();

    return () => {
      active = false;
    };
  }, [fetchingWorkspaces]);

  useEffect(() => {
    if (!open) {
      setListOfWorkspaces([]);
    }
  }, [open]);

  return (
    <Box className={classes.dashboard} height={height - (isEditing ? 96 : 48)} width={width}>
      {
        loadingWorkspace || !workspace ? (
          <Box
            display='flex'
            flexDirection='column'
            height={height - 48}
            width={width}
            justifyContent='center'
            alignItems='center'
          >
            {
              loadingWorkspace ? (
                <Box>
                  <LinearProgress />
                  <Typography color='primary' variant='body1'>Loading Dashboard</Typography>
                </Box>
              ) : (
                <Box>
                  {
                    workspaceIdFromQuery && (
                      <Fragment>
                        <Alert severity="error">
                          <AlertTitle>Error</AlertTitle>
                              It seems the workspace does not exist or has been deleted.
                        </Alert>
                        <Box py={2}>
                          <Divider />
                        </Box>
                      </Fragment>
                    )
                  }
                  <Box className={classes.workspaceSelectBox}>
                    <Button startIcon={<DashboardIcon />} variant='outlined' fullWidth onClick={() => handleWorkspaceCreateDialog(true)}>Open an existing workspace</Button>
                    <Button startIcon={<AddIcon />} variant='outlined' fullWidth onClick={() => handleWorkspaceCreateDialog(true)}>Create a new workspace</Button>
                  </Box>
                </Box>
              )
            }
          </Box>
        ) : (
          <Fragment>
            <Box className={classes.fabContainer} display='flex' flexDirection='row' justifyContent='flex-end' alignItems='center'>
              {
                isEditing && (
                  <RedFab size='small' color='secondary' className={classes.fab} onClick={handleOnClickDeleteFab}>
                    <DeleteIcon />
                  </RedFab>
                )
              }
              <Fab size='small' color='secondary' className={classes.fab} onClick={isEditing ? lockGrid : unlockGrid}>
                {isEditing ? <SaveIcon /> : <EditIcon />}
              </Fab>
            </Box>
            <WorkspaceBottomBar
              isEditing={isEditing}
              clearLayout={clearLayout}
              handleOnClickDownloadPDF={handleOnClickDownloadPDF}
              handleOnPDFGenerationComplete={handleOnPDFGenerationComplete}
              addWidgetItem={addWidgetItem}
              pdfRef={pdfRef}
            >
              {
                env.ENVIRONMENT === 'development' && (
                  <Typography variant='body2'>{`breakpoint : ${breakpoint}, cols ${cols}`}</Typography>
                )
              }
              <Button size='small' onClick={handleWorkspaceCreateDialog} startIcon={<AddCircleIcon/>} color='secondary' variant='contained'>
                Create
              </Button>
              <PopupState variant='popover' popupId='Workspace List'>
                {(popupState) => (
                  <Fragment>
                    <Button
                      size='small'
                      startIcon={<DashboardIcon/>}
                      endIcon={popupState.isOpen ? <ArrowDropUpIcon/> : <ArrowDropDownIcon/>}
                      color='secondary'
                      variant='contained'
                      classes={{
                        label: classes.fakeDropdownLabel
                      }}
                      {...bindToggle(popupState)}
                      onClick={(e) => {
                        if (popupState.isOpen) {
                          setOpen(false);
                        } else {
                          setOpen(true);
                        }
                        popupState.toggle(e);
                      }}
                    >
                      {`${endsWith(toLower(workspace?.name), 'workspace') ? workspace?.name : `${workspace?.name} Workspace`}`}
                    </Button>
                    <Menu 
                      {...bindMenu(popupState)}
                      className={classes.widgetMenuList}
                    >
                      {
                        listOfWorkspaces && listOfWorkspaces.map((workspace) => (
                          <MenuItem key={`workspace-${workspace.workspaceConfigId}`} onClick={() => {
                            handleWorkspaceSelect(workspace.workspaceConfigId);
                          }}>
                            {workspace?.name || workspace.workspaceConfigId}
                          </MenuItem>
                        ))
                      }
                    </Menu>
                  </Fragment>
                )}
              </PopupState>
              {
                isDefaultWorkspace || (
                  <Tooltip title='Set this workspace as your default workspace when you visit the "My Workspace" page.'>
                    <Button size='small' onClick={handleOnClickSetAsDefaultWorkspace} startIcon={<AddCircleIcon/>} color='secondary' variant='contained'>
                      Set as default
                    </Button>
                  </Tooltip>
                )
              }
            </WorkspaceBottomBar>
            <Box ref={pdfRef}>
              <Box width={dashboardBox.width} >
                <ResponsiveReactGridLayout
                  width={dashboardBox.width}
                  className='layout'
                  layouts={workspace.layout}
                  breakpoints={{
                    lg: theme.breakpoints.values.lg,
                    md: theme.breakpoints.values.lg,
                    sm: theme.breakpoints.values.md,
                    xs: theme.breakpoints.values.sm,
                    xxs: theme.breakpoints.values.xs
                  }}
                  useCSSTransforms={false}
                  rowHeight={rowHeight}
                  margin={gridItemMargin}
                  containerPadding={gridLayoutPadding}
                  isBounded
                  draggableHandle='.grid-item-draggable'
                  isDraggable={isEditing}
                  isRearrangeable={isEditing}
                  isResizable={isEditing}
                  cols={{ lg: 12, md: 12, sm: 8, xs: 6, xxs: 4 }}
                  onLayoutChange={onLayoutChange}
                  onBreakpointChange={onBreakpointChange}
                  compactType='vertical'
                >
                  {
                    workspace.items.map((gridItem) => {
                      const gridInLayout = find(workspace.layout[breakpoint], ['i', gridItem.i]);
                      const widgetSetting = find(allWidgets, ['id', gridItem.widgetId]);
                      const dataGrid = gridInLayout || {
                        ...widgetSetting.defaultGrid,
                        x: workspace.items.length % (cols || 12),
                        y: 0
                      };
                      return (
                        <WidgetContainer
                          key={gridItem.i}
                          breakpoint={breakpoint}
                          data-grid={dataGrid}
                          widget={gridItem}
                          onDeleteWidget={deleteWidgetItem}
                          onUpdateWidgetProps={handleUpdateProps}
                          isEditing={isEditing}
                          printMode={hideUIForPDF}
                        />
                      );
                    })
                  }
                </ResponsiveReactGridLayout>
                <Box height={64} />
              </Box>
            </Box>
          </Fragment>
        )
      }
      <Dialog disableEscapeKeyDown={isProcessing} maxWidth={false} open={newWorkspaceDialog} onClose={() => handleWorkspaceCreateDialog(false)} >
        <DialogContent>
          <DialogContentText>
            To create a new workspace, just type in the name of the new workspace.
          </DialogContentText>
          <Autocomplete
            onChange={(event, newValue) => {
              if (typeof newValue === 'string') {
                setSelectedWorkspace({
                  name: newValue,
                });
              } else if (newValue && newValue.inputValue) {
                setSelectedWorkspace({
                  name: newValue.inputValue,
                });
              } else {
                setSelectedWorkspace(newValue);
              }
            }}
            filterOptions={(options, params) => {
              const filtered = filter(options, params);
              // Suggest the creation of a new value
              if (params.inputValue !== '') {
                filtered.push({
                  inputValue: params.inputValue,
                  name: `Create "${params.inputValue}" workspace`,
                });
              }
              return filtered;
            }}
            selectOnFocus
            clearOnBlur
            handleHomeEndKeys
            options={listOfWorkspaces}
            getOptionLabel={(option) => {
              // Value selected with enter, right from the input
              if (typeof option === 'string') {
                return option;
              }
              // Add "xxx" option created dynamically
              if (option.inputValue) {
                return option.inputValue;
              }
              // Regular option
              return option.name;
            }}
            renderOption={(option) => option.name}
            freeSolo
            // style={{ width: 300 }}
            loading={fetchingWorkspaces}
            open={open}
            onOpen={() => {
              setOpen(true);
            }}
            onClose={() => {
              setOpen(false);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label='Workspace'
                variant='outlined'
                fullWidth
                InputProps={{
                  ...params.InputProps,
                  endAdornment: (
                    <React.Fragment>
                      {fetchingWorkspaces ? <CircularProgress color="inherit" size={20} /> : null}
                      {params.InputProps.endAdornment}
                    </React.Fragment>
                  ),
                }}        
              />
            )}
          />
        </DialogContent>
        <DialogActions>
          <Button disabled={isProcessing} onClick={() => handleWorkspaceCreateDialog(false)} color='primary'>
            Cancel
          </Button>
          <Button disabled={isProcessing} onClick={handleDialogOnAffirmative} color='primary'>
            { workspaceDialogButtonText }
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

export default WorkspaceSystem;