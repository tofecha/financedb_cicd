/* eslint-disable react/display-name */
import React, { forwardRef, Fragment, useEffect, useRef } from 'react';
import { Paper, Box, Typography, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Close, Settings } from '@material-ui/icons';
import { allWidgets } from '../Widgets/WidgetBlock';
import { find } from 'lodash-es';

const useStyles = makeStyles((theme) => ({
  gridToolbar: {
    cursor: 'all-scroll',
    '& hover': {
      cursor: 'all-scroll'
    }
  },
  gridContainer: {
    position: 'relative',
    overflow: 'hidden',
  },
  gridContainerNoOverflow: {
    overflow: 'hidden',
    position: 'relative',
  },
  gridTitleBar: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    cursor: 'all-scroll',
    '& hover': {
      cursor: 'all-scroll'
    },
    position: 'absolute',
    display: 'flex',
    top: 0, left: 0, right: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    borderBottom: `1px solid ${theme.palette.divider}`,
    zIndex: 1000,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
}));

const WidgetContainer = forwardRef((props, ref) => {
  const classes = useStyles();
  const { breakpoint, printMode, isEditing, children, style, className, widget, onDeleteWidget, onUpdateWidgetProps, ...rest } = props;
  const { w, h } = props['data-grid'];
  const { i, widgetId, widgetProps } = widget;

  const editMode = isEditing;

  const width = parseInt(style.width, 10);
  const height = parseInt(style.height, 10);
  
  const widgetRef = useRef(null);
  const anchorRef = useRef(null);

  const renderWidget = (widgetId) => {
    const widgetToReturn = find(allWidgets, ['id', widgetId]);
    if (widgetToReturn) {
      return (
        React.cloneElement(widgetToReturn.widget,
          {
            ref: widgetRef,
            breakpoint: breakpoint,
            editMode: editMode,
            printMode: printMode,
            width: width - 2,
            height: height - 2,
            w: w,
            h: h,
            onUpdateWidgetProps: handleWidgetPropUpdate,
            onOptionToggle: handleOptionsToggle,
            onDeleteWidget: handleDeleteWidget,
            ...widgetToReturn.defaultProps,
            ...widgetProps,
          }
        )
      );
    } else {
      return null;
    }
  };

  const handleDeleteWidget = () => {
    onDeleteWidget(i);
  };

  const handleOptionsToggle = () => {
    if (widgetRef.current?.isOptionsOpen !== undefined) {
      if (widgetRef.current?.isOptionsOpen) {
        widgetRef.current?.closeOptions();
      } else {
        widgetRef.current?.openOptions(anchorRef.current);
      }
    }
  };

  const handleWidgetPropUpdate = (update) => {
    onUpdateWidgetProps({
      i : i,
      ...widgetProps,
      ...update
    });
  };

  return (
    <Fragment>
      <Paper
        style={{ ...style }}
        ref={ref}
        className={`${className} ${classes.gridContainer}`}
        {...rest}
        variant='outlined'
      >
        {
          editMode && (
            <Box className={`${classes.gridTitleBar}`}>
              <IconButton ref={anchorRef} size='small' onClick={handleOptionsToggle}>
                <Settings />
              </IconButton>
              <Box className={'grid-item-draggable'} display='flex' flexDirection='row' justifyContent='center' alignItems='center' flexGrow={1}>
                <Typography noWrap align='center' variant='overline'><strong>Click & Drag</strong></Typography>
              </Box>
              <IconButton size='small' onClick={handleDeleteWidget}>
                <Close />
              </IconButton>
            </Box>
          ) 
        }
        {
          renderWidget(widgetId)
        }
        {
          children
        }
      </Paper>
    </Fragment>
  );
});

export default React.memo(WidgetContainer);