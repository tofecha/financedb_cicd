import { Box, Button, Divider, TextField, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Apps as AppsIcon, AttachMoney as AttachMoneyIcon, Clear, Dashboard as DashboardIcon, GetApp as GetAppIcon, OpenInBrowser as OpenInBrowserIcon, Share as ShareIcon, ShowChart as ShowChartIcon } from '@material-ui/icons';
import React, { lazy } from 'react';
import { forexWidgets, graphWidgetsList, otherWidgets } from '../Widgets/WidgetBlock';
import { WidgetMenu } from './WidgetMenu';
import { ClearButton, LoadButton } from '../CustomButtons';
import { format } from 'date-fns';

const PDFGenerator = lazy(() => import('../PDFGenerator'));

const useStyles = makeStyles((theme) => ({
  bottomContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: 4000,
  },
  bottomToolbar: {
    '& > *': {
      marginRight: theme.spacing(1)
    },
    '& :last-child': {
      marginRight: theme.spacing(0)
    }
  },
}));

export const WorkspaceBottomBar = (props) => {
  const classes = useStyles();
  const { isEditing, clearLayout, handleOnClickDownloadPDF, handleOnPDFGenerationComplete, addWidgetItem, pdfRef } = props;

  if (isEditing) {
    return (
      <Box className={classes.bottomContainer}>
        <Divider/>
        <Toolbar className={classes.bottomToolbar}>
          {
            props?.children
          }
          <Divider orientation='vertical' flexItem />
          <WidgetMenu startIcon={<ShowChartIcon/>} buttonLabel='Graphs' widgets={graphWidgetsList} onMenuItemClick={addWidgetItem} />
          <WidgetMenu startIcon={<AttachMoneyIcon />} buttonLabel='Forex' widgets={forexWidgets} onMenuItemClick={addWidgetItem} />
          <WidgetMenu startIcon={<AppsIcon/>} buttonLabel='Others' widgets={otherWidgets} onMenuItemClick={addWidgetItem} />
          <Divider orientation='vertical' flexItem />
          <ClearButton startIcon={<Clear/>} variant='outlined' size='small' onClick={clearLayout}>
            Clear Layout
          </ClearButton>
          {/* <Box flexGrow={1}/>
          <ClearButton startIcon={<Clear/>} variant='outlined' size='small' onClick={clearLayout}>
            Clear Layout
          </ClearButton> */}
        </Toolbar>
      </Box>
    );
  }

  return (
    <Box className={classes.bottomContainer}>
      <Toolbar className={classes.bottomToolbar}>
        {
          props?.children
        }
        <Button startIcon={<ShareIcon />} size='small' variant='contained'>
          Share Workspace
        </Button>
        <PDFGenerator
          targetRef={pdfRef}
          filename={`report-${format(new Date(),'yyyy-MM-dd-hh-mm-ss')}`}
          scale={2}
          options={{
            orientation: 'l',
            unit: 'px',
            format: [2863, 1600],
            margin: 10,
            compressPdf: true
          }}
          onComplete={handleOnPDFGenerationComplete}
        >
          {({ toPdf, isProcessing }) => (
            <Button size='small' disabled={isProcessing} variant='contained' startIcon={(
              <GetAppIcon />
            )} onClick={() => {
              handleOnClickDownloadPDF(toPdf);
            }}>
              { isProcessing ? 'Generating PDF' : 'Download as PDF'}
            </Button>
          )}
        </PDFGenerator>
      </Toolbar>
    </Box>
  );
};