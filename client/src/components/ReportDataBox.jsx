import { Box, Divider, IconButton, InputAdornment, Toolbar, Typography, TextField, MenuItem, Popover } from '@material-ui/core';
import { addMonths, format, isSameDay, isValid, parseISO, subMonths } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { Fragment } from 'react';
import { reportDateFormat, getReport, getMonthsWithData } from '../config/bspApi';
import { makeStyles } from '@material-ui/core/styles';
import { ArrowBack, ArrowForward, Refresh } from '@material-ui/icons';
import { useQuery } from 'react-query';
import { first, includes, last } from 'lodash-es';
import { CustomDateSelector } from './CustomDateSelector';

const useStyles = makeStyles((theme) => ({
  graphLabel: {
    marginLeft: theme.spacing(1),
    fontSize: theme.spacing(2)
  },
  toolbarSpacer: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(3),
    '& > *': {
      marginRight: theme.spacing(2)
    },
    '& :last-child': {
      marginRight: theme.spacing(0)
    }
  }
}));

const ReportDataBox = (props) => {
  const classes = useStyles();

  const { minLimit = 3, maxLimit = 12, fetchingEnabled, label, children, limit: defaultLimit = 3 , printMode = false, date: defaultDate = new Date(), onDataChange } = props;
  const [ limit, setLimit ] = useState(3);
  const [ date, setDate ] = useState(defaultDate);
  const [ generatedData, setGeneratedData ] = useState([]);
  const [ monthsWithData, setMonthsWithData ] = useState([]);
  const [ minDate, setMinDate ] = useState(null);
  const [ maxDate, setMaxDate ] = useState(null);

  const [anchorEl, setAnchorEl] = useState(null);

  const query = useQuery(['getReport', {
    date: format(date, reportDateFormat),
    limit
  }], () => (
    getReport(limit, format(date, reportDateFormat))
  ), {
    cacheTime: 2500,
    onSuccess: (data) => {
      setGeneratedData(data);
    }
  });

  useQuery(['getMonthsWithData'], () => (
    getMonthsWithData()
  ), {
    cacheTime: 5000,
    onSuccess: (data) => {
      setMinDate(parseISO(first(data)));
      setMaxDate(parseISO(last(data)));
      setMonthsWithData(data);
      if (!includes(data, format(date, reportDateFormat))) {
        setDate(parseISO(last(data)));
      }
    }
  });

  const { isFetching, refetch } = query;

  const onDateArrowClick = (e, action) => {
    e.stopPropagation();
    let newDate = null;
    if (action === 'next') {
      newDate = addMonths(date, 1);
      if (newDate > maxDate) {
        setDate(maxDate);
      } else {
        setDate(newDate);
      }
    } else {
      newDate = subMonths(date, 1);
      setDate(newDate);
    }

    if (onDataChange) {
      onDataChange('date', newDate);
    }
  };

  const handleDateChange = (date) => {
    setDate(date);
    if (onDataChange) {
      onDataChange('date', date);
    }
  };

  const handleOnLimitChange = (e) => {
    const limit = parseInt(e.target.value) || 3;
    setLimit(limit);
    if (onDataChange) {
      onDataChange('limit', limit);
    }
  };

  const onRefreshClick = (e) => {
    e.stopPropagation();
    refetch();
  };
  
  const createOptions = () => {
    const options = [];
    for (let x = minLimit; x <= maxLimit; x++) {
      options.push(x);
    }
    return options;
  };
  
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'popover' : undefined;
  
  useEffect(() => {
    const newDate = typeof defaultDate === 'string' ? parseISO(defaultDate) : isValid(defaultDate) ? defaultDate : new Date();
    setDate(newDate);
  }, [defaultDate]);

  useEffect(() => {
    setLimit(defaultLimit);
  }, [defaultLimit]);

  return (
    <Fragment>
      <Toolbar disableGutters className={classes.toolbarSpacer}>
        <Typography align='left' variant='h6' noWrap className={classes.graphLabel}>{label}</Typography>
        <Box flexGrow={1} />
        {
          printMode || (
            <Fragment>
              <Typography variant='body1'>Show:</Typography>
              <TextField
                value={limit}
                onChange={handleOnLimitChange}
                type='number'
                min={3}
                max={12}
                select
                style={{
                  width: 100
                }}
              >
                {
                  createOptions().map((option, optionIdx) => (
                    <MenuItem key={`option-${optionIdx}`} value={option}>
                      {`${option} month${option > 1 ? 's' : ''}`}
                    </MenuItem>
                  ))
                }
              </TextField>
              <TextField
                onClick={handleClick}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position='start'>
                      <IconButton size='small' edge='start' disabled={isFetching || minDate === format(date, reportDateFormat)} onClick={(e) => onDateArrowClick(e, 'prev')}>
                        <ArrowBack/>
                      </IconButton>
                    </InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment position='end'>
                      <IconButton size='small' disabled={isFetching} onClick={onRefreshClick}>
                        <Refresh/>
                      </IconButton>
                      <IconButton size='small' edge='end' disabled={isFetching || isSameDay(date, maxDate)} onClick={(e) => onDateArrowClick(e, 'next')}>
                        <ArrowForward/>
                      </IconButton>
                    </InputAdornment>
                  )
                }}
                size='small'
                margin='dense'
                variant='standard'
                value={format(date, 'LLL yyyy')}
                style={{
                  width: 160
                }}
                disabled={isFetching}
              />
            </Fragment>
          )
        }
      </Toolbar>
      <Divider />
      {
        children({ generatedData, isFetching, limit })
      }
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <CustomDateSelector date={date} dates={monthsWithData} onDateChange={handleDateChange} />
      </Popover>
    </Fragment>
  );
};

export default ReportDataBox;