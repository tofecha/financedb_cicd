/* eslint-disable react/display-name */
import React, { useEffect, useRef, useState } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { Button, Typography } from '@material-ui/core';
import { Add, Delete } from '@material-ui/icons';
import { USDFormatter } from '../../config/usdFormatter';
import { trim } from 'lodash-es';

const ListBuilder = (props) => {
  const {
    // requiredRows = [],
    onAddRow,
    onRemoveRow,
    onEditRow,
    rows = [],
    labelKey = 'label',
    valueKey = 'value',
    labelHeader = 'Label',
    valueLabel = 'Value',
    readOnly = false
  } = props;
  
  const newRow = {
    id: -1,
    [labelKey]: undefined,
    [valueKey]: 0,
  };

  const [autoWidth, setAutoWidth] = useState(100);
  const gridRef = useRef(null);

  useEffect(() => {
    if (gridRef.current?.clientWidth) {
      let colWidth = gridRef.current.clientWidth - 110;
      if (colWidth < 200) {
        colWidth = 100;
      } else {
        colWidth = colWidth / 2;
      }
      setAutoWidth(colWidth);
    }
  }, [gridRef.current?.clientWidth]);

  const handleRowChanges = (row) => {
    if (row.id === -1) {
      if (onAddRow && typeof onAddRow === 'function') {
        const value = (row.field === valueKey) ? parseFloat(row.value) : trim(row.value);
        const newRow = {
          ...row,
          [row.field]: value
        };
        
        if (value) {
          newRow[valueKey] = newRow[valueKey] || 0;
          onAddRow(newRow);
        }
      }
    } else {
      if (onEditRow && typeof onEditRow === 'function') {
        const value = (row.field === valueKey) ? parseFloat(row.value) : trim(row.value);
        const updatedRow = {
          ...rows[row.id],
          id: row.id,
          [row.field]: value
        };
        onEditRow(updatedRow);
      }
    }
  };

  const handleRemoveRow = (row) => {
    if (onRemoveRow && typeof onRemoveRow === 'function') {
      onRemoveRow(row);
    }
  };

  const generateData = () => {
    return [
      ...rows?.map((row, index) => ({
        ...row,
        id: index
      })),
      { ...newRow }
    ];
  };

  return (
    <DataGrid
      ref={gridRef}
      columns={[{
        field: 'id',
        hide: true
      }, {
        field: 'rating',
        headerName: ' ',
        width: 20,
        hide: true
      }, {
        field: labelKey,
        headerName: labelHeader,
        editable: !readOnly,
        width: autoWidth,
        disableColumnMenu: true,
        sortable: false,
        renderCell: ({ value }) => {
          return (
            <Typography variant='subtitle1'>
              { value || 'Double click to edit ...' }
            </Typography>
          );
        },
      }, {
        field: valueKey,
        headerName: valueLabel,
        editable: !readOnly,
        width: autoWidth,
        disableColumnMenu: true,
        sortable: false,
        type: 'number',
        renderCell: ({ value }) => {
          return (
            <Typography variant='body1'>
              { USDFormatter.format(value) }
            </Typography>
          );
        }
      }, {
        field: 'actions',
        width: 100,
        headerName: 'Actions',
        disableColumnMenu: true,
        sortable: false,
        renderCell: ({ row }) => {
          return (
            row.id !== -1 && (
              <Button
                size='small'
                startIcon={<Delete />}
                variant='contained'
                onClick={() => handleRemoveRow(row)}
              >
                Remove
              </Button>
            )
          );
        },
        hide: readOnly 
      }]}
      rows={generateData()}
      headerHeight={40}
      rowHeight={36}
      disableSelectionOnClick
      onCellEditCommit={handleRowChanges}
      hideFooter
    />
  );
};

export default ListBuilder;