import React from 'react';
import { Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    marginRight: theme.spacing(3),
  },
  logoContainer: {
    display: 'flex',
    paddingLeft: theme.spacing(3),
    [theme.breakpoints.between('md', 'xl')]: {
      width: theme.drawerWidth - 1,
    }
  },
  titleIcon: {
    marginRight: theme.spacing(1)
  },
  logoDividerMargin: {
    marginRight: theme.spacing(3),
    [theme.breakpoints.between('xs', 'md')] : {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(1),
    }
  },
  spacer: {
    flexGrow: 1,
  }
}));

const EventBar = () => {
  const classes = useStyles();

  return (
    <Toolbar variant='dense' className={classes.toolbar}>
      <Typography variant='body1'>Today | Important Event 1, Important Event 2, Meeting</Typography>
    </Toolbar>
  );
};

export default EventBar;