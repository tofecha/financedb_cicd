import React, { useEffect, useRef } from 'react';
import { Typography, useMediaQuery } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { format } from 'date-fns';
import { memo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDashboardStore } from '../stores/dashboardStore';

const useStyles = makeStyles(() => ({
  digitalTextFace: {
    fontFamily: '"Roboto Mono", monospace',
  },
}));

const TimeDisplay = () => {
  const [store, actions] = useDashboardStore();
  const classes = useStyles();
  const theme = useTheme();
  const isWidthLgAndUp = useMediaQuery(theme.breakpoints.up('lg'));
  const timeDivider = useRef(true);

  useEffect(() => timeDivider.current = store.dateTime.getSeconds() % 2 === 0 ? true : false, [store.dateTime]);

  useEffect(() => {
    const interval = setInterval(() => actions.setDateTime(new Date()), 1000);
    return () => {
      clearInterval(interval);
    };
  }, [actions]);

  return (
    <Typography className={classes.digitalTextFace} variant={isWidthLgAndUp ? 'h6' : 'subtitle2'} >{`${ format(store.dateTime, timeDivider.current ? 'hh:mm:ss a' : 'hh mm ss a') }`}</Typography>
  );
};

export default memo(TimeDisplay);