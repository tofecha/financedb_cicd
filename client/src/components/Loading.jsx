import { Box } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useAppStore } from '../stores/appStore';

const Loading = () => {
  const [ , actions] = useAppStore();
  useEffect(() => {
    actions.showPageStateLoading();
    return () => {
      actions.hidePageStateLoading();
    };
  }, [actions]);
  return <Box />;
};

export default Loading;