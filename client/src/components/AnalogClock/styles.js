const withDefault = (value, defaultValue) => {
  if (value === null || value === undefined) return defaultValue;
  return value;
};

const hand = {
  left: '50%',
  position: 'absolute',
  top: '50%',
  transformOrigin: '50% 100%',
};

const base = {
  backgroundColor: s => s.theme.background,
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  borderRadius: '100%',
  border: s => `${s.width / 20}px solid ${s.theme.border}`,
  height: s => s.width - ((s.width / 20) * 2),
  width: s => s.width - ((s.width / 20) * 2),
  position: 'relative',
};

const center = {
  backgroundColor: s => s.theme.center,
  borderRadius: '100%',
  height: '12px',
  left: '50%',
  position: 'absolute',
  top: '50%',
  transform: 'translateX(-50%) translateY(-50%)',
  width: '12px',
};

const second = Object.assign({}, hand, {
  backgroundColor: s => s.theme.seconds,
  height: s => Math.floor(s.width * 0.35),
  width: s => withDefault(s.theme.secondHandWidth, 3),
});

const minute = Object.assign({}, hand, {
  backgroundColor: s => s.theme.minutes,
  height: s => Math.floor(s.width * 0.35),
  width: s => withDefault(s.theme.minuteHandWidth, 6),
});

const hour = Object.assign({}, hand, {
  backgroundColor: s => s.theme.hour,
  height: s => Math.floor(s.width * 0.2),
  width: s => withDefault(s.theme.hourHandWidth, 8),
});

const smallTick = {
  backgroundColor: s => s.theme.tick,
  height: 6,
  left: '50%',
  position: 'absolute',
  top: 6,
  transformOrigin: s => `0 ${s.width / 2.5}px`,
  width: s => withDefault(s.theme.smallTickWidth, 2),
};

const largeTick = {
  backgroundColor: s => s.theme.tick,
  height: 10,
  left: '50%',
  position: 'absolute',
  top: 10,
  transformOrigin: s => `0 ${s.width / 2.5}px`,
  width: s => withDefault(s.theme.largeTickWidth, 4),
};

export {
  base,
  center,
  second,
  minute,
  hour,
  smallTick,
  largeTick
};
