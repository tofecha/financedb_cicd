import React, { useEffect, useState } from 'react';
import AnalogClockLayout from './AnalogClockLayout';
import * as Styles from './styles';
import { cssTransform } from './util';
import { dark } from './themes';

const AnalogClock = (props) => {
  const { dateTime = undefined, theme = dark, width, showSmallTicks = true } = props;
  const [hour, setHour] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [styles, setStyles] = useState(cssTransform(Styles, { theme, width }));

  useEffect(() => {
    setStyles(cssTransform(Styles, { theme, width }));
  }, [theme, width]);

  useEffect(() => {
    if (dateTime?.getHours) {
      setHour(dateTime?.getHours() || 0);
      setMinutes(dateTime?.getMinutes() || 0);
      setSeconds(dateTime?.getSeconds() || 0);
    }
  }, [dateTime]);

  return (
    <AnalogClockLayout
      seconds={seconds}
      minutes={minutes}
      hour={hour}
      styles={styles}
      width={width}
      theme={theme}
      showSmallTicks={showSmallTicks}
    />
  );
};

export default AnalogClock;

