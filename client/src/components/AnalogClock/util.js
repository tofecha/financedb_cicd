export function cssTransform(styles, props) {
  return Object.keys(styles).reduce((newStyles, rootKey) => {
    const style = styles[rootKey];
    newStyles[rootKey] = Object.keys(style).reduce((newStyle, key) => {
      if (typeof style[key] === 'function') {
        newStyle[key] = style[key](props);
      } else {
        newStyle[key] = style[key];
      }
      return newStyle;
    }, {});
    return newStyles;
  }, {});
}

