import React from 'react';

const ErrorFallback = () => {
  return (
    <p>
      There was an error in loading this page.{' '}
      <span
        style={{ cursor: 'pointer', color: '#0077FF' }}
        onClick={() => {
          window.location.reload();
        }}
      >
        Reload this page
      </span>{' '}
    </p>
  );
};

export default ErrorFallback;