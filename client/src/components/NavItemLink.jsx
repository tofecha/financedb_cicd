import { Box, Button, ListItemIcon } from '@material-ui/core';
import { Link as RouterLink } from '@reach/router';
import { startsWith } from 'lodash-es';
import React, { useMemo, forwardRef } from 'react';
import breadcrumbNameMap from '../config/breadcrumbNameMap';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  box: {
    position: 'absolute',
    bottom: 0,
    left: '1.1rem',
    right: '1.1rem',
    height: '0.15rem'
  },
  selected: {
    // backgroundColor: theme.palette.primary.main,
  },
  selectedText: {
    color: theme.palette.primary.main,
    fontWeight: 'bold',
    backgroundColor: theme.palette.background.default
  },
  unselectedText: {
    fontWeight: 'normal'
  },
  toolbarSpacer: {
    '& > *' : {
      marginRight: theme.spacing(1)
    },
    '& > :last-child' : {
      marginRight: theme.spacing(0)
    }
  },
}));

const NavItemLink = (props) => {
  const classes = useStyles();

  const { icon, to, selected } = props;
  const primary = breadcrumbNameMap[to];
  const isSelected = startsWith(selected, to);
  const boxClassName = `${classes.box} ${isSelected && classes.selected}`;
  const textBoxClassName = `${isSelected ? classes.selectedText : classes.unselectedText}`;

  const renderLink = useMemo(
    () => (
      // eslint-disable-next-line react/display-name
      forwardRef((itemProps, ref) => (
        <RouterLink to={to} ref={ref} { ...itemProps }/>
      ))
    ), [to]
  );

  return (
    <Box key={`listitem-${primary}`} position='relative'>
      <Button startIcon={icon} size='small' color='inherit' variant={isSelected ? 'contained' : 'text'} component={renderLink} className={textBoxClassName}>
        { primary }
      </Button>
      {/* <Box className={boxClassName} /> */}
    </Box>
  );
};

export default NavItemLink;