import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  label: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  percentage: {
    fontSize: 36
  }
}));

const SidebarAssetsCard = (props) => {
  const classes = useStyles();
  const { percentage, label } = props;
  return (
    <Box className={classes.root}>
      <Typography variant='body2' className={classes.label}>
        { label }
      </Typography>
      <Typography align='right' variant='body1' className={classes.percentage}>
        { `${percentage}%`}
      </Typography>
    </Box>
  );
};

export default SidebarAssetsCard;