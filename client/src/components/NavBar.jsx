import React from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import NavItemLink from './NavItemLink';
import { Location } from '@reach/router';
import { Fragment } from 'react';
import { BarChart, Dashboard, DataUsage, Report, ShowChart } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: 'flex',
    flexDirection: 'row',
    '& > *' : {
      marginRight: theme.spacing(1)
    },
    '& > :last-child' : {
      marginRight: theme.spacing(0)
    }
  },
}));

const NavBar = () => {
  const classes = useStyles();
  return (
    <Box className={classes.toolbar}>
      <Location>
        {({ location }) => (
          <Fragment>
            <NavItemLink icon={<ShowChart/>} to='/forex' selected={location.pathname} />
            <NavItemLink icon={<BarChart/>} to='/dashboard' selected={location.pathname} />
            <NavItemLink icon={<Dashboard />} to='/my-workspace' selected={location.pathname} />
            {/* <NavItemLink to='/admin' selected={location.pathname} /> */}
          </Fragment>
        )}
      </Location>
    </Box>
  );
};

export default NavBar;