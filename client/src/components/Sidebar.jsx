import React from 'react';
import { Box, Collapse, Drawer, Hidden, List, Toolbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ListItemLink from './ListItemLink';
import { Location } from '@reach/router';

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: theme.drawerWidth,
  },
  // drawerPaper: {
  //   width: theme.drawerWidth,
  // },
  drawerContainer: {
    overflow: 'auto',
    width: theme.drawerWidth - 1,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const Sidebar = () => {
  const classes = useStyles();

  return (
    <Drawer className={classes.drawer} variant='permanent'>
      <Toolbar />
      <Hidden xlUp>
        <Toolbar variant='dense' />
      </Hidden>
      <Box className={classes.drawerContainer}>
        <Location>
          {({ location }) => (
            <List>
              <ListItemLink to="/dashboard" selected={location.pathname} />
              <ListItemLink to="/reports" selected={location.pathname} />
              <ListItemLink to="/my-workspace" selected={location.pathname} />

              {/* <ListItemLink to="/sandbox" selected={location.pathname} /> */}
            </List>
          )}
        </Location>
      </Box>
    </Drawer>
  );
};

export default Sidebar;