import { Box, Divider, Grid, IconButton, InputAdornment, ListSubheader, MenuItem, Select, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect, useState, memo } from 'react';
import { BSPoldestDate, forexDateFormat, getCurrentForexRate } from '../../config/bspApi';
import { map, omit, pick } from 'lodash-es';
import currencies from '../../data/currencies.json';
import { addDays, format, isSameDay, subDays } from 'date-fns';
import { DatePicker } from '@material-ui/pickers';
import { ArrowBack as ArrowBackIcon, ArrowForward as ArrowForwardIcon, Refresh as RefreshIcon } from '@material-ui/icons';
import ForexHistoricalRate from './ForexHistoricalRate';
import ForexAverageRate from './ForexAverageRate';
import { useAppStore } from '../../stores/appStore';
import { useQuery } from 'react-query';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    marginTop: 10,
  },
  forexBox: {
    position: 'relative'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  inputTextNoGutter: {
    margin: 0
  },
  subheader: {
    backgroundColor: theme.palette.background.default
  },
  sourceSelect: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 80
  },
  boxDivider: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));

const commonCurrencies = pick(currencies, ['USD', 'PHP', 'AUD', 'EUR', 'HKD', 'JPY', 'GBP', 'CHF', 'CAD', 'SGD', 'BHD', 'KWD', 'SAR', 'BND', 'IDR', 'THB', 'AED', 'KRW', 'CNY']);
const otherCurrencies = omit(currencies, ['USD', 'PHP', 'AUD', 'EUR', 'HKD', 'JPY', 'GBP', 'CHF', 'CAD', 'SGD', 'BHD', 'KWD', 'SAR', 'BND', 'IDR', 'THB', 'AED', 'KRW', 'CNY']);

const ForexItem = (props) => {
  const classes = useStyles();
  const [ store ] = useAppStore();
  const maxDate = store.date;
  const { defaultBase = 'USD', defaultSymbol = 'AUD' } = props;

  const [base, setBase] = useState(defaultBase);
  const [symbol, setSymbol] = useState(defaultSymbol);
  const [date, setDate] = useState(maxDate);
  const [fetchDate, setFetchDate] = useState(undefined);

  const [baseValue, setBaseValue] = useState(1);
  const [symbolValue, setSymbolValue] = useState(0);

  const [forexData, setForexData] = useState({});
  const [isForexDataIsEmpty, setForexDataIsEmpty] = useState(JSON.stringify(forexData) === '{}');

  const query = useQuery(['getCurrentForexRate', {
    date: format(date, forexDateFormat),
    base,
    symbol
  }], () => (
    getCurrentForexRate(format(date, forexDateFormat), base, symbol)
  ), {
    cacheTime: 30000,
    onError: (error) => {
      setForexData({ error: error });
    },
    onSuccess: (data) => {
      setForexData(data || {
        error: 'Cannot fetch data'
      });
      setFetchDate(new Date());
    },
  });

  const { refetch, isFetching } = query;

  const calculateSymbolValue = (baseValue) => {
    setBaseValue(baseValue);
    setSymbolValue((forexData?.rates[symbol] * baseValue).toFixed(2));
  };

  const calculateBaseValue = (symbolValue) => {
    setSymbolValue(symbolValue);
    setBaseValue((symbolValue / forexData?.rates[symbol]).toFixed(2));
  };

  const onDateArrowClick = (e, action) => {
    e.stopPropagation();
    if (action === 'next') {
      const newDate = addDays(date, 1);
      if (newDate > maxDate) {
        setDate(maxDate);
      } else {
        setDate(newDate);
      }
    } else {
      const newDate = subDays(date, 1);
      if (newDate < BSPoldestDate) {
        setDate(BSPoldestDate);
      } else {
        setDate(newDate);
      }
    }
  };

  const onRefreshClick = (e) => {
    e.stopPropagation();
    refetch();
  };

  useEffect(() => {
    if (JSON.stringify(forexData) === '{}' || forexData.error) {
      setForexDataIsEmpty(true);
      setBaseValue(0);
      setSymbolValue(0);
    } else {
      setForexDataIsEmpty(false);
      if (baseValue !== 0) {
        setSymbolValue((forexData.rates[symbol] * baseValue).toFixed(2));
      } else {
        setBaseValue(1);
        setSymbolValue(forexData.rates[symbol].toFixed(2));
      }
    }
  }, [baseValue, forexData, symbol]);

  return (
    <Box className={classes.root}>
      <ForexHistoricalRate showYAxis={true} base={base} symbol={symbol} date={date} height={280} enableArea/>
      <Divider />
      <Box className={classes.forexBox} m={2}>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={0}>
            <Grid item xs={12}>
              <Typography variant='body1' noWrap>
                {
                  forexData.error ? forexData.error : isForexDataIsEmpty ? '' : `1 ${currencies[base]}`
                }
              </Typography>
            </Grid>
    
            <Grid item xs={12}>
              <Typography variant='h4' noWrap>
                { 
                  isForexDataIsEmpty ? 'Choose a currency below' : forexData.error ? 'Error' : `${forexData?.rates[symbol]?.toFixed(2)} ${currencies[symbol]}`
                }
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography variant='h6'>
                {
                  isForexDataIsEmpty || forexData.error ? 'No data to display' : `Data date: ${format(new Date(forexData?.date), 'd MMM')}`
                }
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography variant='body2'>
                {
                  `${fetchDate ? `Fetched on: ${format(new Date(fetchDate), 'MMM dd - hh:mm:ss a')}` : ''}`
                }
              </Typography>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <DatePicker
              InputProps={{
                startAdornment: (
                  <InputAdornment position='start'>
                    <IconButton edge='start' disabled={isSameDay(date, BSPoldestDate)} onClick={(e) => onDateArrowClick(e, 'prev')}>
                      <ArrowBackIcon/>
                    </IconButton>
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position='end'>
                    <IconButton onClick={(e) => onRefreshClick(e)}>
                      <RefreshIcon/>
                    </IconButton>

                    <IconButton edge='end' disabled={isSameDay(date, maxDate)} onClick={(e) => onDateArrowClick(e, 'next')}>
                      <ArrowForwardIcon/>
                    </IconButton>
                  </InputAdornment>
                )
              }}
              autoOk
              size='small'
              disabled={isFetching}
              disableFuture
              variant='inline'
              inputVariant='outlined'
              format='LLL dd yyyy'
              margin='dense'
              value={date}
              fullWidth
              minDate={BSPoldestDate}
              maxDate={maxDate}
              onChange={setDate}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField inputMode='numeric' fullWidth margin={'dense'} disabled={isFetching || isForexDataIsEmpty || forexData.error} type='number' min={0.00} variant='outlined' value={baseValue} onChange={(e) => calculateSymbolValue(e.target.value)} className={classes.inputTextNoGutter}/>
          </Grid>
          <Grid item xs={6} >
            <Select margin={'dense'} disabled={isFetching} fullWidth variant='outlined' value={base} onChange={(e) => setBase(e.target.value)}>
              <ListSubheader className={classes.subheader}>Common currencies</ListSubheader>
              { 
                map(commonCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
              <ListSubheader className={classes.subheader}>Other currencies</ListSubheader>
              { 
                map(otherCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
            </Select>
          </Grid>
          <Grid item xs={6} >
            <TextField inputMode='numeric' fullWidth margin={'dense'} disabled={isFetching || isForexDataIsEmpty || forexData.error} type='number' min={0.00} variant='outlined' value={symbolValue} onChange={(e) => calculateBaseValue(e.target.value)} className={classes.inputTextNoGutter}/>
          </Grid>
          <Grid item xs={6} >
            <Select margin={'dense'} disabled={isFetching} fullWidth variant='outlined' value={symbol} onChange={(e) => setSymbol(e.target.value)}>
              <ListSubheader className={classes.subheader}>Common currencies</ListSubheader>
              { 
                map(commonCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
              <ListSubheader className={classes.subheader}>Other currencies</ListSubheader>
              { 
                map(otherCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
            </Select>
          </Grid>
          <Grid item xs={12}>
            <Divider className={classes.boxDivider}/>
            <ForexAverageRate base={base} symbol={symbol}/>
          </Grid>
          <Grid item xs={12}>
            <Divider className={classes.boxDivider}/>
            <Typography variant='caption'>
              {
                'Data from Bangko Sentral ng Pilipinas'
              }
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default memo(ForexItem);