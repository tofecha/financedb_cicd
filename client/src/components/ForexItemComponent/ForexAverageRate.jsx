import { Divider, Grid, Typography } from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import { addDays, differenceInDays, format, isSameDay, subDays, subWeeks } from 'date-fns';
import React, { useState } from 'react';
import { BSPoldestDate, forexDateFormat, getHistoricalForexData } from '../../config/bspApi';
import { sumBy, takeRight } from 'lodash-es';
import { useAppStore } from '../../stores/appStore';
import { useQuery } from 'react-query';

const ForexAverageRate = (props) => {
  const { base = 'USD', symbol = 'PHP' } = props;
  const [ store ] = useAppStore();
  const maxDate = store.date;
  const [startDate, setStartDate] = useState(subWeeks(maxDate, 1));
  const [endDate, setEndDate] = useState(maxDate);
  const [selectedAvg, setSelectedAvg] = useState(0);

  const onQuerySettled = (data, error) => {
    if (data && !data.error) {
      const _startDate = subDays(startDate, 10);
      let currentDate = startDate;
      let currentValue = 0;
      const sanitizedData = [];

      while (currentDate <= endDate) {
        const temp = data?.rates[format(currentDate, forexDateFormat)];
        if (temp) {
          currentValue = parseFloat(temp[symbol]);
        } else {
          let searchDate = currentDate;
          while (!isSameDay(searchDate, _startDate)) {
            const temp = data?.rates[format(searchDate, forexDateFormat)];
            if (temp) {
              currentValue = parseFloat(temp[symbol]);
              break;
            }
            searchDate = subDays(searchDate, 1);
          }
        }

        sanitizedData.push({
          x: format(currentDate, forexDateFormat),
          y: currentValue
        });
        currentDate = addDays(currentDate, 1);
      }

      const diff = differenceInDays(endDate, startDate) + 1;
      const takeRightData = takeRight(sanitizedData, diff);
      setSelectedAvg((sumBy(takeRightData, 'y') / diff).toFixed(4));
    }
  };

  const query = useQuery(['getHistoricalForexData', {
    startDate: format(startDate, forexDateFormat),
    endDate: format(endDate, forexDateFormat),
    base,
    symbol
  }], () => (
    getHistoricalForexData(format(subDays(startDate, 10), forexDateFormat), format(endDate, forexDateFormat), base, symbol)
  ), {
    cacheTime: 900000, // 15 minute cache
    onSettled: onQuerySettled,
  });

  const { isFetching } = query;

  return (
    <Grid container spacing={1} justifyContent='space-between'>
      <Grid item xs={12}>
        <Typography variant='h6'>Average Rates</Typography>
      </Grid>
      <Grid item xs={3}>
        <DatePicker
          size='small'
          disabled={isFetching}
          disableFuture
          autoOk
          disableToolbar
          variant='inline'
          inputVariant='standard'
          format='LLL dd yyyy'
          margin='none'
          label='From'
          value={startDate}
          minDate={BSPoldestDate}
          maxDate={maxDate}
          fullWidth
          onChange={(e) => setStartDate(e)}
        />
      </Grid>
      <Grid item xs={3}>
        <DatePicker
          size='small'
          disabled={isFetching}
          disableFuture
          autoOk
          disableToolbar
          variant='inline'
          inputVariant='standard'
          format='LLL dd yyyy'
          margin='none'
          label='To'
          value={endDate}
          minDate={BSPoldestDate}
          maxDate={maxDate}
          fullWidth
          onChange={(e) => setEndDate(e)}
        />
      </Grid>
      <Divider flexItem orientation='vertical' />
      <Grid container item xs={5} alignContent='center' justifyContent='center'>
        <Typography
          align='center'
          noWrap
          variant='h5'>
          {`${symbol} ${selectedAvg}`}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default ForexAverageRate;