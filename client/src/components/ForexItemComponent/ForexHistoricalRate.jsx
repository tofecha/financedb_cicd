import { first, last, map, takeRight } from 'lodash-es';
import React, { memo, useState } from 'react';
import { addDays, format, isSameDay, subDays } from 'date-fns';
import { forexDateFormat, getHistoricalForexData } from '../../config/bspApi';

import * as echarts from 'echarts/core';
import { LineChart } from 'echarts/charts';
import { SVGRenderer, } from 'echarts/renderers';
import { GridComponent, DatasetComponent, TooltipComponent } from 'echarts/components';
import ReactEChartsCore from 'echarts-for-react/lib/core';
import { greenLine, greyLine, redLine } from '../../config/forexGraphStyles';
import { useQuery } from 'react-query';
import { AppStoreSubscriber } from '../../stores/appStore';

echarts.use(
  [DatasetComponent, GridComponent, TooltipComponent, LineChart, SVGRenderer]
);

const ForexHistoricalRate = (props) => {
  const { showYAxis = false, base = 'USD', symbol = 'PHP', height = 100, date = new Date()} = props;
  const [graphData, setGraphData] = useState([]);

  const onQuerySettled = (data, error) => {
    if (data && !data.error) {
      let startDate = subDays(date, 40);
      let currentDate = startDate;
      let endDate = date;
      let currentValue = 0;
      const sanitizedData = [];

      while (currentDate <= endDate) {
        const temp = data?.rates[format(currentDate, 'yyyy-MM-dd')];
        if (temp) {
          currentValue = parseFloat(temp[symbol]);
        } else {
          let searchDate = currentDate;
          while (!isSameDay(searchDate, startDate)) {
            const temp = data?.rates[format(searchDate, 'yyyy-MM-dd')];
            if (temp) {
              currentValue = parseFloat(temp[symbol]);
              break;
            }
            searchDate = subDays(searchDate, 1);
          }
        }

        sanitizedData.push({
          x: currentDate,
          y: currentValue
        });
        currentDate = addDays(currentDate, 1);
      }

      const prunedData = takeRight(sanitizedData, 30);
      const firstData = first(prunedData);
      const lastData = last(prunedData);

      const graphColorStyles = firstData?.y > lastData?.y ? redLine : firstData?.y < lastData?.y ? greenLine : greyLine;

      setGraphData([
        {
          data: map(prunedData, ({ x, y }) => {
            return [x, y];
          }),
          type: 'line',
          symbol: 'none',
          ...graphColorStyles
        },
      ]);
    }
  };

  const query = useQuery(['getHistoricalForexData', {
    startDate: format(subDays(date, 40), forexDateFormat),
    endDate: format(date, forexDateFormat),
    base,
    symbol
  }], () => (
    getHistoricalForexData(format(subDays(date, 40), forexDateFormat), format(date, forexDateFormat), base, symbol)
  ), {
    cacheTime: 900000, // 15 minute cache
    onSettled: onQuerySettled,
  });

  const options = {
    tooltip: {
      show: true,
      trigger: 'item',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      top: 10,
      right: 0,
      bottom: 20,
      left: showYAxis ? 55 : 0,
      borderWidth: 0,
    },
    xAxis: {
      type: 'time',
      show: true,
      splitNumber: 5,
      axisLabel: {
        formatter: '{M}/{d}',
      },
      axisPointer: {
        snap: true,
        label: {
          formatter: (({ value }) => {
            return format(value, 'MM/dd');
          })
        }
      }
    },
    yAxis: {
      show: showYAxis,
      splitLine: {
        show: false
      },
      splitNumber: 5,
      min: 'dataMin',
      max: 'dataMax',
      type: 'value',
      axisPointer: {
        snap: true,
      },
      axisLabel: {
        formatter: ((value) => {
          return parseFloat(value).toPrecision(4);
        })
      }
    },
    animationEasing: 'elasticOut',
  };

  const { isFetching } = query;

  return (
    <AppStoreSubscriber>
      {({ theme }) => (
        <ReactEChartsCore
          echarts={echarts}
          option={{
            ...options,
            series: graphData
          }}
          notMerge={true}
          lazyUpdate={true}
          opts={{
            // width: width,
            height: height
          }}
          theme={theme}
        />
      )}
    </AppStoreSubscriber>
  );
};

export default memo(ForexHistoricalRate);