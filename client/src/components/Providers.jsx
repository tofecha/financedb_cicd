import React from 'react';
import { ThemeProvider as MaterialThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { AppStoreSubscriber } from '../stores/appStore';
import { darkMatTheme, lightMatTheme } from '../config/matThemes';
import { SnackbarProvider } from 'notistack';
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import env from '../config/env';

const queryClient = new QueryClient();

const Providers = ({ children }) => {
  return (
    <QueryClientProvider client={queryClient}>
      <AppStoreSubscriber>
        {({ theme }) => (
          <MaterialThemeProvider theme={theme === 'dark' ? darkMatTheme : lightMatTheme}>
            <SnackbarProvider>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <CssBaseline />
                { children }
              </MuiPickersUtilsProvider>
            </SnackbarProvider>
          </MaterialThemeProvider>
        )}
      </AppStoreSubscriber>
      {
        env.ENVIRONMENT === 'development' && (
          <ReactQueryDevtools initialIsOpen={false} />
        )
      }
    </QueryClientProvider>
  );
};

export default Providers;