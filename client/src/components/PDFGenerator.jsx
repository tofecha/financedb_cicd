import { useRef, useState } from 'react';
import JsPdf from 'jspdf';
import html2canvas from 'html2canvas';

const PDFGenerator = (props) => {
  const { scale, targetRef, filename, options, onComplete, children } = props;
  const [ isProcessing, setProcessing ] = useState(false);
  const thisTargetRef = useRef();

  const toPdf = async () => {
    if (!isProcessing) {
      setProcessing(true);
      const source = targetRef || thisTargetRef;
      const targetComponent = source.current || source;
  
      if (!targetComponent) {
        throw new Error (
          'Target ref must be used or informed'
        );
      }
  
      const pdf = new JsPdf(options);
      const margin = options.margin || 0;
      const pdfPageWidth = pdf.internal.pageSize.getWidth() - (margin * 2);
      const pdfPageHeight = pdf.internal.pageSize.getHeight() - (margin * 2);

      for (let index = 0; index < targetComponent.children.length; index++) {
        try {
          const html2CanvasResult = await html2canvas(targetComponent.children[index], {
            logging: false,
            useCORS: true,
            scale: scale
          });
          
          if (html2CanvasResult) {
            const imgData = html2CanvasResult.toDataURL();
            const imgProps= pdf.getImageProperties(imgData);

            const pageRatio = Math.round((pdfPageWidth / pdfPageHeight)).toFixed(2);
            const imgRatio = Math.round((imgProps.width / imgProps.height)).toFixed(2);

            if (pageRatio > imgRatio) {
              const fh = pdfPageHeight;
              const fw = imgProps.width * (fh / imgProps.height);
              pdf.addImage(imgData, 'PNG', ((pdfPageWidth - fw) / 2) + margin, ((pdfPageHeight - fh) / 2) + margin, fw, fh, '', 'MEDIUM');  
            } else {
              const fw = pdfPageWidth;
              const fh = imgProps.height * (fw / imgProps.width);
              pdf.addImage(imgData, 'PNG', ((pdfPageWidth - fw) / 2) + margin, ((pdfPageHeight - fh) / 2) + margin, fw, fh, '', 'MEDIUM');  
            }

            if (index + 1 < targetComponent.children.length) {
              pdf.addPage();
            }
          }
        } catch (err) {
          console.error(err);
        }
      }

      pdf.save(filename);
      if (onComplete) {
        onComplete();
      }
      setProcessing(false);
    }
  };

  return (
    children({ toPdf, targetRef, isProcessing })
  );
};

export default PDFGenerator;