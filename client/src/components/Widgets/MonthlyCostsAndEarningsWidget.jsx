import { Button, Checkbox, FormControlLabel } from '@material-ui/core';
import React, { forwardRef, Fragment, memo, useEffect, useImperativeHandle, useRef, useState } from 'react';
import BarAndLineGraph from '../Graphs/BarAndLineGraph';
import ReportDataBox from '../ReportDataBox';
import WidgetMenuPopover from './WidgetMenuPopover';

const MonthlyCostsAndEarningsWidgetDefaults = {
  date: new Date(),
  limit: 3,
  labelOnBar: false
};

const MemoizedBarAndLineGraph = memo(BarAndLineGraph);

const MonthlyCostsAndEarningsWidget = forwardRef((props, innerRef) => {
  const { w, h, breakpoint, editMode, printMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;

  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const date = useRef(MonthlyCostsAndEarningsWidgetDefaults.date);
  const limit = useRef(MonthlyCostsAndEarningsWidgetDefaults.limit);
  const [labelOnBar, setLabelOnBar] = useState(MonthlyCostsAndEarningsWidgetDefaults.labelOnBar);

  const handleDataChanges = (key, value) => {
    if (key === 'date') {
      date.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'limit') {
      limit.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'labelOnBar') {
      setLabelOnBar(value);
      onUpdateWidgetProps({
        [key]: value
      });
    }
  };

  useEffect(() => {
    if (widgetProps) {
      date.current = widgetProps?.date || MonthlyCostsAndEarningsWidgetDefaults.date;
      limit.current = widgetProps?.limit || MonthlyCostsAndEarningsWidgetDefaults.limit;
      setLabelOnBar(widgetProps?.labelOnBar);
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...MonthlyCostsAndEarningsWidgetDefaults,
      date: date.current,
      limit: limit.current,
      labelOnBar: labelOnBar
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [isOptionsOpen, labelOnBar]);


  return (
    <Fragment>
      <ReportDataBox
        fetchingEnabled={!editMode}
        minLimit={3}
        maxLimit={12}
        date={date.current}
        limit={limit.current}
        label='Monthly Costs and Earnings'
        onDataChange={handleDataChanges}
        printMode={printMode}
      >
        {({ generatedData, isFetching }) => (
          <MemoizedBarAndLineGraph labelOnBar={labelOnBar} height={height - 48} isFetching={isFetching} data={generatedData} />
        )}
      </ReportDataBox>
      <WidgetMenuPopover
        open={isOptionsOpen}
        anchorEl={optionButtonRef.current}
        onClose={onOptionToggle}
      >
        <FormControlLabel
          control={
            <Checkbox
              checked={labelOnBar}
              onChange={(event) => {
                handleDataChanges('labelOnBar', event.target.checked);
              }}
              color='primary'
            />
          }
          label='Label on Bar'
        />
        <Button variant='outlined' size='small' onClick={onOptionToggle}>
          Close
        </Button>
      </WidgetMenuPopover>

    </Fragment>
  );
});

export {
  MonthlyCostsAndEarningsWidgetDefaults
};
export default MonthlyCostsAndEarningsWidget;