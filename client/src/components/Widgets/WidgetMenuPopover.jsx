import React from 'react';
import { Popover, Box } from '@material-ui/core';
import widgetStyles from '../../config/widgetStyles';

const WidgetMenuPopover = (props) => {
  const { open, anchorEl, onClose, children } = props;
  const widgetClasses = widgetStyles();

  return (
    <Popover
      open={open}
      anchorEl={anchorEl}
      onClose={onClose}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
      PaperProps={{
        variant: 'outlined'
      }}
      placement='bottom-start'
    >
      <Box width={260} p={1} className={widgetClasses.gridOptions}>
        { children }
      </Box>
    </Popover>
  );
};

export default WidgetMenuPopover;