import { Button, Checkbox, FormControlLabel } from '@material-ui/core';
import React, { useEffect, useRef, useState, useImperativeHandle, forwardRef, memo, Fragment } from 'react';
import SalesCollectionGraph from '../Graphs/SalesCollectionGraph';
import ReportDataBox from '../ReportDataBox';
import WidgetMenuPopover from './WidgetMenuPopover';

const SalesCollectionWidgetDefaults = {
  date: new Date(),
  limit: 3,
  labelOnBar: false
};

const MemoizedSalesCollectionGraph = memo(SalesCollectionGraph);

const SalesCollectionWidget = forwardRef((props, innerRef) => {
  const { w, h, breakpoint, editMode, printMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;

  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const date = useRef(SalesCollectionWidgetDefaults.date);
  const limit = useRef(SalesCollectionWidgetDefaults.limit);
  const [labelOnBar, setLabelOnBar] = useState(SalesCollectionWidgetDefaults.labelOnBar);

  const handleDataChanges = (key, value) => {
    if (key === 'date') {
      date.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'limit') {
      limit.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'labelOnBar') {
      setLabelOnBar(value);
      onUpdateWidgetProps({
        [key]: value
      });
    }
  };

  useEffect(() => {
    if (widgetProps) {
      date.current = widgetProps?.date || SalesCollectionWidgetDefaults.date;
      limit.current = widgetProps?.limit || SalesCollectionWidgetDefaults.limit;
      setLabelOnBar(widgetProps?.labelOnBar);
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...SalesCollectionWidgetDefaults,
      date: date.current,
      limit: limit.current,
      labelOnBar: labelOnBar
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [isOptionsOpen, labelOnBar]);

  return (
    <Fragment>
      <ReportDataBox
        fetchingEnabled={!editMode}
        minLimit={3}
        maxLimit={12}
        date={date.current}
        limit={limit.current}
        label='Total Monthly Sales Collection'
        onDataChange={handleDataChanges}
        printMode={printMode}
      >
        {({ generatedData, isFetching }) => (
          <MemoizedSalesCollectionGraph labelOnBar={labelOnBar} height={height - 48} isFetching={isFetching} data={generatedData} />
        )}
      </ReportDataBox>
      <WidgetMenuPopover
        open={isOptionsOpen}
        anchorEl={optionButtonRef.current}
        onClose={onOptionToggle}
      >
        <FormControlLabel
          control={
            <Checkbox
              checked={labelOnBar}
              onChange={(event) => {
                handleDataChanges('labelOnBar', event.target.checked);
              }}
              color='primary'
            />
          }
          label='Label on Bar'
        />
        <Button variant='outlined' size='small' onClick={onOptionToggle}>
          Close
        </Button>
      </WidgetMenuPopover>

    </Fragment>
  );
});

export {
  SalesCollectionWidgetDefaults
};
export default SalesCollectionWidget;