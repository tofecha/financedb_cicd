/* eslint-disable react/display-name */
import { Box, Typography, TextField, FormControlLabel, Checkbox, Button } from '@material-ui/core';
import React, { useState, useEffect, Fragment, useRef, forwardRef, useImperativeHandle } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { includes, find, startsWith } from 'lodash-es';
import { utcToZonedTime } from 'date-fns-tz';
import { getTimeZones } from '@vvo/tzdb';
import { format } from 'date-fns';
import AnalogClock, { Themes } from '../../AnalogClock/';
import timeZoneList from '../../../config/timezoneList';
import { useDashboardStore } from '../../../stores/dashboardStore';
import WidgetMenuPopover from '../WidgetMenuPopover';

const useStyles = makeStyles((theme) => ({
  digitalTextFace: {
    fontFamily: '"Roboto Mono", monospace',
    fontWeight: 700,
    fill: theme.palette.type === 'dark' ? theme.palette.common.white : theme.palette.common.black,
    textAnchor: 'middle'
  },
  digitalAreaText: {
    fontFamily: '"Noto Serif", serif',
    fill: theme.palette.type === 'dark' ? theme.palette.common.white : theme.palette.common.black,
    textAnchor: 'middle'
  },
  box: {
    resize: 'both',
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  clockContainer: {
    // padding: theme.spacing(3)
  },
}));

const ClockWidgetDefaults = {
  area: 'Manila',
  digital: true,
  military: false
};

const ClockWidget = forwardRef((props, innerRef) => {
  const classes = useStyles();
  const { w, h, breakpoint, editMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;
  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);
  const [store] = useDashboardStore();
  
  const [isDigital, setClockToDigital] = useState(ClockWidgetDefaults.digital);
  const [isMilitary, setMilitaryTime] = useState(ClockWidgetDefaults.military);
  const [area, setArea] = useState(ClockWidgetDefaults.area);
  const [timezone, setTimezone] = useState(null);
  const [timezoneDetails, setTimezoneDetails] = useState(undefined);
  const timeDivider = useRef(true);

  const handleTimezoneChange = (e, newValue) => {
    let area = null;
    if (e.target.value) {
      area = find(newValue.cities, (city) => {
        if (startsWith(e.target.value.toLowerCase(), city.toLowerCase())) return city;
      });
    }
    area = area || newValue.cities[0];
    setArea(area);
    onUpdateWidgetProps({
      area: area
    });
  };

  const handleClockTypeChange = (event) => {
    setClockToDigital(event.target.checked);
    onUpdateWidgetProps({
      digital: event.target.checked
    });
  };

  const handle24Toggle = (event) => {
    setMilitaryTime(event.target.checked);
    onUpdateWidgetProps({
      military: event.target.checked
    });
  };

  useEffect(() => {
    if (widgetProps) {
      setClockToDigital(widgetProps?.digital);
      setArea(widgetProps?.area);
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useEffect(() => {
    if (area) {
      const findTimezone = find(timeZoneList, (currentTz) => {
        if (includes(currentTz.cities, area)) return currentTz;
      });
      setTimezone(findTimezone);
    }
  }, [area]);

  useEffect(() => {
    if (timezone?.name) {
      setTimezoneDetails(find(getTimeZones(), ['name', timezone.name]));
    }
  }, [timezone?.name]);

  useEffect(() => {
    timeDivider.current = store.dateTime.getSeconds() % 2 === 0 ? true : false;
  }, [store.dateTime, editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [isOptionsOpen]);

  const timeValue = timezone ? utcToZonedTime(store.dateTime, timezone.name) : store.dateTime;
  const timeOfDay = format(timezone ? utcToZonedTime(store.dateTime, timezone.name) : store.dateTime, 'aaaaa');

  return (
    <Fragment>
      <Box height={height} width={width} className={classes.box}>
        {
          isDigital ? (
            <Fragment>
              <svg
                width='100%'
                height='100%'
                viewBox={`0 0 ${width} ${height < 0 ? 0 : height}`}
                preserveAspectRatio='xMidYMid meet'
              >
                <text
                  x='50%'
                  y='50%'
                  className={classes.digitalTextFace}
                  fontSize={height * 0.2}
                >
                  {
                    isMilitary ? (
                      format(timeValue, timeDivider.current ? 'HH:mm' : 'HH mm')
                    ) : (
                      format(timeValue, timeDivider.current ? 'hh:mm aa' : 'hh mm aa')
                    )
                  }
                </text>
                <text
                  x='50%'
                  y='67%'
                  className={classes.digitalAreaText}
                  fontSize={height * 0.12}
                >
                  {`${String(area).toUpperCase()}`}
                </text>
                <text
                  x='50%'
                  y='80%'
                  className={classes.digitalAreaText}
                  fontSize={height * 0.095}
                >
                  {`${timezoneDetails?.currentTimeFormat ? timezoneDetails.currentTimeFormat.substring(0, 6) : ''}`}
                </text>
              </svg>
            </Fragment>
          ) : (
            <Fragment>
              <AnalogClock
                theme={timeOfDay === 'a' ? Themes.light : Themes.dark}
                width={height - 20}
                dateTime={timeValue}
                showSmallTicks={false}
              />
              <Typography variant='body2' noWrap>
                <strong>{`${String(area).toUpperCase()} ${timezoneDetails?.currentTimeFormat ? timezoneDetails.currentTimeFormat.substring(0, 6) : ''}`}</strong>
              </Typography>
            </Fragment>
          )
        }
      </Box>
      <WidgetMenuPopover
        open={isOptionsOpen}
        anchorEl={optionButtonRef.current}
        onClose={onOptionToggle}
      >
        <FormControlLabel
          control={
            <Checkbox
              checked={isDigital}
              onChange={handleClockTypeChange}
              color='primary'
            />
          }
          label='Digital'
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={isMilitary}
              onChange={handle24Toggle}
              color='primary'
            />
          }
          label='24 hours'
        />
        <Autocomplete
          fullWidth
          disableClearable
          options={timeZoneList || [{ display: '', name: '', cities: [''] }]}
          getOptionLabel={(option) => option.display}
          size='small'
          autoHighlight
          value={timezone || { display: '', name: '', cities: [''] }}
          onChange={(event, newValue) => handleTimezoneChange(event, newValue)}
          renderInput={(params) => <TextField {...params} variant='standard' />}
          renderOption={(option) => <Typography noWrap variant='body1'>{option.display}</Typography>}
        />
        <Button variant='outlined' size='small' onClick={onOptionToggle}>
          Close
        </Button>
      </WidgetMenuPopover>
    </Fragment>
  );
});

export {
  ClockWidgetDefaults
};
export default ClockWidget;