import { Button, Checkbox, FormControlLabel, ListSubheader, MenuItem, Select } from '@material-ui/core';
import React, { useEffect, useRef, useState, useImperativeHandle, forwardRef, memo } from 'react';
import { Fragment } from 'react';
import RevenueBarGraph from '../Graphs/RevenueBarGraph';
import ReportDataBox from '../ReportDataBox';
import WidgetMenuPopover from './WidgetMenuPopover';

const RevenueBarGraphWidgetDefaults = {
  date: new Date(),
  limit: 3,
  labelOnBar: false
};

const MemoizedRevenueBarGraph = memo(RevenueBarGraph);

const RevenueBarGraphWidget = forwardRef((props, innerRef) => {
  const { w, h, breakpoint, editMode, printMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;

  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const date = useRef(RevenueBarGraphWidgetDefaults.date);
  const limit = useRef(RevenueBarGraphWidgetDefaults.limit);
  const [labelOnBar, setLabelOnBar] = useState(RevenueBarGraphWidgetDefaults.labelOnBar);

  const handleDataChanges = (key, value) => {
    if (key === 'date') {
      date.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'limit') {
      limit.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'labelOnBar') {
      setLabelOnBar(value);
      onUpdateWidgetProps({
        [key]: value
      });
    }
  };

  useEffect(() => {
    if (widgetProps) {
      date.current = widgetProps?.date || RevenueBarGraphWidgetDefaults.date;
      limit.current = widgetProps?.limit || RevenueBarGraphWidgetDefaults.limit;
      setLabelOnBar(widgetProps?.labelOnBar);
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...RevenueBarGraphWidgetDefaults,
      date: date.current,
      limit: limit.current,
      labelOnBar: labelOnBar
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [isOptionsOpen, labelOnBar]);
  
  return (
    <Fragment>
      <ReportDataBox
        fetchingEnabled={!editMode}
        minLimit={3}
        maxLimit={12}
        date={date.current}
        limit={limit.current}
        label='Actual vs Target Revenue'
        onDataChange={handleDataChanges}
        printMode={printMode}
      >
        {({ generatedData, isFetching }) => (
          <MemoizedRevenueBarGraph labelOnBar={labelOnBar} height={height - 48} isFetching={isFetching} data={generatedData} />
        )}
      </ReportDataBox>
      <WidgetMenuPopover
        open={isOptionsOpen}
        anchorEl={optionButtonRef.current}
        onClose={onOptionToggle}
      >
        <FormControlLabel
          control={
            <Checkbox
              checked={labelOnBar}
              onChange={(event) => {
                handleDataChanges('labelOnBar', event.target.checked);
              }}
              color='primary'
            />
          }
          label='Label on Bar'
        />
        <Button variant='outlined' size='small' onClick={onOptionToggle}>
          Close
        </Button>
      </WidgetMenuPopover>
    </Fragment>
  );
});

export {
  RevenueBarGraphWidgetDefaults
};
export default RevenueBarGraphWidget;