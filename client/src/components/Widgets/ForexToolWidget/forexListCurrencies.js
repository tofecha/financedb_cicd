import { pick, omit } from 'lodash-es';
import currencies from '../../../data/currencies.json';

const commonCurrencies = pick(currencies, ['USD', 'PHP', 'AUD', 'EUR', 'HKD', 'JPY', 'GBP', 'CHF', 'CAD', 'SGD', 'BHD', 'KWD', 'SAR', 'BND', 'IDR', 'THB', 'AED', 'KRW', 'CNY']);
const otherCurrencies = omit(currencies, ['USD', 'PHP', 'AUD', 'EUR', 'HKD', 'JPY', 'GBP', 'CHF', 'CAD', 'SGD', 'BHD', 'KWD', 'SAR', 'BND', 'IDR', 'THB', 'AED', 'KRW', 'CNY']);

export {
  commonCurrencies,
  otherCurrencies
};