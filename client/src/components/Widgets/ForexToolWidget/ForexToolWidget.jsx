/* eslint-disable react/display-name */
import { Box, Divider, Grid, IconButton, InputAdornment, ListSubheader, MenuItem, Select, TextField, Typography } from '@material-ui/core';
import React, { useState, useEffect, forwardRef, useImperativeHandle, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useAppStore } from '../../../stores/appStore';
import bspApi, { BSPoldestDate, forexDateFormat, getCurrentForexRate } from '../../../config/bspApi';
import { addDays, format, isSameDay, subDays } from 'date-fns';
import ForexHistoricalRate from '../../ForexItemComponent/ForexHistoricalRate';
import ForexAverageRate from '../../ForexItemComponent/ForexAverageRate';
import currencies from '../../../data/currencies.json';
import { commonCurrencies, otherCurrencies } from './forexListCurrencies';
import { ArrowBack, ArrowForward, Refresh } from '@material-ui/icons';
import { DatePicker } from '@material-ui/pickers';
import { isEmpty, map } from 'lodash-es';
import { useQuery } from 'react-query';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    marginTop: 10,
  },
  box: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  forexBox: {
    position: 'relative'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  inputTextNoGutter: {
    margin: 0
  },
  subheader: {
    backgroundColor: theme.palette.background.default
  },
  sourceSelect: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 80
  },
  boxDivider: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));

const ForexToolWidgetDefaults = {
  base: 'USD',
  symbol: 'PHP',
  source: 'BSP'
};

const ForexToolWidget = forwardRef((props, innerRef) => {
  const classes = useStyles();
  const { w, h, breakpoint, editMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;
  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const [store] = useAppStore();
  const maxDate = store.date;
  const detailRef = useRef(null);
  const [base, setBase] = useState(ForexToolWidgetDefaults.base);
  const [symbol, setSymbol] = useState(ForexToolWidgetDefaults.symbol);
  const [source, setSource] = useState(ForexToolWidgetDefaults.source);
  const oldestDate = BSPoldestDate;
  const [date, setDate] = useState(maxDate);
  const [fetchDate, setFetchDate] = useState(undefined);
  const [baseValue, setBaseValue] = useState(1);
  const [symbolValue, setSymbolValue] = useState(0);
  const [forexData, setForexData] = useState({});
  const [historicalRateHeight, setHistoricalRateHeight] = useState(0);

  const isForexDataIsEmpty = isEmpty(forexData);

  const query = useQuery(['getCurrentForexRate', {
    date: format(date, forexDateFormat),
    base,
    symbol
  }], () => (
    getCurrentForexRate(format(date, forexDateFormat), base, symbol)
  ), {
    cacheTime: 30000,
    onError: (error) => {
      setForexData({ error: error });
    },
    onSuccess: (data) => {
      setForexData(data || {
        error: 'Cannot fetch data'
      });
      setFetchDate(new Date());
    },
  });

  const { refetch, isFetching } = query;

  const calculateSymbolValue = (baseValue) => {
    setBaseValue(baseValue);
    setSymbolValue((forexData?.rates[symbol] * baseValue).toFixed(2));
  };

  const calculateBaseValue = (symbolValue) => {
    setSymbolValue(symbolValue);
    setBaseValue((symbolValue / forexData?.rates[symbol]).toFixed(2));
  };
  
  const calculateGraphHeight = (height) => {
    const graphHeight = (height - detailRef.current.clientHeight) - 60;
    setHistoricalRateHeight(graphHeight < 100 ? 100 : graphHeight > 200 ? 200 : graphHeight);
  };

  const onDateArrowClick = (e, action) => {
    e.stopPropagation();
    if (action === 'next') {
      const newDate = addDays(date, 1);
      if (newDate > maxDate) {
        setDate(maxDate);
      } else {
        setDate(newDate);
      }
    } else {
      const newDate = subDays(date, 1);
      if (newDate < oldestDate) {
        setDate(oldestDate);
      } else {
        setDate(newDate);
      }
    }
  };

  const onRefreshClick = (e) => {
    e.stopPropagation();
    refetch();
  };

  const handleCurrencyChange = (currency, value) => {
    if (currency === 'base') {
      setBase(value);
      onUpdateWidgetProps({
        base: value,
      });
    } else {
      setSymbol(value);
      onUpdateWidgetProps({
        symbol: value
      });
    }
  };

  useEffect(() => {
    if (isForexDataIsEmpty || forexData.error) {
      setBaseValue(0);
      setSymbolValue(0);
    } else {
      if (baseValue !== 0) {
        setSymbolValue((forexData.rates[symbol] * baseValue).toFixed(2));
      } else {
        setBaseValue(1);
        setSymbolValue(forexData.rates[symbol]?.toFixed(2) || 0);
      }
    }
  }, [baseValue, forexData, isForexDataIsEmpty, symbol]);

  useEffect(() => {
    // its in timeout so that the height gets rendered
    // first before calculating the remaining space for the graphHeight
    let timeout = null;
    if (detailRef.current) {
      timeout = setTimeout(() => {
        calculateGraphHeight(height);
      }, 500);
    }
    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [breakpoint, h, height, detailRef]);

  useEffect(() => {
    if (widgetProps) {
      setBase(widgetProps?.base);
      setSymbol(widgetProps?.symbol);
      setSource(widgetProps?.source);
    }
  }, [widgetProps]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...ForexToolWidgetDefaults,
      base: base,
      symbol: symbol,
      source: source
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [base, isOptionsOpen, source, symbol]);

  return (
    <Box height={height} width={width} className={classes.box}>
      {
        historicalRateHeight && (
          <ForexHistoricalRate
            base={base}
            symbol={symbol}
            date={date}
            height={historicalRateHeight}
            enableArea
          />
        )
      }
      {/* <Box height={historicalRateHeight} width={width - 2}>
      </Box> */}
      <Divider />
      <Box ref={detailRef} className={classes.forexBox} m={2}>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={0}>
            <Grid item xs={12}>
              <Typography variant='body1' noWrap>
                {
                  forexData.error ? forexData.error : isForexDataIsEmpty ? '' : `1 ${currencies[base]}`
                }
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography variant='h4' noWrap>
                {
                  isForexDataIsEmpty ? 'Choose a currency below' : forexData.error ? 'Error' : `${forexData?.rates[symbol]?.toFixed(2)} ${currencies[symbol]}`
                }
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography variant='h6'>
                {
                  isForexDataIsEmpty || forexData.error ? 'No data to display' : `Data date: ${format(new Date(forexData?.date), 'd MMM')}`
                }
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography variant='body2'>
                {
                  `${fetchDate ? `Fetched on: ${format(new Date(fetchDate), 'MMM dd - hh:mm:ss a')}` : ''}`
                }
              </Typography>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <DatePicker
              InputProps={{
                startAdornment: (
                  <InputAdornment position='start'>
                    <IconButton edge='start' disabled={isSameDay(date, BSPoldestDate)} onClick={(e) => onDateArrowClick(e, 'prev')}>
                      <ArrowBack />
                    </IconButton>
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position='end'>
                    <IconButton onClick={(e) => onRefreshClick(e)}>
                      <Refresh />
                    </IconButton>

                    <IconButton edge='end' disabled={isSameDay(date, maxDate)} onClick={(e) => onDateArrowClick(e, 'next')}>
                      <ArrowForward />
                    </IconButton>
                  </InputAdornment>
                )
              }}
              autoOk
              size='small'
              disabled={isFetching}
              disableFuture
              variant='inline'
              inputVariant='outlined'
              format='LLL dd yyyy'
              margin='dense'
              value={date}
              fullWidth
              minDate={oldestDate}
              maxDate={maxDate}
              onChange={setDate}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField inputMode='numeric' fullWidth margin={'dense'} disabled={isFetching || isForexDataIsEmpty || forexData.error} type='number' min={0.00} variant='outlined' value={baseValue} onChange={(e) => calculateSymbolValue(e.target.value)} className={classes.inputTextNoGutter} />
          </Grid>
          <Grid item xs={6} >
            <Select margin={'dense'} disabled={isFetching} fullWidth variant='outlined' value={base} onChange={(e) => handleCurrencyChange('base', e.target.value)}>
              <ListSubheader className={classes.subheader}>Common currencies</ListSubheader>
              {
                map(commonCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
              <ListSubheader className={classes.subheader}>Other currencies</ListSubheader>
              {
                map(otherCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
            </Select>
          </Grid>
          <Grid item xs={6} >
            <TextField inputMode='numeric' fullWidth margin={'dense'} disabled={isFetching || isForexDataIsEmpty || forexData.error} type='number' min={0.00} variant='outlined' value={symbolValue} onChange={(e) => calculateBaseValue(e.target.value)} className={classes.inputTextNoGutter} />
          </Grid>
          <Grid item xs={6} >
            <Select margin={'dense'} disabled={isFetching} fullWidth variant='outlined' value={symbol} onChange={(e) => handleCurrencyChange('symbol', e.target.value)}>
              <ListSubheader className={classes.subheader}>Common currencies</ListSubheader>
              {
                map(commonCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
              <ListSubheader className={classes.subheader}>Other currencies</ListSubheader>
              {
                map(otherCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
              }
            </Select>
          </Grid>
          <Grid item xs={12}>
            <Divider className={classes.boxDivider} />
            <ForexAverageRate base={base} symbol={symbol} />
          </Grid>
          <Grid item xs={12}>
            <Divider className={classes.boxDivider} />
            <Typography variant='caption'>
              Data from Bangko Sentral ng Pilipinas
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
});

export {
  ForexToolWidgetDefaults
};
export default ForexToolWidget;