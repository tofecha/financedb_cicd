import React from 'react';
import ForexWidget, { ForexWidgetDefaults } from './ForexWidget';
import ClockWidget, { ClockWidgetDefaults } from './ClockWidget/ClockWidget';
import ForexToolWidget, { ForexToolWidgetDefaults } from './ForexToolWidget/ForexToolWidget';
import SalesCollectionWidget, { SalesCollectionWidgetDefaults } from './SalesCollectionWidget';
import RevenueBarGraphWidget, { RevenueBarGraphWidgetDefaults } from './RevenueBarWidget';
import MonthlyCostsAndEarningsWidget, { MonthlyCostsAndEarningsWidgetDefaults } from './MonthlyCostsAndEarningsWidget';
import PercentageEarningsWidget, { PercentageEarningsWidgetDefaults } from './PercentageEarningsWidget';
import TopClientsWidget, { TopClientsWidgetDefaults } from './TopClientsWidget';
import TopOperatingCostsWidget, { TopOperatingCostsWidgetDefaults } from './TopOperatingCostsWidget';
import FixedAssetsWidget, { FixedAssetsWidgetDefaults } from './FixedAssetsWidget';

const graphWidgetsList = [{
  id: 'sales-collection',
  name: 'Sales Collection',
  widget: <SalesCollectionWidget />,
  defaultProps: SalesCollectionWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 12,
    h: 3,
    minH: 3,
    maxH: 6
  }
}, {
  id: 'revenue-collection',
  name: 'Revenue Graph',
  widget: <RevenueBarGraphWidget />,
  defaultProps: RevenueBarGraphWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 12,
    h: 3,
    minH: 3,
    maxH: 6
  }
}, {
  id: 'monthly-costs-earnings',
  name: 'Monthly Costs and Earnings',
  widget: <MonthlyCostsAndEarningsWidget />,
  defaultProps: MonthlyCostsAndEarningsWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 12,
    h: 3,
    minH: 3,
    maxH: 6
  }
}, {
  id: 'percentage-earnings',
  name: 'Percentage Earnings',
  widget: <PercentageEarningsWidget />,
  defaultProps: PercentageEarningsWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 12,
    h: 3,
    minH: 3,
    maxH: 6
  }
}, {
  id: 'top-clients',
  name: 'Top Clients',
  widget: <TopClientsWidget />,
  defaultProps: TopClientsWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 12,
    h: 3,
    minH: 3,
    maxH: 6
  }
}, {
  id: 'top-oper-costs',
  name: 'Top Operating Costs',
  widget: <TopOperatingCostsWidget />,
  defaultProps: TopOperatingCostsWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 12,
    h: 3,
    minH: 3,
    maxH: 6
  }
}, {
  id: 'fixed-assets',
  name: 'Fixed Assets',
  widget: <FixedAssetsWidget />,
  defaultProps: FixedAssetsWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 12,
    h: 3,
    minH: 3,
    maxH: 6
  }
}];

const forexWidgets = [{
  id: 'forex',
  name: 'Forex',
  widget: <ForexWidget />,
  defaultProps: ForexWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 1,
    maxW: 3,
    h: 3,
    minH: 1,
    maxH: 3
  }
}, {
  id: 'forex-tool',
  name: 'Forex Tool',
  widget: <ForexToolWidget />,
  defaultProps: ForexToolWidgetDefaults,
  defaultGrid: {
    w: 3,
    minW: 3,
    maxW: 5,
    h: 4,
    minH: 4,
    maxH: 6
  }
}];

const otherWidgets = [{
  id: 'clock',
  name: 'World clock',
  widget: <ClockWidget />,
  defaultProps: ClockWidgetDefaults,
  defaultGrid: {
    w: 2,
    minW: 1,
    maxW: 2,
    h: 2,
    minH: 1,
    maxH: 2
  }
}];

const allWidgets = [
  ...graphWidgetsList,
  ...forexWidgets,
  ...otherWidgets
];

export {
  graphWidgetsList,
  forexWidgets,
  otherWidgets,
  allWidgets
};
