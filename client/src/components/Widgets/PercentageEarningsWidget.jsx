import React, { useEffect, useRef, useState, useImperativeHandle, forwardRef, memo } from 'react';
import PercentageEarningBeforeIncomeTaxGraph from '../Graphs/PercentageEarningBeforeIncomeTaxGraph';
import ReportDataBox from '../ReportDataBox';

const PercentageEarningsWidgetDefaults = {
  date: new Date(),
  limit: 3
};

const MemoizedPercentageEarningBeforeIncomeTaxGraph = memo(PercentageEarningBeforeIncomeTaxGraph);

const PercentageEarningsWidget = forwardRef((props, innerRef) => {
  const { w, h, breakpoint, editMode, printMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;

  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const date = useRef(PercentageEarningsWidgetDefaults.date);
  const limit = useRef(PercentageEarningsWidgetDefaults.limit);

  const handleDataChanges = (key, value) => {
    if (key === 'date') {
      date.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'limit') {
      limit.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    }
  };

  useEffect(() => {
    if (widgetProps) {
      date.current = widgetProps?.date || PercentageEarningsWidgetDefaults.date;
      limit.current = widgetProps?.limit || PercentageEarningsWidgetDefaults.limit;
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...PercentageEarningsWidgetDefaults,
      date: date.current,
      limit: limit.current,
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [date, isOptionsOpen, limit]);

  return (
    <ReportDataBox
      fetchingEnabled={!editMode}
      minLimit={3}
      maxLimit={12}
      date={date.current}
      limit={limit.current}
      label='Percentage of Earnings Before Income Tax'
      onDataChange={handleDataChanges}
      printMode={printMode}
    >
      {({ generatedData, isFetching }) => (
        <MemoizedPercentageEarningBeforeIncomeTaxGraph height={height - 48} isFetching={isFetching} data={generatedData} />
      )}
    </ReportDataBox>
  );
});

export {
  PercentageEarningsWidgetDefaults
};
export default PercentageEarningsWidget;