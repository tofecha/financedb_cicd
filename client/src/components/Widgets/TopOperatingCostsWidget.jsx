import { Box, Divider, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { format, parseISO } from 'date-fns/esm';
import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react';
import ListTable from '../Graphs/ListTable';
import ReportDataBox from '../ReportDataBox';

const useStyles = makeStyles((theme) => ({
  graphLabel: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: '0.8rem',
    textTransform: 'uppercase',
  },
}));

const TopOperatingCostsWidgetDefaults = {
  date: new Date(),
  limit: 1
};

const TopOperatingCostsWidget = forwardRef((props, innerRef) => {
  const classes = useStyles();
  const { w, h, breakpoint, editMode, printMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;

  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const date = useRef(TopOperatingCostsWidgetDefaults.date);
  const limit = useRef(TopOperatingCostsWidgetDefaults.limit);

  const handleDataChanges = (key, value) => {
    if (key === 'date') {
      date.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'limit') {
      limit.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    }
  };

  useEffect(() => {
    if (widgetProps) {
      date.current = widgetProps?.date || TopOperatingCostsWidgetDefaults.date;
      limit.current = widgetProps?.limit || TopOperatingCostsWidgetDefaults.limit;
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...TopOperatingCostsWidgetDefaults,
      date: date.current,
      limit: limit.current,
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [date, isOptionsOpen, limit]);

  return (
    <ReportDataBox
      fetchingEnabled={!editMode}
      minLimit={1}
      maxLimit={4}
      date={date.current}
      limit={limit.current}
      label={'Top Operating Costs'}
      onDataChange={handleDataChanges}
      printMode={printMode}
    >
      {({ generatedData, isFetching, limit }) => (
        <Box height={height}>
          <Grid container justifyContent='center' spacing={1}>
            {
              generatedData.map((monthlyData) => {
                return (
                  <Grid key={`client-list-${monthlyData.date}`} item xs={limit > 3 ? 3 : (12 / limit)}>
                    <Typography align='center' className={classes.graphLabel}>{ format(parseISO(monthlyData.date), 'LLL yyyy') }</Typography>
                    <Divider />
                    <ListTable
                      rows={monthlyData?.operating_costs}
                      readOnly={true}
                      labelHeader='Operation'
                      valueLabel='Cost'
                      labelKey='operation_name'
                      valueKey='cost'
                    />
                  </Grid>
                );
              })
            }
          </Grid>
        </Box>
      )}
    </ReportDataBox>
  );
});

export {
  TopOperatingCostsWidgetDefaults
};
export default TopOperatingCostsWidget;