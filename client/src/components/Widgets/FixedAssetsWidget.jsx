import { Box, Divider, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { format, parseISO } from 'date-fns/esm';
import React, { useEffect, useRef, useState, useImperativeHandle, forwardRef } from 'react';
import ListTable from '../Graphs/ListTable';
import ReportDataBox from '../ReportDataBox';

const useStyles = makeStyles((theme) => ({
  graphLabel: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: '0.8rem',
    textTransform: 'uppercase',
  },
}));

const FixedAssetsWidgetDefaults = {
  date: new Date(),
  limit: 1
};

const FixedAssetsWidget = forwardRef((props, innerRef) => {
  const classes = useStyles();
  const { w, h, breakpoint, editMode, printMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;
  
  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const date = useRef(FixedAssetsWidgetDefaults.date);
  const limit = useRef(FixedAssetsWidgetDefaults.limit);

  const handleDataChanges = (key, value) => {
    if (key === 'date') {
      date.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    } else if (key === 'limit') {
      limit.current = value;
      onUpdateWidgetProps({
        [key]: value
      });
    }
  };

  useEffect(() => {
    if (widgetProps) {
      date.current = widgetProps?.date || FixedAssetsWidgetDefaults.date;
      limit.current = widgetProps?.limit || FixedAssetsWidgetDefaults.limit;
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...FixedAssetsWidgetDefaults,
      date: date.current,
      limit: limit.current,
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [date, isOptionsOpen, limit]);

  return (
    <ReportDataBox
      fetchingEnabled={!editMode}
      minLimit={1}
      maxLimit={4}
      date={date.current}
      limit={limit.current}
      label={'Fixed Assets'}
      onDataChange={handleDataChanges}
      printMode={printMode}
    >
      {({ generatedData, limit }) => (
        <Box height={height}>
          <Grid container justifyContent='center' spacing={1}>
            {
              generatedData.map((monthlyData) => {
                return (
                  <Grid key={`client-list-${monthlyData.date}`} item xs={limit > 3 ? 3 : (12 / limit)}>
                    <Typography align='center' className={classes.graphLabel}>{format(parseISO(monthlyData.date), 'LLL yyyy')}</Typography>
                    <Divider />
                    <ListTable
                      rows={monthlyData?.fixed_assets}
                      readOnly={true}
                      labelHeader='Asset'
                      valueLabel='Cost'
                      labelKey='asset_name'
                      valueKey='cost'
                    />
                  </Grid>
                );
              })
            }
          </Grid>
        </Box>
      )}
    </ReportDataBox>
  );
});

export {
  FixedAssetsWidgetDefaults
};
export default FixedAssetsWidget;