/* eslint-disable react/display-name */
import React, { useState, useEffect, useRef, Fragment, useMemo, forwardRef, useImperativeHandle, useCallback } from 'react';
import { Divider, Typography, Button, Box, Select, ListSubheader, MenuItem, setRef, FormControlLabel, Checkbox } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import bspApi, { BSPoldestDate, forexDateFormat, getHistoricalForexData } from '../../config/bspApi';
import { addDays, format, isSameDay, subDays } from 'date-fns';
import currencies from '../../data/currencies.json';
import { map, takeRight, last, first, isEmpty } from 'lodash-es';

import * as echarts from 'echarts/core';
import { LineChart } from 'echarts/charts';
import { SVGRenderer, } from 'echarts/renderers';
import { GridComponent, DatasetComponent, TooltipComponent } from 'echarts/components';
import ReactEChartsCore from 'echarts-for-react/lib/core';
import WidgetMenuPopover from './WidgetMenuPopover';
import { commonCurrencies, otherCurrencies } from './ForexToolWidget/forexListCurrencies';
import { greenLine, greyLine, redLine } from '../../config/forexGraphStyles';
import { useQuery } from 'react-query';
import { AppStoreSubscriber } from '../../stores/appStore';
import { DatePicker } from '@material-ui/pickers';

echarts.use(
  [DatasetComponent, GridComponent, TooltipComponent, LineChart, SVGRenderer]
);

const useStyles = makeStyles((theme) => ({
  forexBox: {
    display:'flex',
    flexDirection:'column',
    justifyContent: 'flex-end',
    overflow: 'hidden',
  }
}));

const ForexAverageRateDefaults = {
  base: 'USD',
  symbol: 'PHP',
  date: new Date(),
  fetchLatest: true
};

const ForexAverageRate = forwardRef((props, innerRef) => {
  const classes = useStyles();
  const { w, h, breakpoint, editMode, printMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;

  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);

  const textBoxRef = useRef(null);
  const [refreshRate, setRefreshRate] = useState(30000); // 5 minutes, 15 minutes, 1 hour
  const [base, setBase] = useState(ForexAverageRateDefaults.base);
  const [symbol, setSymbol] = useState(ForexAverageRateDefaults.symbol);
  const [date, setDate] = useState(ForexAverageRateDefaults.date);
  const [fetchLatest, setFetchLatest] = useState(ForexAverageRateDefaults.fetchLatest);
  const [limit, setLimit] = useState(7);

  const [forexData, setForexData] = useState({});
  const [graphData, setGraphData] = useState([]);

  const isForexDataIsEmpty = isEmpty(forexData);

  const onQuerySettled = (data, error) => {
    if (data && !data.error) {
      const startDate = subDays(date, limit + 5);
      let currentDate = startDate;
      let endDate = date;
      let currentValue = 0;
      const sanitizedData = [];

      while (currentDate <= endDate) {
        const temp = data.rates[format(currentDate, forexDateFormat)];
        if (temp) {
          currentValue = parseFloat(temp[symbol]);
        } else {
          let searchDate = currentDate;
          while (!isSameDay(searchDate, startDate)) {
            const temp = data.rates[format(searchDate, forexDateFormat)];
            if (temp) {
              currentValue = parseFloat(temp[symbol]);
              break;
            }
            searchDate = subDays(searchDate, 1);
          }
        }

        sanitizedData.push({
          x: currentDate,
          y: currentValue
        });
        currentDate = addDays(currentDate, 1);
      }
      const lastDate = last(Object.keys(data.rates));
      setForexData({
        base: base,
        date: lastDate,
        rates: data.rates[lastDate]
      });
      const prunedData = takeRight(sanitizedData, limit);
      const firstData = first(prunedData);
      const lastData = last(prunedData);

      const graphColorStyles = firstData?.y > lastData?.y ? redLine : firstData?.y < lastData?.y ? greenLine : greyLine;

      setGraphData([
        {
          data: map(prunedData, ({ x, y }) => {
            return [x, y];
          }),
          type: 'line',
          symbol: 'none',
          ...graphColorStyles
        },
      ]);
    } else {
      setForexData({ error: error || 'Cannot fetch data' });
    }
  };

  const query = useQuery(['getHistoricalForexData', {
    date: format(fetchLatest ? new Date() : date, forexDateFormat),
    base,
    symbol,
    limit
  }], () => (
    getHistoricalForexData(format(subDays(fetchLatest ? new Date() : date, limit + 5), forexDateFormat), format(fetchLatest ? new Date() : date, forexDateFormat), base, symbol)
  ), {
    cacheTime: 900000, //15 minute cache
    onSettled: onQuerySettled,
  });

  const { refetch, isFetching } = query;

  const updateCurrency = (type, value) => {
    if (type === 'base') {
      setBase(value);
      onUpdateWidgetProps({
        base: value
      });
    } else {
      setSymbol(value);
      onUpdateWidgetProps({
        symbol: value
      });
    }
  };

  const handleDateChange = (date) => {
    console.log('date', date);
    setDate(date);
    onUpdateWidgetProps({
      date: date
    });
  };

  const handleFetchLatest = (latest) => {
    const dateToSet = latest ? new Date() : date;
    setFetchLatest(latest);
    if (latest) {
      setDate(new Date());
    } else {
      setDate(date);
    }
    onUpdateWidgetProps({
      fetchLatest: latest,
      date: dateToSet
    });
  };

  useEffect(() => {
    if (widgetProps) {
      setBase(widgetProps?.base);
      setSymbol(widgetProps?.symbol);
      setFetchLatest(widgetProps?.fetchLatest);
      if (typeof widgetProps?.date === 'string') {
        setDate(new Date(widgetProps?.date));
        onUpdateWidgetProps({
          date: new Date(widgetProps?.date)
        });
      } else {
        setDate(widgetProps?.date);
      }
    }
  }, [onUpdateWidgetProps, widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useEffect(() => {
    if (!editMode && w) {
      setLimit(w >= 2 ? 14 : 7);
    }
  }, [editMode, w]);

  useEffect(() => {
    const interval = setInterval(() => {
      refetch();
    }, refreshRate);
    return () => {
      clearInterval(interval);
    };
  }, [refetch, refreshRate]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    widgetProps: {
      ...ForexAverageRateDefaults,
      base: base,
      symbol: symbol,
      date: date,
      fetchLatest: fetchLatest
      // widget settings to be saved 
    },
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [base, date, fetchLatest, isOptionsOpen, symbol]);

  const graphHeight = textBoxRef.current && (height - textBoxRef.current.clientHeight) > 100 ? height - textBoxRef.current.clientHeight : 100;

  const isLargeTile = (w > 1 && h > 1);

  const options = {
    tooltip: {
      show: isLargeTile,
      trigger: 'item',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      top: 10,
      right: 0,
      bottom: isLargeTile ? 36 : 10,
      left: isLargeTile ? 50 : 0,
      borderWidth: 0,
    },
    xAxis: {
      type: 'time',
      show: isLargeTile,
      axisLabel: {
        formatter: '{M}/{d}',
      },
      axisPointer: {
        snap: true,
        label: {
          formatter: (({ value }) => {
            return format(value, 'MM/dd');
          })
        }
      }
    },
    yAxis: {
      show: isLargeTile,
      splitLine: {
        show: false
      },
      splitNumber: 5,
      min: 'dataMin',
      max: 'dataMax',
      type: 'value',
      axisPointer: {
        snap: true,
      },
      axisLabel: {
        formatter: ((value) => {
          return parseFloat(value).toPrecision(4);
        })
      }
    },
    animationEasing: 'elasticOut',
  };

  return (
    <Fragment>
      <Box height={height} width={width} className={classes.forexBox}>
        <Box height={graphHeight} width={width}>
          {
            graphHeight && (
              <AppStoreSubscriber>
                {({ theme }) => (
                  <ReactEChartsCore
                    echarts={echarts}
                    option={{
                      ...options,
                      series: graphData
                    }}
                    notMerge={true}
                    lazyUpdate={true}
                    opts={{
                      height: graphHeight
                    }}
                    theme={theme}
                  />
                )}
              </AppStoreSubscriber>
            )
          }
        </Box>
        <Box ref={textBoxRef}>
          <Divider />
          <Box p={1}>
            <Typography
              variant={['md', 'lg'].includes(breakpoint) ? 'body1' : 'h6'}
              noWrap
            >
              {
                forexData.error ? forexData.error : isForexDataIsEmpty ? '' : `1 ${isLargeTile ? (currencies[base] || base) : base}`
              }
            </Typography>
            <Typography
              variant={['md', 'lg'].includes(breakpoint) ? 'h6' : 'h4'}
              noWrap
            >
              <strong>
                {
                  forexData.error ? forexData.error : isForexDataIsEmpty ? '' : `${forexData?.rates[symbol]?.toFixed(2)} ${isLargeTile ? (currencies[symbol] || symbol) : symbol}`
                }

              </strong>
            </Typography>
          </Box>
          {
            isLargeTile && (
              <Fragment>
                <Divider />
                <Box p={1}>
                  <Typography variant='caption'>{`Data as of ${forexData?.date}`}</Typography>
                </Box>
              </Fragment>
            )
          }
        </Box>
      </Box>
      <WidgetMenuPopover
        open={isOptionsOpen}
        anchorEl={optionButtonRef.current}
        onClose={onOptionToggle}
      >
        <Select margin={'dense'} disabled={isFetching} fullWidth variant='outlined' value={base} onChange={(e) => updateCurrency('base', e.target.value)}>
          <ListSubheader>Common currencies</ListSubheader>
          {
            map(commonCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
          }
          <ListSubheader>Other currencies</ListSubheader>
          {
            map(otherCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
          }
        </Select>
        <Select margin={'dense'} disabled={isFetching} fullWidth variant='outlined' value={symbol} onChange={(e) => updateCurrency('symbol', e.target.value)}>
          <ListSubheader>Common currencies</ListSubheader>
          {
            map(commonCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
          }
          <ListSubheader>Other currencies</ListSubheader>
          {
            map(otherCurrencies, (currency, key) => <MenuItem key={key} value={key}>{currency}</MenuItem>)
          }
        </Select>
        <DatePicker
          // InputProps={{
          //   startAdornment: (
          //     <InputAdornment position='start'>
          //       <IconButton size='small' edge='start' disabled={isFetching || minDate === format(date, reportDateFormat)} onClick={(e) => onDateArrowClick(e, 'prev')}>
          //         <ArrowBack/>
          //       </IconButton>
          //     </InputAdornment>
          //   ),
          //   endAdornment: (
          //     <InputAdornment position='end'>
          //       <IconButton size='small' disabled={isFetching} onClick={onRefreshClick}>
          //         <Refresh/>
          //       </IconButton>
          //       <IconButton size='small' edge='end' disabled={isFetching || isSameDay(date, maxDate)} onClick={(e) => onDateArrowClick(e, 'next')}>
          //         <ArrowForward/>
          //       </IconButton>
          //     </InputAdornment>
          //   )
          // }}
          label='Date'
          disableToolbar
          disableFuture
          size='small'
          margin='dense'
          variant='inline'
          inputVariant='outlined'
          fullWidth
          format='LLL dd yyyy'
          minDate={BSPoldestDate}
          value={date}
          onChange={handleDateChange}
          autoOk
          animateYearScrolling
          disabled={isFetching || fetchLatest}
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={fetchLatest}
              onChange={(event) => {
                handleFetchLatest(event.target.checked);
              }}
              color='primary'
            />
          }
          label='Get latest Forex data'
        />

        <Button variant='outlined' size='small' onClick={onOptionToggle}>
          Close
        </Button>
      </WidgetMenuPopover>
    </Fragment>
  );
});

export {
  ForexAverageRateDefaults
};
export default ForexAverageRate;