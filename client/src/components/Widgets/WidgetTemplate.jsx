/* eslint-disable react/display-name */
import { Box, Typography, TextField, FormControlLabel, Checkbox, Button } from '@material-ui/core';
import React, { useEffect, Fragment, forwardRef, useImperativeHandle, useRef, useState } from 'react';
import WidgetMenuPopover from './WidgetMenuPopover';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  box: {
    padding: theme.spacing(1),
  }
}));

const WidgetTemplateDefaults = {
  value: 7000
};

const WidgetTemplate = forwardRef((props, innerRef) => {
  const classes = useStyles();
  const { w, h, breakpoint, editMode, width, height, onUpdateWidgetProps, onOptionToggle, onDeleteWidget, ...widgetProps } = props;
  const optionButtonRef = useRef(null);
  const [isOptionsOpen, setOptions] = useState(false);
  const [value, setValue] = useState(7000);

  const updateValue = (value) => {
    setValue(value);
    onUpdateWidgetProps({
      value: value
    });
  };

  useEffect(() => {
    if (widgetProps) {
      setValue(widgetProps?.value || 0);
    }
  }, [widgetProps]);

  useEffect(() => {
    setOptions(false);
  }, [editMode]);

  useImperativeHandle(innerRef, () => ({
    isOptionsOpen: isOptionsOpen,
    openOptions(optionButton) {
      optionButtonRef.current = optionButton;
      setOptions(true);
    },
    closeOptions() {
      optionButtonRef.current = null;
      setOptions(false);
    }
  }), [isOptionsOpen]);

  return (
    <Fragment>
      <Box height={height} width={width} className={classes.box}>
        <TextField value={value} onChange={(e) => {
          updateValue(e.target.value);
        }} />
      </Box>
      <WidgetMenuPopover
        open={isOptionsOpen}
        anchorEl={optionButtonRef.current}
        onClose={onOptionToggle}
      >
        <Button variant='outlined' size='small' onClick={onOptionToggle}>
          Close
        </Button>
      </WidgetMenuPopover>
    </Fragment>
  );
});

export {
  WidgetTemplateDefaults
};
export default WidgetTemplate;