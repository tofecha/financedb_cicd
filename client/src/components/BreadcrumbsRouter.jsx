import React from 'react';
import { useNavigate, useLocation, Link as RouterLink } from '@reach/router';
import { Breadcrumbs, Link, Typography } from '@material-ui/core';
import breadcrumbNameMap from '../config/breadcrumbNameMap';

const LinkRouter = (props) => <Link {...props} component={RouterLink} />;

const BreadcrumbsRouter = () => {
  const location = useLocation();
  const pathnames = location.pathname.split('/').filter((x) => x);
  
  return (
    <Breadcrumbs>
      {
        pathnames && (
          pathnames.map((value, index) => {
            const last = index === pathnames.length - 1;
            const to = `/${pathnames.slice(0, index + 1).join('/')}`;

            return last ? (
              <Typography color='textPrimary' key={to}>
                { breadcrumbNameMap[to] }
              </Typography>
            ) : (
              <LinkRouter color='inherit' to={to} key={to}>
                { breadcrumbNameMap[to] }
              </LinkRouter>
            );
          })
        )
      }
    </Breadcrumbs>
  );
};

export default BreadcrumbsRouter;