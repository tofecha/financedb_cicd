import { Box, Button, Typography, Badge } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { DataGrid } from '@material-ui/data-grid';
import { clone, find, map } from 'lodash-es';
import React, { Fragment, useState, useEffect, useRef } from 'react';
import bspApi from '../../config/bspApi';

const useStyles = makeStyles((theme) => ({
  actionColumn: {
    '& > *': {
      marginRight: theme.spacing(1)
    },
    '& > :last-child': {
      marginRight: theme.spacing(0)
    }
  },
  accessColumn: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    '& > :first-child': {
      marginRight: theme.spacing(1)
    }
  },
  allowed: {
    color: theme.palette.success.dark,
    textTransform: 'capitalize'
  },
  pending: {
    color: theme.palette.grey[500],
    textTransform: 'capitalize'
  },
  unauthorized: {
    color: theme.palette.error.main,
    textTransform: 'capitalize'
  }
}));

const fetchUsers = async () => {
  const fetchUserResponse = await bspApi.get('/user/all');
  if (fetchUserResponse.ok && fetchUserResponse.data) {
    const { data, ...filter } = fetchUserResponse.data;
    console.log('filter is ', filter);
    return map(data, (user) => ({
      ...user,
      id: user.user_account_id
    }));
  }
};

const modifyUserAccess = async (action, userId) => {
  const query = await bspApi.post(`/user/${action}`, {
    user_account_id: userId
  });
  if (query.ok) {
    const { access } = query.data;
    return {
      user_account_id: userId,
      access: access
    };
  } else {
    return query.data;
  }
};

const AdminUserManagementPanel = (props) => {
  const [users, setUsers] = useState([]);
  const classes = useStyles();
  const gridRef = useRef(null);
  const [autoWidth, setAutoWidth] = useState(100);
  
  // const limit = 10;
  // const [page, setPage] = useState(0);
  // const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);
  
  // const handlePageChange = (params) => {
  //   setPage(params.page);
  // };

  const actionAllowUser = async (userId) => {
    setLoading(true);
    const result = await modifyUserAccess('allow', userId);
    if (result.error) {
      console.error(result.error);
    } else {
      const updatedUsers = map(users, (user) => {
        return user.user_account_id === userId ? {
          ...user,
          ...result
        } : user;
      });
      setUsers(updatedUsers);
    }
    setLoading(false);
  };

  const actionDenyUser = async (userId) => {
    setLoading(true);
    const result = await modifyUserAccess('deny', userId);
    if (result.error) {
      console.error(result.error);
    } else {
      const updatedUsers = map(users, (user) => {
        return user.user_account_id === userId ? {
          ...user,
          ...result
        } : user;
      });
      setUsers(updatedUsers);
    }
    setLoading(false);
  };

  const calcDataGridColumnWidth = () => {
    if (gridRef.current?.clientWidth) {
      let colWidth = gridRef.current.clientWidth - 60;
      if (colWidth < 200) {
        colWidth = 100;
      } else {
        colWidth = colWidth / 4;
      }
      setAutoWidth(colWidth);
    }
  };

  const handleFetchUser = async () => {
    setLoading(true);
    const newRows = await fetchUsers();
    setUsers(newRows);
    setLoading(false);
  };

  useEffect(() => {
    handleFetchUser();
    calcDataGridColumnWidth();
  }, []);

  useEffect(() => {
    calcDataGridColumnWidth();
  }, [gridRef.current?.clientWidth]);

  return (
    <DataGrid
      ref={gridRef}
      components={{
        Toolbar: () => (
          <Box display='flex' flexDirection='row'>
            <Button disabled={loading} variant='contained' size='small' onClick={handleFetchUser}>
              {`${loading ? 'Loading users' : 'Load users'}`}
            </Button>
          </Box>
        )
      }}
      disableSelectionOnClick
      loading={loading}
      autoPageSize
      density='compact'
      rows={users}
      columns={[{
        field: 'id',
        headerName: 'ID',
        hide: true
      }, {
        field: 'fullName',
        headerName: 'Name',
        valueGetter: (params) => (
          `${params.getValue(params.id, 'first_name')} ${params.getValue(params.id, 'last_name')}`
        ),
        width: autoWidth,

      }, {
        field: 'company_email',
        headerName: 'Company email',
        width: autoWidth,
      }, {
        field: 'access',
        headerName: 'Access',
        width: autoWidth,
        renderCell: (params) => (
          <Typography className={classes[params.row.access]} variant='caption'>
            {params.row.access}
          </Typography>
        )
      }, {
        field: 'actions',
        headerName: 'Actions',
        renderCell: (params) => {
          const access = params.row.access;
          const userId = params.row.user_account_id;
          return (
            <Box className={classes.actionColumn}>
              {
                (access === 'pending' || access === 'unauthorized') && (
                  <Button disabled={loading} variant='outlined' size='small' onClick={() => actionAllowUser(userId)}>
                    Allow
                  </Button>
                )
              }
              {
                (access === 'pending' || access === 'allowed') && (
                  <Button disabled={loading} variant='outlined' size='small' onClick={() => actionDenyUser(userId)}>
                    Deny
                  </Button>
                )
              }
            </Box>
          );
        },
        width: autoWidth,
      }]}
    />
  );
};

export default AdminUserManagementPanel;

