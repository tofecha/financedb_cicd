import { Typography } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { List } from '@material-ui/core';
import { ListItem } from '@material-ui/core';
import { Checkbox } from '@material-ui/core';
import { ListItemSecondaryAction } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import { Tooltip } from '@material-ui/core';
import { ListItemText } from '@material-ui/core';
import { ListItemIcon } from '@material-ui/core';
import { InputAdornment } from '@material-ui/core';
import { Box } from '@material-ui/core';
import { Add, ChevronRight, PlaylistAdd } from '@material-ui/icons';
import { chain, isEmpty, includes, startsWith } from 'lodash-es';
import React from 'react';
import { Fragment } from 'react';
import { RPMode } from '../../config/enums';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  buttonSpacer: {
    '& button': {
      marginRight: theme.spacing(1)
    }
  },
  addButton: {
    marginLeft: theme.spacing(1)
  }
}));

const PermissionsPanel = (props) => {
  const { slugGenerator, permissions, mode, selected, permissionText, setupModeToDefault, setPermissionText, handleAddPermission, checkBoxClicked, doesRoleHavePermission, setupModeToPermissionsToRole } = props;
  const classes = useStyles();
  return (
    <Fragment>
      {
        mode === RPMode.PERMISSION_TO_ROLES ? (
          <Box height={400} display='flex' flexDirection='column' justifyContent='center'>
            <Grid container alignItems='center' spacing={1}>
              <Grid item xs={10}>
                <Typography variant='h6'>Select the roles for the permission:</Typography>
                <Typography variant='body1'>{selected?.permission}</Typography>
              </Grid>
              <Grid item xs={2}>
                <Box display='flex' alignItems='flex-start' justifyContent='flex-end'>
                  <ChevronRight fontSize='large' />
                </Box>
              </Grid>
              <Grid item xs={12}>
              </Grid>
            </Grid>
            <Box className={classes.buttonSpacer}>
              <Button size='small' variant='contained' onClick={setupModeToDefault}>Done</Button>
            </Box>
          </Box>
        ) : (
          <Box height={400} display='flex' flexDirection='column' justifyContent='center'>
            <Typography variant='h6'>Permissions</Typography>
            <TextField
              variant='outlined'
              margin='dense'
              fullWidth
              placeholder='search or add permission'
              value={permissionText}
              onChange={(e) => {
                setPermissionText(slugGenerator(e.target.value) || '');
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment>
                    <Button
                      className={classes.addButton}
                      variant='contained'
                      startIcon={<Add />}
                      size='small'
                      disabled={includes(permissions, permissionText) || !permissionText}
                      onClick={handleAddPermission}
                    >
                          Add
                    </Button>
                  </InputAdornment>
                )
              }}
            />
            <Box height={400} style={{ overflowY: 'scroll'}}>
              <List dense>
                {
                  chain(permissions).filter((value) => {
                    if (isEmpty(permissionText)) {
                      return value;
                    } else {
                      return startsWith(value, permissionText);
                    }
                  }).map((permission) => (
                    <ListItem
                      button={mode === RPMode.ROLE_TO_PERMISSIONS}
                      disableGutters
                      key={`permission-${permission}`}
                      onClick={() => {
                        if (mode === RPMode.ROLE_TO_PERMISSIONS) {
                          checkBoxClicked(selected?.role, permission);
                        }
                      }}
                    >
                      {
                        mode === RPMode.ROLE_TO_PERMISSIONS && (
                          <ListItemIcon>
                            <Checkbox
                              size='small'
                              checked={doesRoleHavePermission(selected?.role, permission)}
                            />
                          </ListItemIcon>
                        )
                      }
                      <ListItemText primary={permission} primaryTypographyProps={{ noWrap: true }}/>
                      {
                        mode === RPMode.DEFAULT && (
                          <ListItemSecondaryAction>
                            <Tooltip title={`Add roles to ${permission}`}>
                              <IconButton
                                size='small'
                                variant='outlined'
                                onClick={() => setupModeToPermissionsToRole({ permission: permission})}
                              >
                                <PlaylistAdd />
                              </IconButton>
                            </Tooltip>
                          </ListItemSecondaryAction>
                        )
                      }
                    </ListItem>
                  )).value()
                }
              </List>
            </Box>
          </Box>
        )
      }
    </Fragment>
  );
};

export default PermissionsPanel;