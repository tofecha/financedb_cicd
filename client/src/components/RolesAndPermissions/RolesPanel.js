import { Typography } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { List } from '@material-ui/core';
import { ListItem } from '@material-ui/core';
import { Checkbox } from '@material-ui/core';
import { ListItemSecondaryAction } from '@material-ui/core';
import { IconButton } from '@material-ui/core';
import { Tooltip } from '@material-ui/core';
import { ListItemText } from '@material-ui/core';
import { ListItemIcon } from '@material-ui/core';
import { InputAdornment } from '@material-ui/core';
import { Box } from '@material-ui/core';
import { Add, ChevronRight, Lock, PlaylistAdd } from '@material-ui/icons';
import { chain, isEmpty, includes, startsWith } from 'lodash-es';
import React from 'react';
import { Fragment } from 'react';
import { ROLES, RPMode } from '../../config/enums';
import { makeStyles } from '@material-ui/core/styles';
import slugGenerator from '../../helper/slugGenerator';

const useStyles = makeStyles((theme) => ({
  buttonSpacer: {
    '& button': {
      marginRight: theme.spacing(1)
    }
  },
  addButton: {
    marginLeft: theme.spacing(1)
  }
}));

const RolesPanel = (props) => {
  const { roles, mode, selected, handleAddRole, setupModeToDefault, roleText, setRoleText, checkBoxClicked, doesRoleHavePermission, setupModeToRoleToPermissions } = props;
  const classes = useStyles();
  return (
    <Fragment>
      {
        mode === RPMode.ROLE_TO_PERMISSIONS ? (
          <Box height={400} display='flex' flexDirection='column' justifyContent='center'>
            <Grid container alignItems='center' spacing={1}>
              <Grid item xs={8}>
                <Typography variant='h6'>Select permissions for the role:</Typography>
                <Typography variant='body1'>{selected?.role}</Typography>
              </Grid>
              <Grid item xs={4}>
                <Box display='flex' alignItems='flex-start' justifyContent='center'>
                  <ChevronRight fontSize='large' />
                </Box>
              </Grid>
              <Grid item xs={12}>
              </Grid>
            </Grid>
            <Box className={classes.buttonSpacer}>
              <Button size='small' variant='contained' onClick={setupModeToDefault}>Done</Button>
            </Box>
          </Box>
        ) : (
          <Box height={400} style={{ overflowY: 'scroll'}}>
            <Typography variant='h6'>Roles</Typography>
            <TextField
              variant='outlined'
              margin='dense'
              fullWidth
              placeholder='search or add role'
              value={roleText}
              onChange={(e) => {
                setRoleText(slugGenerator(e.target.value) || '');
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment>
                    <Button
                      className={classes.addButton}
                      variant='contained' 
                      startIcon={<Add />}
                      size='small'
                      disabled={includes(roles, roleText) || !roleText}
                      onClick={handleAddRole}
                    >
                      Add
                    </Button>
                  </InputAdornment>
                )
              }}
            />
            <List dense>
              {
                chain(roles).filter((value) => {
                  if (isEmpty(roleText)) {
                    return value;
                  } else {
                    return startsWith(value, roleText);
                  }
                }).map((role) => (
                  <ListItem
                    button={role === ROLES.SUPER_ADMIN ? false : mode === RPMode.PERMISSION_TO_ROLES}
                    disableGutters
                    key={`role-${role}`}
                    onClick={() => {
                      if (mode === RPMode.PERMISSION_TO_ROLES && role !== ROLES.SUPER_ADMIN) {
                        checkBoxClicked(role, selected?.permission);
                      }
                    }}
                  >
                    {
                      mode === RPMode.PERMISSION_TO_ROLES && (
                        <ListItemIcon>
                          <Checkbox
                            disabled={role === ROLES.SUPER_ADMIN}
                            size='small'
                            checked={role === ROLES.SUPER_ADMIN || doesRoleHavePermission(role, selected?.permission)}
                          />
                        </ListItemIcon>
                      )
                    }
                    <ListItemText primary={role} primaryTypographyProps={{ noWrap: true }}/>
                    {
                      mode === RPMode.DEFAULT && (
                        <ListItemSecondaryAction>
                          {
                            role === ROLES.SUPER_ADMIN ? (
                              <Tooltip title={`'${role}' by default has all permissions allowed.`}>
                                <span>
                                  <IconButton disabled edge='end' size='small' variant='outlined' >
                                    <Lock />
                                  </IconButton>
                                </span>
                              </Tooltip>
                            ) : (
                              <Tooltip title={`Add permissions to ${role}`}>
                                <IconButton
                                  edge='end'
                                  size='small'
                                  variant='outlined'
                                  onClick={() => setupModeToRoleToPermissions({ role: role })}
                                >
                                  <PlaylistAdd />
                                </IconButton>
                              </Tooltip>
                            )
                          }
                        </ListItemSecondaryAction>
                      )
                    }
                  </ListItem>
                )).value()
              }
            </List>
          </Box>

        )
      }
    </Fragment>
  );
};

export default RolesPanel;