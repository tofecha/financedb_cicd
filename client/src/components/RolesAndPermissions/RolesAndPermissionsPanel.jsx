import React, { Fragment, useEffect, useState } from 'react';

import { Checkbox, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { DataGrid, GridToolbarContainer } from '@material-ui/data-grid';
import { includes, map, pull, clone, find } from 'lodash-es';
import { Tooltip } from '@material-ui/core';
import { Box } from '@material-ui/core';
import { Divider } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { ChangeHistory, CloudDownload } from '@material-ui/icons';
import { Dialog } from '@material-ui/core';
// import RolesAndPermissionEditor from './RolesAndPermissionEditor';
import { DialogTitle } from '@material-ui/core';
import { DialogContent } from '@material-ui/core';
import { DialogActions } from '@material-ui/core';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    '& button': {
      marginRight: theme.spacing(1)
    }
  },
}));

const RolesAndPermissionsPanel = (props) => {
  const {
    slugGenerator,
    roleList,
    permissionList,
    fetchPermissions,
    fetchRoles,
    fetchCurrentRolePermissions,
    setRolePermission,
    addRole,
    addPermission
  } = props;
  const classes = useStyles();

  const { enqueueSnackbar } = useSnackbar();

  const [ roles, setRoles ] = useState(undefined);
  const [ permissions, setPermissions ] = useState(undefined);

  const [ rows, setRows ] = useState([]);
  const [ columns, setColumns ] = useState([]);
  const [ rolesPermissionsData, setRolesPermissionsData ] = useState([]);

  const [ editorDialog, setEditorDialog ] = useState(false);

  const renderDataGrid = (roles, permissions) => {
    const columns = map(roles, (data) => {
      return ({
        field: data,
        width: 120,
        renderHeader: () => (
          <Tooltip title={data}>
            <Typography variant='overline' noWrap>{data}</Typography>
          </Tooltip>
        ),
        renderCell: (params) => {
          const { field: role, row } = params;
          const { permission } = row;      
          const checked = role === roleList.SUPER_ADMIN || includes(find(rolesPermissionsData, ['role_name', role])?.permissions, permission);

          return (
            <Checkbox disabled={role === roleList.SUPER_ADMIN} checked={checked} onClick={() => handleOnCheckboxClick(params)} />
          );
        }
      });
    });
    columns.unshift({
      field: 'permission',
      renderHeader: () => (
        <Typography variant='body1'>Permissions</Typography>
      ),
      width: 220
    });
    setColumns(columns);
    const rows = map(permissions, (permission, index) => {
      return ({
        id: index,
        permission: permission
      });
    });
    setRows(rows);
  };

  const handleOnCheckboxClick = async (params) => {
    let notiMessage = 'Success';
    let notiOpt = {
      variant: 'success',
      preventDuplicate: true,
      autoHideDuration: 3000
    };
    
    const { field: role, row } = params;
    const { permission } = row;
    
    const current = clone(rolesPermissionsData);

    let roleToUpdate = find(current, ['role_name', role]);
    if (!roleToUpdate) {
      current.push({
        role_name: role,
        permissions: []
      });
      roleToUpdate = find(current, ['role_name', role]);
    }

    const result = await setRolePermission({ role, permission });
    if (result.ok) {
      // kapag OKAY yung API, dito gawin yung actual checking and reflecting UI
      if (includes(roleToUpdate.permissions, permission)) {
        pull(roleToUpdate.permissions, permission);
      } else {
        roleToUpdate.permissions.push(permission);
      }
    } else {
      notiMessage = `Error: ${result.data.message}`;
      notiOpt = {...notiOpt, variant: 'error'};
    }

    enqueueSnackbar(notiMessage, notiOpt);
    setRolesPermissionsData(current);
  };
  
  const fetchRolesAndPermissions = () => {
    Promise.all([ fetchRoles(), fetchPermissions(), fetchCurrentRolePermissions()]).then((values) => {
      if (values[0].ok && values[0].data) {
        setRoles(values[0].data);
      }

      if (values[1].ok && values[1].data) {
        setPermissions(values[1].data);
      }

      if (values[2].ok && values[2].data) {
        setRolesPermissionsData(values[2].data);
      }
    }).catch((err) => {
      console.error(err);
    }).finally(() => {

    });
  };
 
  const applyRolesAndPermissionsFromEditor = (roles, permissions, rolesPermissionsData) => {
    setRoles(roles);
    setPermissions(permissions);
    setRolesPermissionsData(rolesPermissionsData);
  };

  const showEditorDialog = () => setEditorDialog(true);
  const hideEditorDialog = () => setEditorDialog(false);

  useEffect(() => {
    fetchRolesAndPermissions();
  }, []);

  useEffect(() => {
    renderDataGrid();
  }, [rolesPermissionsData, roles, permissions]);

  return (
    <Fragment>
      <DataGrid
        disableSelectionOnClick
        rows={rows}
        columns={columns}
        autoPageSize={true}
        rowHeight={36}
        headerHeight={40}
        density='compact'
        components={{
          Toolbar: () => (
            <Fragment>
              <GridToolbarContainer>
                <Box m={2} className={classes.toolbar}>
                  <Button variant='contained' startIcon={<ChangeHistory />} onClick={showEditorDialog}>Manage Roles & Permissions</Button>
                  <Button variant='contained' startIcon={<CloudDownload />} >Export to Enum</Button>
                </Box>
              </GridToolbarContainer>
              <Divider />
            </Fragment>
          )
        }}
      />

      <Dialog disableEscapeKeyDown disableBackdropClick scroll='paper' fullWidth maxWidth='sm' open={editorDialog} onClose={hideEditorDialog} PaperProps={{ variant: 'outlined' }}>
        <DialogTitle>Roles and permissions editor</DialogTitle>
        <DialogContent dividers>
          {/* <RolesAndPermissionEditor 
            currentRolesPermissionsData={rolesPermissionsData}
            currentRoles={roles}
            currentPermissions={permissions}
            onApply={applyRolesAndPermissionsFromEditor}
            refetch={fetchRolesAndPermissions}
          /> */}
        </DialogContent>
        <DialogActions>
          <Button variant='contained' size='small' onClick={hideEditorDialog}>Close</Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

export default RolesAndPermissionsPanel;