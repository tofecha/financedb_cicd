import { Grid } from '@material-ui/core';
import { clone, includes, find, pull } from 'lodash-es';
import React, { useEffect, useState } from 'react';
import { Fragment } from 'react';
import { RPMode } from '../../config/enums';
import { useStore } from '../../store/store';
import RolesPanel from './RolesPanel';
import PermissionsPanel from './PermissionsPanel';
import { useSnackbar } from 'notistack';

const RolesAndPermissionEditor = (props) => {
  const [ , actions ] = useStore();
  const { enqueueSnackbar } = useSnackbar();
  
  const { currentRolesPermissionsData, currentRoles, currentPermissions, refetch,
    setRolePermission,
    addRole,
    addPermission } = props;
  const [ mode, setMode ] = useState(RPMode.DEFAULT);
  const [ selected, setSelected ] = useState(undefined);

  const [ roles, setRoles ] = useState(undefined);
  const [ permissions, setPermissions ] = useState(undefined);
  const [ rolesPermissionsData, setRolesPermissionsData ] = useState([]);
  
  const [ roleText, setRoleText ] = useState(undefined);
  const [ permissionText, setPermissionText ] = useState(undefined);

  const setupModeToRoleToPermissions = (role) => {
    setSelected(role);
    setMode(RPMode.ROLE_TO_PERMISSIONS);
  };

  const setupModeToPermissionsToRole = (permission) => {
    setSelected(permission);
    setMode(RPMode.PERMISSION_TO_ROLES);
  };

  const setupModeToDefault = () => {
    setMode(RPMode.DEFAULT);
  };

  const checkBoxClicked = async (role, permission) => {
    let notiMessage = 'Success';
    let notiOpt = {
      variant: 'success',
      preventDuplicate: true,
      autoHideDuration: 3000
    };
    actions.showPageStateLoading();
    
    const current = clone(rolesPermissionsData);
    
    let roleToUpdate = find(current, ['role_name', role]);
    if (!roleToUpdate) {
      current.push({
        role_name: role,
        permissions: []
      });
      roleToUpdate = find(current, ['role_name', role]);
    }

    const result = await setRolePermission({ role, permission });
    if (result.ok) {
      // kapag OKAY yung API, dito gawin yung actual checking and reflecting UI
      if (includes(roleToUpdate.permissions, permission)) {
        pull(roleToUpdate.permissions, permission);
      } else {
        roleToUpdate.permissions.push(permission);
      }
    } else {
      notiMessage = `Error: ${result.data.message}`;
      notiOpt = {...notiOpt, variant: 'error'};
    }

    enqueueSnackbar(notiMessage, notiOpt);
    setRolesPermissionsData(current);
    actions.hidePageStateLoading();
  };

  const doesRoleHavePermission = (role, permission) => includes(find(rolesPermissionsData, ['role_name', role])?.permissions, permission);

  const handleAddRole = async () => {
    let notiMessage = 'Success';
    let notiOpt = {
      variant: 'success',
      preventDuplicate: true,
      autoHideDuration: 3000
    };

    actions.showPageStateLoading();

    const result = await addRole({ role: roleText});

    if (result.ok) {
      refetch();

    } else {
      notiMessage = `Error: ${result.data.message}`;
      notiOpt = {...notiOpt, variant: 'error'};
    }

    enqueueSnackbar(notiMessage, notiOpt);
    actions.hidePageStateLoading();
    // clear on successful add
  };

  const handleAddPermission = async () => {
    let notiMessage = 'Success';
    let notiOpt = {
      variant: 'success',
      preventDuplicate: true,
      autoHideDuration: 3000
    };
    actions.showPageStateLoading();

    const result = await addPermission({ permission: permissionText});

    if (result.ok) {
      refetch();

    } else {
      notiMessage = `Error: ${result.data.message}`;
      notiOpt = {...notiOpt, variant: 'error'};
    }
    enqueueSnackbar(notiMessage, notiOpt);
    actions.hidePageStateLoading();
    
    // clear on successful add
  };

  useEffect(() => {
    setRoles(currentRoles);
    setPermissions(currentPermissions);
    setRolesPermissionsData(currentRolesPermissionsData);
  }, []);

  return (
    <Fragment>
      <Grid container spacing={2} direction={ mode === RPMode.PERMISSION_TO_ROLES ? 'row-reverse' : 'row'}>
        <Grid item xs={6}>
          <RolesPanel
            roles={roles}
            mode={mode}
            selected={selected}
            handleAddRole={handleAddRole}
            setupModeToDefault={setupModeToDefault}
            roleText={roleText}
            setRoleText={setRoleText}
            checkBoxClicked={checkBoxClicked}
            doesRoleHavePermission={doesRoleHavePermission}
            setupModeToRoleToPermissions={setupModeToRoleToPermissions}
          />
        </Grid>
        <Grid item xs={6}>
          <PermissionsPanel
            permissions={permissions}
            mode={mode}
            selected={selected}
            permissionText={permissionText}
            setupModeToDefault={setupModeToDefault}
            setPermissionText={setPermissionText}
            handleAddPermission={handleAddPermission}
            checkBoxClicked={checkBoxClicked}
            doesRoleHavePermission={doesRoleHavePermission}
            setupModeToPermissionsToRole={setupModeToPermissionsToRole}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default RolesAndPermissionEditor;