import React, { useEffect, useState } from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { find, isEmpty, keys, map } from 'lodash-es';
import { format, parseISO } from 'date-fns';

import * as echarts from 'echarts/core';
import { BarChart } from 'echarts/charts';
import { SVGRenderer, } from 'echarts/renderers';
import { GridComponent, DatasetComponent, TooltipComponent, LegendComponent } from 'echarts/components';
import ReactEChartsCore from 'echarts-for-react/lib/core';
import { USDFormatter } from '../../config/usdFormatter';
import labelOnBarOptions from '../../config/echartsLabelOnBar';
import { AppStoreSubscriber } from '../../stores/appStore';

echarts.use(
  [LegendComponent, DatasetComponent, GridComponent, TooltipComponent, BarChart, SVGRenderer]
);

const useStyles = makeStyles((theme) => ({
}));

const BarAndLineGraph = (props) => {
  const { data = [], labelOnBar = false, height = 400 } = props;
  const [xAxis, setXAxis] = useState([]);
  const [graphData, setGraphData] = useState([]);

  const earningsBeforeTax = (values) => {
    const actualRevenue = values?.actual_revenue;
    const operationCosts = values?.operating_costs;

    if (actualRevenue && !isEmpty(operationCosts)) {
      const totalOperationCosts = values?.operating_costs?.reduce((acc, cur) => (
        acc + cur.cost
      ), 0);
      return actualRevenue - totalOperationCosts;
    }
    return 0;
  };

  useEffect(() => {
    if (!isEmpty(data)) {
      const xAxis = [];
      const coserviceBar = [];
      const adminOperBar = [];
      const earningBeforeTaxLine = [];

      map(data, (monthData) => {
        if (keys(monthData).length > 1) {
          const sumOperatingCost = monthData.operating_costs.reduce((acc, cur) => (
            cur.operation_name !== 'Admin and other operating costs' ? acc + cur.cost : acc
          ), 0);
          const adminOperatingCost = find(monthData.operating_costs, ['operation_name', 'Admin and other operating costs'])?.cost || 0;
          const earnings = earningsBeforeTax(monthData) ;
  
          xAxis.push(monthData.date);
          coserviceBar.push(sumOperatingCost);
          adminOperBar.push(adminOperatingCost);
          earningBeforeTaxLine.push(earnings);
        } else {
          xAxis.push(monthData.date);
          coserviceBar.push(null);
          adminOperBar.push(null);
          earningBeforeTaxLine.push(null);
        }
      });

      setXAxis(xAxis);
      setGraphData([{
        name: 'Cost of services',
        data: coserviceBar,
        type: 'bar',
        avoidLabelOverlap: true,
        label: {
          show: true,
          position: 'top',
          ...labelOnBarOptions(labelOnBar),
          formatter: (({ value }) => {
            return USDFormatter.format(value);
          })
        },
      }, {
        name: 'Admin and other Operating expenses',
        data: adminOperBar,
        type: 'bar',
        avoidLabelOverlap: true,
        label: {
          show: true,
          position: 'top',
          ...labelOnBarOptions(labelOnBar),
          formatter: (({ value }) => {
            return USDFormatter.format(value);
          })
        },
      }, {
        name: 'Earnings before Income tax',
        data: earningBeforeTaxLine,
        type: 'line',
        avoidLabelOverlap: true,
        label: {
          show: true,
          position: 'top',
          ...labelOnBarOptions(labelOnBar),
          formatter: (({ value }) => {
            return USDFormatter.format(value);
          })
        },
      }]);
    }
  }, [data, labelOnBar]);

  const options = {
    legend: {
      type: 'scroll',
    },
    tooltip: {
      show: true,
      trigger: 'item',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      top: 40,
      right: 0,
      bottom: 20,
      left: 10,
      borderWidth: 0,
      containLabel: true
    },
    xAxis: {
      type: 'category',
      show: true,
      splitNumber: 5,
      data: xAxis,
      axisLabel: {
        formatter: ((value) => {
          return format(parseISO(value), 'MMM yyyy');
        }),
      },
      axisPointer: {
        snap: true,
        label: {
          formatter: (({ value }) => {
            return format(parseISO(value), 'MM/dd');
          })
        }
      }
    },
    yAxis: {
      show: true,
      splitLine: {
        show: false
      },
      splitNumber: 5,
      min: 0,
      type: 'value',
      axisPointer: {
        snap: true,
      },
    },
    animationEasing: 'elasticOut',
  };

  return (
    <Box height={height} >
      <AppStoreSubscriber>
        {({ theme }) => (
          <ReactEChartsCore
            echarts={echarts}
            option={{
              ...options,
              series: graphData
            }}
            notMerge={true}
            lazyUpdate={true}
            opts={{
              height: height
            }}
            theme={theme}
          />
        )}
      </AppStoreSubscriber>
    </Box>
  );
};

export default BarAndLineGraph;