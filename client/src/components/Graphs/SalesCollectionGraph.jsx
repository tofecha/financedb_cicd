import { Box } from '@material-ui/core';
import { format, parseISO } from 'date-fns';
import { map } from 'lodash-es';
import React, { useEffect, useState } from 'react';
import { darkNivoTheme, lightNivoTheme } from '../../config/nivoThemes';

import * as echarts from 'echarts/core';
import { BarChart } from 'echarts/charts';
import { SVGRenderer, } from 'echarts/renderers';
import { GridComponent, DatasetComponent, TooltipComponent } from 'echarts/components';
import ReactEChartsCore from 'echarts-for-react/lib/core';
import { USDFormatter } from '../../config/usdFormatter';
import labelOnBarOptions from '../../config/echartsLabelOnBar';
import { AppStoreSubscriber } from '../../stores/appStore';

echarts.use(
  [DatasetComponent, GridComponent, TooltipComponent, BarChart, SVGRenderer]
);

const SalesCollectionGraph = (props) => {
  const { data = [], labelOnBar = false,  height = 400, width = null } = props;
  const [ xAxis, setXAxis ] = useState([]);
  const [ graphData, setGraphData ] = useState([]);

  useEffect(() => {
    if (data) {
      const xAxis = [];
      const salesCollectionBar = [];

      map(data, (monthData) => {
        xAxis.push(monthData.date);
        const salesCollection = monthData.sales_collection !== 0 ? monthData.sales_collection : monthData?.sales_collection === 0 ? 0 : null;
        salesCollectionBar.push(salesCollection);
      });

      setXAxis(xAxis);
      setGraphData([{
        data: salesCollectionBar,
        type: 'bar',
        label: {
          show: true,
          position: 'top',
          ...labelOnBarOptions(labelOnBar),
          formatter: (({ value }) => {
            return USDFormatter.format(value);
          })
        },
      }]);
    }
  }, [data, labelOnBar]);

  const options = {
    tooltip: {
      show: true,
      trigger: 'item',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      top: 40,
      right: 0,
      bottom: 20,
      left: 10,
      borderWidth: 0,
      containLabel: true
    },
    xAxis: {
      type: 'category',
      show: true,
      splitNumber: 5,
      data: xAxis,
      axisLabel: {
        formatter: ((value) => {
          return format(parseISO(value), 'MMM yyyy');
        }),
      },
      axisPointer: {
        snap: true,
        label: {
          formatter: (({ value }) => {
            return format(parseISO(value), 'MM/dd');
          })
        }
      }
    },
    yAxis: {
      show: true,
      splitLine: {
        show: false
      },
      splitNumber: 5,
      min: 0,
      type: 'value',
      axisPointer: {
        snap: true,
      },
    },
    animationEasing: 'elasticOut',
  };

  return (
    <Box height={height} width={width}>
      <AppStoreSubscriber>
        {({ theme }) => (
          <ReactEChartsCore
            echarts={echarts}
            option={{
              ...options,
              series: graphData
            }}
            notMerge={true}
            lazyUpdate={true}
            opts={{
              height: height
            }}
            theme={theme}
          />
        )}
      </AppStoreSubscriber>
    </Box>
  );
};

export default SalesCollectionGraph;

