import React, { useEffect, useState } from 'react';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { isEmpty, keys, map } from 'lodash-es';
import { format, parseISO } from 'date-fns';

import * as echarts from 'echarts/core';
import { LineChart } from 'echarts/charts';
import { SVGRenderer, } from 'echarts/renderers';
import { GridComponent, DatasetComponent, TooltipComponent } from 'echarts/components';
import ReactEChartsCore from 'echarts-for-react/lib/core';
import { AppStoreSubscriber } from '../../stores/appStore';

echarts.use(
  [DatasetComponent, GridComponent, TooltipComponent, LineChart, SVGRenderer]
);

const useStyles = makeStyles((theme) => ({
}));

const PercentageEarningBeforeIncomeTaxGraph = (props) => {
  const { data = [], theme, height = 400 } = props;
  const [ graphData, setGraphData ] = useState([]);

  const earningsBeforeTax = (values) => {
    const actualRevenue = values?.actual_revenue;
    const operationCosts = values?.operating_costs;

    if (actualRevenue && !isEmpty(operationCosts)) {
      const totalOperationCosts = values?.operating_costs?.reduce((acc, cur) => (
        acc + cur.cost
      ), 0);
      return (((actualRevenue - totalOperationCosts) / actualRevenue)).toPrecision(5) ;
    }
    return 0;
  };

  useEffect(() => {
    if (!isEmpty(data)) {
      setGraphData([{
        data: map(data, (monthData) => [
          monthData.date,
          (keys(monthData).length > 1 ? earningsBeforeTax(monthData) : 0)
        ]),
        label: {
          show: true,
          position: 'top',
          formatter: (({ value }) => {
            return `${(value[1] * 100).toFixed(2)}%`;
          })
        },
        type: 'line',
        symbol: 'emptyCircle',
        symbolSize: 10
      }]);
    }
  }, [data]);

  const options = {
    tooltip: {
      show: true,
      trigger: 'item',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      },
      formatter: (({ value }) => {
        return `${(value[1] * 100).toFixed(2)}%`;
      })
    },
    grid: {
      top: 40,
      right: 30,
      bottom: 20,
      left: 10,
      borderWidth: 0,
      containLabel: true
    },
    xAxis: {
      type: 'category',
      show: true,
      splitNumber: 5,
      axisLabel: {
        formatter: ((value) => {
          return format(parseISO(value), 'MMM yyyy');
        }),
      },
      axisPointer: {
        snap: true,
        label: {
          formatter: (({ value }) => {
            return format(parseISO(value), 'MMM yyyy');
          })
        }
      }
    },
    yAxis: {
      show: true,
      splitLine: {
        show: true
      },
      splitNumber: 5,
      min: 0,
      type: 'value',
      axisPointer: {
        snap: true,
      },
      axisLabel: {
        formatter: ((value) => {
          return `${(value * 100).toFixed(0)}%`;
          // return parseFloat(value).toPrecision(4);
        })
      }
    },
    animationEasing: 'elasticOut',
  };

  return (
    <Box height={height}>
      <AppStoreSubscriber>
        {({ theme }) => (
          <ReactEChartsCore
            echarts={echarts}
            option={{
              ...options,
              series: graphData
            }}
            notMerge={true}
            lazyUpdate={true}
            opts={{
              height: height
            }}
            theme={theme}
          />
        )}
      </AppStoreSubscriber>
    </Box>
  );
};

export default PercentageEarningBeforeIncomeTaxGraph;