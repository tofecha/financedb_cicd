import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import React, { Fragment } from 'react';
import { USDFormatter } from '../../config/usdFormatter';

const StyledTableCell = withStyles((theme) => ({
  head: {
    fontFamily: 'Roboto',
    fontSize: 12,
  },
  body: {
    fontFamily: 'Roboto',
    fontSize: 10,
    fontWeight: 400
  },
}))(TableCell);

const ListTable = (props) => {
  const {
    rows = [],
    labelKey = 'label',
    valueKey = 'value',
    labelHeader = 'Label',
    valueLabel = 'Value',
    limit = 10
  } = props;

  return (
    <TableContainer style={{ marginBottom: 20, minHeight: 400 }}>
      <Table
        size='small'
        stickyHeader
      >
        <TableHead>
          <TableRow>
            <StyledTableCell>{ labelHeader }</StyledTableCell> 
            <StyledTableCell
              align='right'
              sortDirection='desc'
            >
              { valueLabel }
            </StyledTableCell> 
          </TableRow>
        </TableHead>
        <TableBody>
          {
            rows.slice(0, limit).map((row, index) => (
              <TableRow key={`table-cell-${index}`}>
                <StyledTableCell>{ row[labelKey] }</StyledTableCell> 
                <StyledTableCell align='right'>{ USDFormatter.format(row[valueKey]) }</StyledTableCell> 
              </TableRow>
            ))
          }
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ListTable;