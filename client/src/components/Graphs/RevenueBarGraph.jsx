import React, { useEffect, useState } from 'react';
import { Box } from '@material-ui/core';
import { format, parseISO } from 'date-fns';
import { isArray, map } from 'lodash-es';

import * as echarts from 'echarts/core';
import { BarChart } from 'echarts/charts';
import { SVGRenderer, } from 'echarts/renderers';
import { GridComponent, DatasetComponent, TooltipComponent, LegendComponent } from 'echarts/components';
import ReactEChartsCore from 'echarts-for-react/lib/core';
import { USDFormatter } from '../../config/usdFormatter';
import labelOnBarOptions from '../../config/echartsLabelOnBar';
import { AppStoreSubscriber } from '../../stores/appStore';

echarts.use(
  [LegendComponent, DatasetComponent, GridComponent, TooltipComponent, BarChart, SVGRenderer]
);

const RevenueBarGraph = (props) => {
  const { data = [], labelOnBar = false, height = 400 } = props;
  const [ calculatedData, setCalculatedData ] = useState([]);
  const [ xAxis, setXAxis] = useState([]);
  const [ graphData, setGraphData ] = useState([]);

  useEffect(() => {
    if (data && isArray(data)) {
      // setCalculatedData(data.map((d) => ({
      //   ...d,
      //   'Target': d.target_revenue || null,
      //   'Revenue': d.actual_revenue || null
      // })));
      const xAxis = [];
      const actualBar = [];
      const targetBar = [];
      // const regularBar = [];
      // const adhocBar = [];
      // const pendingLaunchBar = [];

      map(data, (monthData) => {
        xAxis.push(monthData.date);
        targetBar.push(monthData.target_revenue);
        actualBar.push(monthData.actual_revenue);
        // regularBar.push(monthData.regular);
        // adhocBar.push(monthData.adhoc);
        // pendingLaunchBar.push(monthData.pending_launch);
      });


      setXAxis(xAxis);
      setGraphData([{
        name: 'Target',
        data: targetBar,
        type: 'bar',
        label: {
          show: true,
          position: 'top',
          ...labelOnBarOptions(labelOnBar),
          formatter: (({value}) => {
            return USDFormatter.format(value);
          })
        },
      }, {
        name: 'Actual',
        data: actualBar,
        type: 'bar',
        label: {
          show: true,
          position: 'top',
          ...labelOnBarOptions(labelOnBar),
          formatter: (({value}) => {
            return USDFormatter.format(value);
          })
        },
      }
      // , {
      //   name: 'Regular',
      //   data: regularBar,
      //   type: 'bar',
      //   stack: 'Actual',
      //   color: '#7CCC86',
      // }, {
      //   name: 'Adhoc',
      //   data: adhocBar,
      //   type: 'bar',
      //   stack: 'Actual',
      //   color: '#7FE3B8',
      // }, {
      //   name: 'Pending Launch',
      //   data: pendingLaunchBar,
      //   type: 'bar',
      //   stack: 'Actual',
      //   color: '#7CCBCC',
      //   label: {
      //     show: true,
      //     position: 'top',
      //     ...labelOnBarOptions(labelOnBar),
      //     formatter: ((data) => {
      //       const { value, dataIndex } = data;
      //       const sum = parseFloat(regularBar[dataIndex]) + parseFloat(adhocBar[dataIndex]) + parseFloat(pendingLaunchBar[dataIndex]);
      //       return USDFormatter.format(sum);
      //     })
      //   },
      // }
      ]);
    }
  }, [data, labelOnBar]);

  const options = {
    legend: {
      data: ['Target', 'Actual']
    },
    tooltip: {
      show: true,
      trigger: 'item',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      top: 40,
      right: 0,
      bottom: 20,
      left: 10,
      borderWidth: 0,
      containLabel: true
    },
    xAxis: {
      type: 'category',
      show: true,
      splitNumber: 5,
      data: xAxis,
      axisLabel: {
        formatter: ((value) => {
          return format(parseISO(value), 'MMM yyyy');
        }),
      },
      axisPointer: {
        snap: true,
        label: {
          formatter: (({ value }) => {
            return format(parseISO(value), 'MM/dd');
          })
        }
      }
    },
    yAxis: {
      show: true,
      splitLine: {
        show: false
      },
      splitNumber: 5,
      min: 0,
      type: 'value',
      axisPointer: {
        snap: true,
      },
    },
    animationEasing: 'elasticOut',
  };

  return (
    <Box height={height}>
      <AppStoreSubscriber>
        {({ theme }) => (
          <ReactEChartsCore
            echarts={echarts}
            option={{
              ...options,
              series: graphData
            }}
            notMerge={true}
            lazyUpdate={true}
            opts={{
              height: height
            }}
            theme={theme}
          />
        )}
      </AppStoreSubscriber>
    </Box>
  );
};

export default RevenueBarGraph;