import React, { useEffect, useState } from 'react';
import { Typography, useMediaQuery } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { addDays, differenceInMilliseconds, format } from 'date-fns';
import { memo } from 'react';

const DateDisplay = () => {
  const theme = useTheme();
  const isWidthLgAndUp = useMediaQuery(theme.breakpoints.up('lg'));
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    const dateNow = new Date();
    const intervalTime = differenceInMilliseconds(addDays(new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(), 0, 0, 0, 0), 1), dateNow);
    const interval = setInterval(() => setDate(new Date()), intervalTime);
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <Typography variant={isWidthLgAndUp ? 'h6' : 'subtitle1'} >
      {`Today is ${ format(date, isWidthLgAndUp ? 'LLLL dd yyyy' : 'LLL dd yyyy') }`}
    </Typography>
  );
};

export default memo(DateDisplay);