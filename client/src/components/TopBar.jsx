import React, { Fragment } from 'react';
import { Avatar, Box, Divider, Grid, Hidden, IconButton, Toolbar, Popover, useMediaQuery, MenuList, Menu, Typography, MenuItem } from '@material-ui/core';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { Timeline as TimelineIcon, Brightness7 as Brightness7Icon, Brightness4 as Brightness4Icon } from '@material-ui/icons';
import DateDisplay from './DateDisplay';
import TimeDisplay from './TimeDisplay';
import { useAppStore } from '../stores/appStore';
import getGravatarLink from '../config/gravatarLink';
import NavBar from './NavBar';
import { useAuthStore } from '../stores/authStore';
import {
  usePopupState,
  bindTrigger,
  bindPopover,
} from 'material-ui-popup-state/hooks';

const useStyles = makeStyles((theme) => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    '& > *': {
      marginRight: theme.spacing(1)
    },
    '& > :last-child': {
      marginRight: theme.spacing(0)
    }
  },
  logoContainer: {
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(1),
  },
  titleIcon: {
    marginRight: theme.spacing(1)
  },
  logoDividerMargin: {
    marginRight: theme.spacing(3),
    [theme.breakpoints.between('xs', 'md')] : {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(1),
    }
  },
  toolbar: {
    height: '100%'
  },
  spacer: {
    flexGrow: 1,
  },
  dividerMargin: {
    marginRight: theme.spacing(3),
    marginLeft: theme.spacing(3),
    [theme.breakpoints.between('xs', 'md')] : {
      marginRight: theme.spacing(1),
      marginLeft: theme.spacing(1),
    }
  },
  avatarSmall: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
  avatarLarge: {
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
  titleCardBox: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'flex-start'
  },
  titleCardText: {
    fontFamily: '"Roboto Mono", monospace',
    fontWeight: 700,
    fill: theme.palette.common.white,
  },
  menuItemMulti: {
    '& > *': {
      marginRight: theme.spacing(1)
    },
    '& > :last-child': {
      marginRight: theme.spacing(0)
    }
  }
}));

const TopBar = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [authStore, authActions] = useAuthStore();
  const [appStore, appActions] = useAppStore();
  const isWidthLgAndUp = useMediaQuery(theme.breakpoints.up('lg'));
  const popupState = usePopupState({
    variant: 'popover',
    popupId: 'avatarPopover',
  });

  const themeChange = () => appActions.setTheme(appStore.theme === 'dark' ? 'light' : 'dark');

  return (
    <Fragment>
      <Toolbar>
        <Grid spacing={0} container direction='row' className={classes.toolbar}>
          <Grid item md={4} sm={2} xs={2} className={classes.row} style={{ justifyContent: 'flex-start'}}>
            <TimelineIcon fontSize='large' className={classes.titleIcon}/>
            <Hidden smDown>
              <svg
                width={100}
                height={40}
                viewBox={`0 0 ${100} ${40}`}
                preserveAspectRatio='xMidYMid meet'
              >
                <text
                  x='0%'
                  y='45%'
                  className={classes.titleCardText}
                  fontSize={32 * 0.5}
                >
                FINANCE
                </text>
                <text
                  x='0%'
                  y='80%'
                  className={classes.titleCardText}
                  fontSize={32 * 0.5}
                >
                DASHBOARD
                </text>
              </svg>
              <Divider orientation="vertical" flexItem />
              <DateDisplay />
            </Hidden>
            <Divider orientation="vertical" flexItem />
          </Grid>
          <Grid item md={4} sm={8} xs={8} className={classes.row} style={{ justifyContent: 'center' }}>
            <NavBar />
          </Grid>
          <Grid item md={4} sm={2} xs={2} className={classes.row} style={{ justifyContent: 'flex-end' }}>
            <Hidden smDown>
              <Divider orientation="vertical" flexItem />
              <TimeDisplay />
            </Hidden>
            <Divider orientation="vertical" flexItem />
            <IconButton size='small' edge='end' color='primary' {...bindTrigger(popupState)}>
              <Avatar className={classes.avatarSmall} src={getGravatarLink(authStore.user?.md5_personal_email, 64)} />
            </IconButton>
          </Grid>
        </Grid>
      </Toolbar>
      <Divider />
      <Popover
        {...bindPopover(popupState)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Box>
          <Box p={2} display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
            <Avatar className={classes.avatarLarge}  src={getGravatarLink(authStore.user?.md5_personal_email, 128)} />
            <Typography variant='h6'>
              { authActions.getFullname() }
            </Typography>
            <Typography variant='subtitle2'>
              { 'Finance Dashboard Developer' }
            </Typography>
            <Typography variant='subtitle1'>
              { authStore.user?.company_email || '' }
            </Typography>
          </Box>
          <Divider />
          <MenuItem onClick={themeChange}>
            <Box display='flex' flexDirection='row' justifyContent='flexStart' alignItems='center' className={classes.menuItemMulti} >
              { appStore.theme === 'dark' ? <Brightness4Icon/> : <Brightness7Icon/>}
              <Typography variant='body1'>
                {`Current: ${appStore.theme}`}
              </Typography>
            </Box>
          </MenuItem>
        </Box>
      </Popover>

    </Fragment>
  );
};

export default TopBar;