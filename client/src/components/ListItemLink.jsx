import React from 'react';
import breadcrumbNameMap from '../config/breadcrumbNameMap';
import { Link as RouterLink } from '@reach/router';
import { ListItem, ListItemText } from '@material-ui/core';
import { startsWith } from 'lodash-es';

const ListItemLink = (props) => {
  const { to, selected, ...other } = props;
  const primary = breadcrumbNameMap[to];
  const isSelected = startsWith(selected, to);

  return (
    <li>
      <ListItem button component={RouterLink} to={to} {...other} selected={isSelected}>
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
};

export default ListItemLink;
