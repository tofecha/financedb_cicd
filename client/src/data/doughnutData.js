export const data1 = [
  { name: 'Client A', value: 6000 },
  { name: 'Client B', value: 4000 },
];

export const data2 = [
  { name: 'Cost of sales', value: 50000 },
  { name: 'Rent', value: 20000 },
  { name: 'Consulting', value: 20000 },
  { name: 'Others', value: 10000 },
];

export const data3 = [
  { name: 'A1', value: 10 },
  { name: 'A2', value: 20 },
  { name: 'B1', value: 50 },
  { name: 'B2', value: 124 },
  { name: 'A3', value: 40 },
];

export const data4 = [
  { name: 'Group A', value: 400 },
  { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 },
  { name: 'Group D', value: 200 },
];

export const data5 = [
  { name: 'Group A', value: 10 },
  { name: 'Group B', value: 423 },
  { name: 'Group C', value: 20 },
  { name: 'Group D', value: 150 },
  { name: 'Group E', value: 999 }

];