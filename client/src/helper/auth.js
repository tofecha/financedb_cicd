import { OAuth2AuthCodePKCE } from '@bity/oauth2-auth-code-pkce/index.umd';
import bspApi, { setApiToken } from '../config/bspApi';
import env from '../config/env';

const oauth = new OAuth2AuthCodePKCE({
  authorizationUrl: `${env.AUTH_URL}/${env.AUTH_ROUTE}`,
  tokenUrl: `${env.AUTH_URL}/${env.TOKEN_ROUTE}`,
  clientId: env.CLIENT_ID,
  scopes: ['*'],
  redirectUrl: env.REDIRECT_URI,
  onAccessTokenExpiry(refreshAccessToken) {
    // 'Expired! Access token needs to be renewed.'
    // 'We will try to get a new access token via grant code or refresh token.'
    console.log('onAccessTokenExpiry', refreshAccessToken);
    return refreshAccessToken();
  },
  onInvalidGrant(refreshAuthCodeOrRefreshToken) {
    // 'Expired! Auth code or refresh token needs to be renewed.'
    // 'Redirecting to auth server to obtain a new auth grant code.'
    console.log('refreshAuthCodeOrRefreshToken', refreshAuthCodeOrRefreshToken);
    return refreshAuthCodeOrRefreshToken();
  },
});

const resetOAuth = () => {
  oauth.reset();
};

const hasToken = async () => {
  const tokenObject = await oauth.getAccessToken();
  if (tokenObject?.token?.value && !oauth.isAccessTokenExpired()) {
    return true;
  } else {
    return false;
  }
};

const setupToken = async () => {
  try {
    const tokenObject = await oauth.getAccessToken();
    if (tokenObject && tokenObject.token.value) {
      setApiToken(tokenObject.token.value);
    }
  } catch (err) {
    setApiToken('');
  }
};

const authenticate = async () => {
  try {
    await setupToken();
    const verify = await bspApi.get('/user');
    const isTokenValid = Boolean(verify.ok && verify.status === 200);

    if (!isTokenValid) {
      const authCodeResult = await oauth.isReturningFromAuthServer();
      if (authCodeResult) {
        await setupToken();
      } else {
        oauth.fetchAuthorizationCode();
      }
    } else {
      if (verify.data?.access !== 'allowed') {
        oauth.reset();
        oauth.fetchAuthorizationCode();
      }
    }
  } catch (err) {
    console.error('authenticate error',err);
  }
};

export {
  hasToken,
  setupToken,
  authenticate
};