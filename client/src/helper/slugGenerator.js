import { replace, trim } from 'lodash-es';

export const slugGenerator = (text) => {
  return text ? replace(trim(text), /\s+/g, '-').toLowerCase() : undefined;
};