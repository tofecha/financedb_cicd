import { defineConfig } from 'vite';
import reactRefresh from '@vitejs/plugin-react-refresh';
import { VitePWA } from 'vite-plugin-pwa';
import { visualizer } from 'rollup-plugin-visualizer';

// https://vitejs.dev/config/
export default defineConfig({
  // This changes the out put dir from dist to build
  // comment this out if that isn't relevant for your project
  logLevel: 'info',
  build: {
    minify: 'esbuild',
    outDir: 'build',
    chunkSizeWarningLimit: '2000',
  },
  plugins: [
    reactRefresh(),
    VitePWA(),
    visualizer()
  ],
  esbuild: {
    loader: 'jsx',
    include: /src\/.*\.jsx?$/,
    exclude: []
  },
  server: {
    watch: {
      usePolling: true
    },
    port: 5031
  },
});
